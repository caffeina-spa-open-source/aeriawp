<?php

namespace Aeria;

class Columns {

	public static function register(array $o = []) {
		if (!isset($o['type'])) {
			throw new \Exception("Missing attribute type");
		} else if (!is_string($o['type']) && !is_array($o['type'])) {
			throw new \Exception("Attribute type must be string or array of
				strings");
		}
		$post_types = [];
		if (isset($o['type'])) {
			$post_types[] = $o['type'];
		}
		if (isset($o['types'])) {
			$post_types = array_merge($post_types, $o['types']);
		}
		$post_types = array_unique($post_types);

		$add = isset($o['add']) && is_array($o['add'])
			? $o['add']
			: [];
		$remove = isset($o['remove']) && is_array($o['remove'])
			? $o['remove']
			: [];

		foreach ($post_types as $post_type) {

			/**
			 * Remove & re-order columns
			 */
			Filter::add(
				'manage_' . $post_type . '_posts_columns',
				function($columns) use ($post_type, $remove, $add) {
					$old_columns  = $columns;
					$firsts = [];
					$lasts = [];
					$centers = [];

					$firsts['cb'] = $old_columns['cb'];
					unset($old_columns['cb']);

					foreach ($add as $name => $column) {
						$col_title = isset($column['title'])
							? $column['title']
							: $name;
						if (isset($column['position'])) {
							if ($column['position'] == 'first') {
								$firsts[$name] = $col_title;
							} else if ($column['position'] == 'last') {
								$lasts[$name] = $col_title;
							} else {
								$centers[$name] = $col_title;
							}
						} else {
							$centers[$name] = $col_title;
						}
					}

					$new_columns = [];

					foreach ($firsts as $name => $col) {
						$new_columns[$name] = $col;
					}

					foreach ($old_columns as $name => $col) {
						if (!in_array($name, $remove)) {
							$new_columns[$name] = $col;
						}
					}

					foreach ($centers as $name => $col) {
						$new_columns[$name] = $col;
					}

					foreach ($lasts as $name => $col) {
						$new_columns[$name] = $col;
					}

					return $new_columns;
				}
			);

			/**
			 * Display columns
			 */
			Action::add(
				'manage_' . $post_type . '_posts_custom_column',
				function($column, $post_id) use ($post_type, $add) {
					if (in_array($column, array_keys($add))) {
						$custom_data = get_post_custom($post_id);

						//$APost = Post::load($post_id, $post_type);
						$def = $add[$column];
						$render = isset($def['render'])
							? $def['render']
							: $column;
						if (Utils::isCallableParam($def['render']))  {
							echo Utils::getParam($def, 'render');
						} else {
							/**\/
							if ($render == 'featured' || $render == 'media') {
								$value = ($render == 'featured')
									? $APost->featuredURL()
									: $APost->fields->{$render};
								$width = @$column_def['width'] ?: '150px';
								$height = @$column_def['height'] ?: '150px';
								$style = @$column_def['style'] ?: '';
								echo '<div style="'
									. 'width:' . $width . ';'
									. 'height:' . $height . ';'
									. 'display:block;'
									. 'margin:0 auto;'
									. 'background:url(' . $value . ') '
									. 'center center no-repeat;'
									. 'background-size:cover;'
									. $style . '"/>';
							} else if ($render == 'relation') {
								$posts = get_posts([
									'post__in' => explode($APost->fields->{$render})
								]);
								$results = [];
								foreach ($posts as $post) {
									$results[] = '<a href="'
										. admin_edit_url_for_id($post->ID)
										. '" target="_blank">'
										. $post->post_title
										. '</a>';
								}
								echo implode('<br/>', $results);
							} else if (isset($custom_data[$render])) {
								echo $custom_data[$render];
							}
							/**/
							if (isset($custom_data[$render])) {
								// TODO: do it better
								echo $custom_data[$render][0];
							}
						}
					}
				},
				2,	// acepted args
				10	// priority
			);

			// Sortable Columns
			$sortable_cols = array_filter($add, function($col) {
				return isset($col['sortable']) && $col['sortable'];
			});
			Action::add(
				'manage_edit-' . $post_type . '_sortable_columns',
				function($columns) use ($add, $remove) {
					$sortable_cols = [];
					foreach ($columns as $key => $_col) {
						if (isset($remove[$key])) {
							continue;
						}
						if (!isset($add[$key])
							|| (isset($add[$key]) && $add[$key]['sortable'])) {
							$sortable_cols[$key] = $key;
						}
					}
					foreach ($add as $key => $col) {
						if (isset($col['sortable']) && $col['sortable']
							&& !isset($sortable_cols[$key])) {
							$sortable_cols[$key] = $key;
						}
					}
					return $sortable_cols;
				}
			);
		}

		// Widths
		Action::add('admin_head', function() use($add) {
			global $post;

			if (!in_array($post->post_type, array_keys($add))) {
				return;
			}
			echo '<style>';
			foreach ($add as $col => $def) {
				echo '.column-' . $col . '{'
					. 'overflow:hidden;vertical-align:middle;'
					. (isset($def['width'])
						? $def['width'] . '!important'
						: ''
					);
			}
			echo '</style>';
		});
	}

}