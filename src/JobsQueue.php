<?php

namespace Aeria;

class JobsQueueHandler {

	use Singleton;

	protected static $wpdb;
	protected static $jobs_queue_table;

	protected function checkQueueTables() {
		if (DBScripts::call('EXISTS::table', ['aeria_jobs_queue'])->res == 0) {
			DBScripts::call('CREATE::aeria_jobs_queue');
		}
	}

	public function __construct() {
		global $wpdb;

		if (Settings::isDisabled('modules.jobsqueue', true)) {
			throw new \Exception("JobsQueue Error: Module JobsQueue is disabled");
		}

		static::$wpdb = $wpdb;
		static::$jobs_queue_table = Settings::get(
			'modules.jobsqueue.tablename',
			$wpdb->prefix . 'aeria_jobs_queue'
		);

		DBScripts::addInterpData('jobs_queue_table', static::$jobs_queue_table);

		DBScripts::load(__DIR__ . '/db/AeriaJobsQueue.sql');

		$this->checkQueueTables();

		Action::add('aeria_add_job', function($options) {
			$this->add($options);
		});

		Action::add('aeria_start_job', function($options) {
			$this->start($options);
		});
	}

	public function updateJobStatus($job_id, $new_status, $message = null) {
		$update_data = ['status' => $new_status];
		if (!empty($message)) {
			$update_data['message'] = $message;
		}
		return (static::$wpdb->update(
			static::$jobs_queue_table,
			$update_data,
			['id' => $job_id]
		) == 1);
	}

	protected function updateMultiJobsStatus(array $ids, $old_statuses, string $new_status) {
		if (!is_array($old_statuses)) {
			$old_statuses = [$old_statuses];
		}
		return (DBScripts::call(
			'UPDATE::jobs_status',
			[$new_status, $ids, $old_statuses]
		) == count($ids));
	}

	public function add($key, $options) {
		$jobname = Utils::getParam($options, 'job');
		if (empty($jobname)) {
			throw new \Exception("Queue: When adding an element to JobsQueue the "
				. "'job' attribute is required");
		}

		$status = Utils::getParam($options, 'status', 'new');
		$message = Utils::getParam($options, 'message', '');
		$data = Utils::getParam($options, 'data', []);
		$metadata = Utils::getParam($options, 'metadata', []);

		$jobkey = hash('sha256', $jobname . '|' . $key);

		$tolerance = (int)Settings::get('modules.jobsqueue.tolerance', 10);
		$update_id = null;

		if ($tolerance > 0) {
			$check = DBScripts::call(
				'CHECK::check_jobkey',
				[ [ $jobkey ], $jobname, [ 'new' ], $tolerance ]
			);
			if (!empty($check)) {
				$update_id = (int)$check->job_id;
			}
		}

		if (!isset($metadata['created_by'])) {
			$metadata['created_by'] = get_current_user_id();
		}

		if (!is_null($update_id) && $update_id > 0) {
			return (static::$wpdb->update(
				static::$jobs_queue_table,
				[
					'jobdata'		=> json_encode($data),
					'metadata'	=> json_encode($metadata),
					'message'		=> $message
				],
				[
					'id' => $update_id
				]
			) == 1);
		}

		return (static::$wpdb->insert(
			static::$jobs_queue_table,
			[
				'jobkey'		=> $jobkey,
				'jobname'		=> $jobname,
				'jobdata'		=> json_encode($data),
				'metadata'	=> json_encode($metadata),
				'status'		=> $status,
				'message'		=> $message
			]
		) == 1);
	}

	public function start($options) {
		if (is_string($options)) {
			$options['job'] = $options;
		}

		$jobname = Utils::getParam($options, 'job');
		if (empty($jobname)) {
			throw new \Exception("Queue: When starting a Job, the JobName is "
				. "needed; pass a 'job' attribute or directly the JobName string");
		}

		$statuses = Utils::getParam($options, 'statuses', ['new']);
		$how_many = Utils::getParam($options, 'how_many', 9999);

		$jobs = DBScripts::call(
			'SELECT::get_jobs_queue',
			[
				$jobname,
				$statuses,
				$how_many
			]
		);

		$jobs_ids = wp_list_pluck($jobs, 'id');

		$curr_job = null;

		try {
			if (!$this->updateMultiJobsStatus($jobs_ids, 'new', 'waiting')) {
				throw new \Exception("Error during status update for "
					. "'{count($jobs_ids)}' jobs from 'new' to 'waiting'");
			}

			foreach ($jobs as $job) {
				$curr_job = $job;

				if (!$this->updateJobStatus($job->id, 'in_progress')) {
					throw new \Exception("Error during status update from "
						. "'{$job->status}' to 'in_progress' for Job '{$job->id}'");
				}

				// NOTE: at the end of the 'do_action' we set the job status as 'done';
				// that means that we need to throw an exception if we want to set its
				// status as 'error'.
				// Right now if we have an error, the queue stop its execution: this
				// behaviour can be debatable.
				// Still, next time it starts, we skip the element with 'error' status
				// and go forward; it gives us the ability to have a single-error per
				// job-operation at the cost of depending on the job-frequency for
				// queue completion.
				do_action('aeria_do_job_' . $jobname, $job);

				if (!$this->updateJobStatus($job->id, 'done')) {
					throw new \Exception("Error during status update from "
						. "'{$job->status}' to 'in_progress' for Job '{$job->id}'");
				}
			}
		} catch (\Exception $e) {
			if (!is_null($curr_job)) {
				$this->updateJobStatus($curr_job->id, 'error', $e->getMessage());
			}
			// reset statuses of waiting jobs in case of error
			$this->updateMultiJobsStatus($job->id, 'waiting', 'new');
			Response::error("JobQueue Error: " . $e->getMessage());
		}
	}

}

class JobsQueue {

	public static function add($key, $options) {
		JobsQueueHandler::getInstance()->add($key, $options);
	}

	public static function start($options) {
		JobsQueueHandler::getInstance()->start($options);
	}

}