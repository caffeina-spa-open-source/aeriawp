<?php

namespace Aeria;

trait SyncablePostType {

	use Syncable;

	abstract public function postType() : string;

	public function getSyncMap() : array {
		return SyncMap::getMapping($this->tableName(), $this->postType());
	}

}