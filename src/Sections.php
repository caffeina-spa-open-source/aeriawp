<?php

namespace Aeria;

class SectionsHandler
{
    use Singleton;

    protected static $wpdb;
    protected static $sections_table;
    protected static $sections_fields_table;

    protected $sections_list = [];

    protected function getSectionsValue(int $post_id)
    {
        return DBScripts::call('SELECT::get_sections', [$post_id]);
    }

    protected function getSectionsGroupValue(int $post_id, string $section_group)
    {
        return DBScripts::call('SELECT::get_sections_group', [$post_id, $section_group]);
    }

    protected static function removeSection(int $post_id, array $section)
    {
        $id = $section['section_id'];
        $group = $section['sectiongroup'];
        // quick check that we are trying to delete the correct section
        $is_one = DBScripts::call(
            'SELECT::check_section',
            [$id, $post_id, $group]
        );
        if ($is_one->res != 1) {
            throw new \Exception('Section: cannot remove the section; wrong data '
                ."specified (id: '$id', post_id: '$post_id', group: '$group')");
        }

        // deleted section field
        do_action('aeria_delete_section', $id, $section);

        // delete sections fields
        static::$wpdb->delete(static::$sections_fields_table, ['section_id' => $id]);
        // delete section
        static::$wpdb->delete(static::$sections_table, ['id' => $id]);

        // NOTE: tables are in cascade, so the double query shouldn't be necessary;
        // anyway at the cost of one more query, we can handle unexpected situations
        // where the db cascade has been changed
    }

    protected static function updateSectionFieldData(int $section_id, array $field)
    {
        $field_id = isset($field['id']) ? $field['id'] : 0;

        // in case of nested-sections, we create new custom numeric ids; they will
        // be all negative numbers as they are not saved as different fields in db
        if ($field['type'] === 'nested-sections') {
            $nested_sections_value = json_decode($field['value'], true);

            if (! is_null($nested_sections_value)) {
                $new_indexed_id = 0;
                foreach ($nested_sections_value as $nsv_index => $nsv_val) {
                    --$new_indexed_id;
                    $nested_sections_value[$nsv_index]['section_id'] = $new_indexed_id;
                }
                $field['value'] = json_encode($nested_sections_value, JSON_NUMERIC_CHECK);
            }
        }

        $field_data = [
            'section_id' => $section_id,
            'fieldkey' => $field['key'],
            /* if WordPress already serializes values *\/
            'fieldvalue'		=> $field['value'],
            /* if WordPress don't serializes values */
            'fieldvalue' => maybe_serialize($field['value']),

            'updatedby_id' => get_current_user_id(),
        ];

        // NOTE: I could also use '$wpdb->replace' here, but I won't be able to add
        // a where statement to the update query.

        // insert
        if ($field_id == 0) {
            $field_data['createdby_id'] = get_current_user_id();
            $outcome = static::$wpdb->insert(static::$sections_fields_table, $field_data);
            if ($outcome === false) {
                throw new \Exception(
                    "Sections: error in section field insertion '".$field_data['fieldkey']."': ".static::$wpdb->last_error
                );
            }
            $field_id = static::$wpdb->insert_id;
        // update
        } else {
            $where = [
                'id' => $field_id,
                'section_id' => $section_id,
                'fieldkey' => $field_data['fieldkey'],
            ];

            $outcome = static::$wpdb->update(
                static::$sections_fields_table,
                $field_data,
                $where
            );
            if ($outcome === false) {
                throw new \Exception(
                    "Sections: error in section field update '".$field_data['fieldkey']."' (field id: '{$field_id}' : ".static::$wpdb->last_error.')'
                );
            }
        }

        // saved section field
        do_action('aeria_save_section_field', $field_id, $field_data);

        return $field_id;
    }

    protected static function updateSection(int $post_id, array $section)
    {
        $section_id = isset($section['section_id']) ? $section['section_id'] : 0;
        $section_group = $section['sectiongroup'];
        $section_data = [
            'post_id' => $post_id,
            'updatedby_id' => get_current_user_id(),
            'title' => $section['title'],
            'sectionorder' => $section['order'],
            'status' => $section['status'],
        ];
        $section_fields_data = isset($section['fields']) ? $section['fields'] : [];

        // NOTE: I could also use '$wpdb->replace' here, but I won't be able to add
        // a where statement to the update query.

        // insert
        if ($section_id <= 0) {
            $postmeta_id = DBScripts::call('GET::meta_id', [$post_id, $section_group]);
            if ($postmeta_id == null) {
                throw new \Exception("Sections: cannot find Meta data '$section_group' "
                    ."linked to Post '$post_id'");
            }
            $section_data['postmeta_id'] = $postmeta_id->meta_id;
            $section_data['sectiongroup'] = $section_group;
            $section_data['type'] = $section['type'];
            $section_data['createdby_id'] = get_current_user_id();
            $outcome = static::$wpdb->insert(static::$sections_table, $section_data);

            if ($outcome === false) {
                throw new \Exception('Sections: error in section insertion '
                    ."'$section_group': ".static::$wpdb->last_error);
            }
            $section_id = static::$wpdb->insert_id;
        // update
        } else {
            $where = [
                'id' => $section_id,
                'sectiongroup' => $section_group,
                'post_id' => $post_id,
            ];
            $outcome = static::$wpdb->update(static::$sections_table, $section_data, $where);
            if ($outcome === false) {
                throw new \Exception('Sections: error in section update '
                    ."'$section_group' (section id: '$section_id' : "
                    .static::$wpdb->last_error.')');
            }
        }

        if (! empty($section_fields_data)) {
            foreach ($section_fields_data as $field_value) {
                $field_data = [
                    'key' => $field_value['id'],
                    'type' => $field_value['type'],
                    'value' => $field_value['value'] ?? null,
                ];
                if (isset($field_value['section_field_id'])) {
                    $field_data['id'] = $field_value['section_field_id'];
                }
                static::updateSectionFieldData($section_id, $field_data);
            }
        }

        // saved section
        do_action('aeria_save_section', $section_id, $section_data);
    }

    protected function checkSectionsTables()
    {
        if (DBScripts::call('EXISTS::table', [static::$sections_table])->res == 0) {
            DBScripts::call('CREATE::aeria_sections');
        }
        if (DBScripts::call('EXISTS::table', [static::$sections_fields_table])->res == 0) {
            DBScripts::call('CREATE::aeria_sections_fields');
        }
    }

    /**
     * Small utility method: uses a passed-by-reference array as a cache to store
     * retrieved users.
     * NOTE: if WordPress already caches things in a smart way, this could be
     * useless.
     */
    protected function usersCache(int $user_id, array &$users_cache)
    {
        if (!isset($users_cache[$user_id])) {
            $users_cache[$user_id] = get_user_by('ID', $user_id);
        }

        return $users_cache[$user_id];
    }

    protected function mapDataToFields(array $section_data, array $section_fields): array
    {
        $mapping = [];
        $keys_values = [];
        foreach ($section_data as $el) {
            $keys_values[$el['key']] = $el;
        }

        foreach ($section_fields as $section_field) {
            if (isset($keys_values[$section_field['id']])) {
                $section_field['value'] = $keys_values[$section_field['id']]['value'];
                $section_field['section_field_id'] = $keys_values[$section_field['id']]['section_field_id'];
            }
            $mapping[] = $section_field;
        }

        return $mapping;
    }

    public function getSavedSections(
            int $post_id,
            array $sections_types = [],
            string $section_group = '')
    {
        $values = empty($section_group)
            ? $this->getSectionsValue($post_id)
            : $this->getSectionsGroupValue($post_id, $section_group);
        $saved_sections = [];

        $curr_section_id = 0;
        $curr_section = [];
        $curr_section_index = -1;

        // small users cache
        $users_cache = [];
        foreach ($values as $value) {
            if ($curr_section_id != $value->section_id) {
                $curr_section_id = $value->section_id;
                $created_by = $this->usersCache($value->section_createdby_id, $users_cache);
                $updated_by = $this->usersCache($value->section_updatedby_id, $users_cache);

                $curr_section = [
                    'section_id' => $value->section_id,
                    'type' => $value->section_type,
                    'postmeta_id' => $value->section_postmeta_id,
                    'post_id' => $value->section_post_id,
                    'sectionorder' => $value->section_sectionorder,
                    'title' => $value->section_title,
                    'sectiongroup' => $value->section_sectiongroup,
                    'status' => $value->section_status,
                    'createdby_id' => $created_by->get('user_login'),
                    'createdat' => $value->section_createdat,
                    'updatedby_id' => $updated_by->get('user_login'),
                    'updatedat' => $value->section_updatedat,
                    'data' => [],
                    'fields' => [],
                ];
                $saved_sections[] = $curr_section;
                ++$curr_section_index;
            }
            $field_created_by = $this->usersCache($value->field_createdby_id, $users_cache);
            $field_updated_by = $this->usersCache($value->field_updatedby_id, $users_cache);
            $field_data = [
                'section_field_id' => $value->field_id,
                'section_id' => $value->field_section_id,
                'key' => $value->field_key,
                'value' => maybe_unserialize($value->field_value),
                'createdby_id' => $field_created_by->get('user_login'),
                'createdat' => $value->field_createdat,
                'updatedby_id' => $field_updated_by->get('user_login'),
                'updatedat' => $value->field_updatedat,
            ];
            $field_data = apply_filters('aeria_section_field_'.$value->section_type.'_'.$value->field_key, $field_data);

            $saved_sections[$curr_section_index]['data'][] = $field_data;
        }
        if (!empty($sections_types)) {
            foreach ($saved_sections as $index => $saved_section) {
                if (!isset($sections_types[$saved_section['type']])
                        || !isset($sections_types[$saved_section['type']]['fields'])) {
                    continue;
                }

                $saved_sections[$index]['fields'] = $this->mapDataToFields(
                    $saved_section['data'],
                    $sections_types[$saved_section['type']]['fields']
                );
            }
        }

        return $saved_sections;
    }

    protected function __construct()
    {
        global $wpdb;

        if (Settings::isDisabled('modules.sections', false)) {
            throw new \Exception('Sections Error: Module Sections is disabled');
        }

        static::$wpdb = $wpdb;
        static::$sections_table = $wpdb->prefix.'aeria_sections';
        static::$sections_fields_table = $wpdb->prefix.'aeria_sections_fields';

        DBScripts::load(__DIR__.'/db/AeriaSections.sql');

        $this->checkSectionsTables();

        /*
         * Handle Sections-data saving when saving the post
         */
        Action::add('save_post_meta', function ($arguments) {
            $post_id = $arguments[0];
            $meta_data = $arguments[1];
            $args = Params::post();

            // Fill data from original post during post duplication
            if (isset($_REQUEST['action'], $_REQUEST['post']) 
                    && $_REQUEST['action'] === 'duplicate_post_clone') {
                $origin = (int) $_REQUEST['post'];

                // Get all the section groups
                $args['aeria--sections'] = array_unique(
                    array_column(Sections::getSavedSections($origin), 'sectiongroup')
                );

                Filter::add(
                    'duplicate_post_meta_keys_filter', 
                    fn ($metas) => array_diff($metas, $args['aeria--sections'])
                );

                foreach ($args['aeria--sections'] as $group) {
                    $sections = get_post_meta($origin)[$group][0] ?? [];

                    // Add the "create" action to all the sections
                    $sections = json_decode($sections);
                    foreach ($sections as $key => $section) {
                        $sections[$key]->action = 'create';
                        $sections[$key]->post_id = $post_id;

                        foreach ($section->data as $fieldKey => $field) {
                            unset($sections[$key]->data[$fieldKey]->section_field_id);
                        }

                        foreach ($section->fields as $fieldKey => $field) {
                            unset($sections[$key]->fields[$fieldKey]->section_field_id);
                        }
                    }

                    // Add the sections meta to the new post
                    update_post_meta($post_id, $group, json_encode($sections));
                    
                    $args[$group] = addslashes(json_encode($sections));
                }
            }
        
            $sections_boxes = $args['aeria--sections'] ?? [];

            foreach ($sections_boxes as $index => $section_name) {
                if (!isset($args[$section_name])) {
                    throw new \Exception("Aeria\Sections: Unknown Section `$section_name`");
                }
                $section_raw_data = $args[$section_name];
                $sections = (!empty($section_raw_data))
                    ? json_decode(stripslashes($section_raw_data), true)
                    : [];
                $sections_order_counter = 0;
                foreach ($sections as $section) {
                    $action = $section['action'] ?? null;
                    switch ($action) {
                        case 'create':
                            $section['section_id'] = 0;
                            $section['order'] = $sections_order_counter++;
                            static::updateSection($post_id, $section);
                            break;

                        case 'delete':
                            static::removeSection($post_id, $section);
                            break;

                        case 'update':
                        default:
                            $section['order'] = $sections_order_counter++;
                            static::updateSection($post_id, $section);
                            break;
                    }
                }
            }
        });

        /*
         * Get Sections Fields & Data
         */
        Ajax::add('get_sections_data', function () {
            $args = Params::request();
            if (! isset($args['post_id'])) {
                Response::error("No 'post_id' specified.");
            } elseif (! is_int($args['post_id'])) {
                Response::error("'post_id' must be a int.");
            }

            $post_id = $args['post_id'];

            $post = get_post($post_id);
            if ($post === null) {
                Response::error("Couldn't find the Post with ID '{$post_id}'");
            }

            $sections_types = $this->getRegisteredSections($post->post_type);
            $saved_sections = $this->getSavedSections($post_id, $sections_types);

            $response = [
                'sections_types' => $sections_types,
                'saved_sections' => $saved_sections,
            ];

            Response::success($response, "post_id: {$post_id}");
        });

        // Set window.aeriaSectionTypes inside the page
        Action::add('admin_enqueue_scripts', function ($hook) {
            if (!in_array($hook, ['post.php', 'post-new.php'])) {
                return;
            }
            $screen = get_current_screen();
            $section_types = $this->getRegisteredSections($screen->post_type);
            $section_types = empty($section_types)
                ? '{}'
                : json_encode($section_types, JSON_NUMERIC_CHECK);
            echo '<script>window.aeriaSectionTypes = '.$section_types.'</script>';
        });

        /*
         * Render specific data for sections fields
         */
        Action::add(
            'aeria_render_field_sections',
            function ($args) {
                $field_name = $args['metabox_id'].'--'.$args['meta_field']['id'];
                echo '
					<input
						type="hidden"
						name="aeria--sections[]"
						value="'.$field_name.'"/>';
            },
            2 // num arguments
        );

        /*
         * Add filter to pre-format sections data inside the metaboxes
         */
        Filter::add(
            'aeria_format_meta_sections',
            function (...$args) {
                $value = $args[0];
                $other_args = $args[1];

                $post_id = $other_args['post_id'];
                if (isset($other_args['post_type'])) {
                    $post_type = $other_args['post_type'];
                } else {
                    $post_type = get_post_type($post_id);
                }
                $section_group = $other_args['metabox_id'].'--'.$other_args['meta_field']['id'];

                $sections_types = $this->getRegisteredSections($post_type);
                $saved_sections = $this->getSavedSections($post_id, $sections_types, $section_group);

                return json_encode($saved_sections, JSON_NUMERIC_CHECK);
            },
            2 // num arguments
        );
    }

    protected function getRegisteredSections(string $post_type)
    {
        $base = [];
        if ($post_type !== '*' && isset($this->sections_list['*'])) {
            $base = $this->sections_list['*'];
        }
        if (!isset($this->sections_list[$post_type])) {
            $section_types = $base;
        } else {
            $section_types = array_merge($base, $this->sections_list[$post_type]);
        }

        $indexed_section_types = [];
        foreach ($section_types as $section_type) {
            $indexed_section_types[$section_type['id']] = $section_type;
        }

        return $indexed_section_types;
    }

    protected function registerSection(string $post_type, array $o)
    {
        if (!isset($this->sections_list[$post_type])) {
            $this->sections_list[$post_type] = [];
        }

        $section = [
            'id' => $o['id'],
            'post_type' => $post_type,
            'label' => $o['label'],
            'fields' => $o['fields'],
        ];

        $this->sections_list[$post_type][] = $section;
    }

    public function getSectionFields(int $section_id)
    {
        return array_map(
            function ($section_field) {
                $field_key = $section_field->field_key;
                $section_field = apply_filters(
                    'aeria_format_section_field',
                    $section_field
                );

                return apply_filters(
                    'aeria_format_section_field_'.$section_field->section_sectiongroup.'--'.$field_key,
                    $section_field
                );
            },
            DBScripts::call(
                'SELECT::get_section_fields',
                [$section_id]
            )
        );
    }

    public function getSection(int $section_id)
    {
        $section = apply_filters('aeria_format_section', DBScripts::call('SELECT::get_section', [$section_id]));

        if (empty($section)) {
            return $section;
        }

        return apply_filters(
            "aeria_format_section_{$section->section_type}",
            $section
        );
    }

    public function register($o)
    {
        try {
            if (! isset($o['id'])) {
                throw new \Exception("Missing attribute 'id'");
            }

            $id = Utils::getParam($o, 'id');

            $check_for_all = Utils::getParam($o, 'types');
            if ($check_for_all === 'all' || $check_for_all === '*') {
                $post_types = ['*'];
            } else {
                $post_types = Utils::getPluralParam($o, 'type', 'types');
            }
            if (empty($post_types)) {
                $post_types = ['*'];
            }

            $label = Utils::getParam($o, 'label', '');
            $fields = Utils::getParam($o, 'fields', []);

            if (empty($fields)) {
                throw new \Exception("Section '$id': No Fields specified");
            }

            /*
             * Register Section to all post_types
             */
            foreach ($post_types as $post_type) {
                $this->registerSection($post_type, [
                    'id' => $id,
                    'label' => $label,
                    'fields' => $fields,
                ]);
            }
        } catch (\Exception $e) {
            wp_die('Section Error: '.$e->getMessage());
        }
    }
}

class Sections
{
    public static function register($o)
    {
        SectionsHandler::getInstance()->register($o);
    }

    public static function getSavedSections(int $post_id)
    {
        return SectionsHandler::getInstance()->getSavedSections($post_id);
    }

    public static function getSection(int $section_id)
    {
        return SectionsHandler::getInstance()->getSection($section_id);
    }

    public static function getSectionFields(int $section_id)
    {
        return SectionsHandler::getInstance()->getSectionFields($section_id);
    }
}
