<?php

namespace Aeria;

class Utils {

	/**
	 * Retrieve the param from an array; it returns the specified default value
	 * if it's not found; it also handles special cases, like 'callable'
	 * attributes (in which it will call the function with the specified
	 * arguments).
	 * @param  array  $src          the object containing the param
	 * @param  string $index        the param index or key
	 * @param  mixed  $defaultValue default value
	 * @return mixed                the retrieved value
	 */
	public static function getParam(
			array $src,
			string $index,
			$default_value = null) {
		if (!isset($src[$index])) {
			return $default_value;
		}

		$value = $src[$index];
		if (static::isCallableParam($value)) {
			$method_name = Utils::cut($value, '@', '(');
			// TODO: handle commas inside strings?
			$method_arguments = explode(',', Utils::cut($value, '(', ')'));
			$args = [];
			foreach ($method_arguments as $argument) {
				$arg = trim($argument);
				$args[] = static::getParam([ $arg ], 0, $arg);
			}
			return $method_name(...$args);
		}

		return $value;
	}

	/**
	 * Combines both singular and plural configuration attributes in an array.
	 * This is better explained with an example; say we have a config array
	 * where we can both specify 'type' (a single type) or 'types' (a list of
	 * types); as it's useful to reduce to problem to a single case (the one with
	 * a list of types, so the single case is mapped by creating a list with only
	 * one element), we also need to handle when both are passed.
	 * This method do this:
	 * 	- if only the single key is specified, an array with the single value is
	 * 		returned;
	 * 	- if only the plural key is specified, the list is returned;
	 *  - if both are specified, they are combined.
	 * @param  array  $src         				the object containing the param(s)
	 * @param  string $singular_key 			singular element key
	 * @param  string $plural_key   			multiple elements key
	 * @param  bool 	$remove_duplicates	tells us if we need to remove duplicates
	 *                                 		or not (default: true)
	 * @return array               				combination of singular and key
	 */
	public static function getPluralParam(
			array		$src,
			string	$singular_key,
			string	$plural_key,
			bool		$remove_duplicates = true) {
		$value = [];

		$single = static::getParam($src, $singular_key);
		if (!is_null($single)) {
			$value[] = $single;
		}

		$multi = static::getParam($src, $plural_key, []);
		if (!is_array($multi)) {
			throw new \Exception("'{$plural_key}' must be an Array");
		}

		if ($remove_duplicates) {
			return array_unique(array_merge($value, $multi));
		}

		return array_merge($value, $multi);
	}

	/**
	 * Flat callable params.
	 * @param  array &$obj config object to flat
	 */
	public static function flatCallParams(array &$obj) {
		foreach ($obj as $key => &$val) {
			if (static::isCallableParam($val)) {
				$val = static::getParam($obj, $key, $val);
			} else if (is_array($val)) {
				static::flatCallParams($val);
			}
		}
	}

	/**
	 * Check if the passed variable is a 'callable param'; a callable param is an
	 * associative array with:
	 * 	- a 'callable' key, that is a callable string;
	 * 	- optionally an 'args' key, that can both be an array of arguments or a
	 * 		single argument to pass when calling the callable;
	 * 	- no other keys.
	 * @param  array    $obj  the maybe callable object
	 * @return boolean        true if it's callable, false otherwise
	 */
	public static function isCallableParam($obj) : bool {
		if (!is_string($obj)) {
			return false;
		}

		if (!Utils::startsWith($obj, '@')) {
			return false;
		}

		if (strpos($obj, '(') === false || strpos($obj, ')') === false) {
			return false;
		}

		$method_name = Utils::cut($obj, '@', '(');
		if (!is_callable($method_name)) {
			return false;
		}

		return true;
	}

	/**
	 * map prefixed data, filtering by a list of prefixes; if domains is false
	 * instead of an array of prefixes, all prefixes are accepted (only
	 * constraint: must have a '--' prefix); an optional array of keys to exclude
	 * from the process can be passed; if $no_prefix is passed as true, the prefix
	 * will be removed from the mapped keys.
	 * @param  array  $data       data to map
	 * @param  mixed  $domains    list of domains or false
	 * @param  array  $exclude    list of elements to exlude from mapping
	 * @param  bool   $no_prefix  remove prefix from mapped keys
	 * @return array              mapped array
	 */
	public static function mapPrefixedData(
			array $data,
			$domains = false,
			array $exclude = [],
			bool $no_prefix = false) {
		$mapped = [];

		$all_domains = $domains === false;
		$domains = is_array($domains) 
			? $domains 
			: [];

		foreach ($data as $key => $value) {
			if (in_array($key, $exclude)) {
				continue;
			}

			$pieces = explode("--", $key, 2);
			if (count($pieces) != 2) {
				continue;
			}

			$prefix = $pieces[0];
			if ($all_domains || in_array($prefix, $domains)) {
				if (!isset($mapped[$prefix])) {
					$mapped[$prefix] = [];
				}
				if ($no_prefix) {
					$mapped[$prefix][str_replace($prefix . '--', '', $key)] = $value;
				} else {
					$mapped[$prefix][$key] = $value;
				}
			}
		}

		return $mapped;
	}

	// -- strings

	/**
	 * NOTE: Method taken from Core; add Core as a dependency?
	 */
	public static function cut($text, $start_tag, $end_tag = null): string
	{
		$_s = strlen($start_tag) + strpos($text, $start_tag);
		
		return $end_tag
			? substr($text, $_s, strpos($text, $end_tag, $_s) - $_s)
			: substr($text, $_s);
  	}

	/**
	 * Classic startsWith function: check if a string starts with the given
	 * substring.
	 * @param  string $str       Source string
	 * @param  string $prefix    Substring to check
	 * @return bool              true/false
	 */
	public static function startsWith(string $str, string $prefix) : bool {
		return (substr($str, 0, strlen($prefix)) == $prefix);
	}

	/**
	 * Classic endsWith function: check if string ends with the given
	 * substring.
	 * @param  string $str       Source string
	 * @param  string $suffix   Substring to check
	 * @return bool              true/false
	 */
	public static function endsWith(string $str, string $suffix) : bool {
		return (substr($str, -1 * strlen($suffix)) == $suffix);
	}

	/**
	 * Classic contains function: check if string contains the given needle.
	 * @param  string $str       Source string
	 * @param  string $needle    Substring to check
	 * @return bool              true/false
	 */
	public static function contains(string $str, string $needle) : bool {
		return strpos($str, $needle) !== false;
	}

	/**
	 * Quick studly method: converts all dashes and underscores ('-', '_') to
	 * spaces, then does an 'ucwords' on the string and lastlyremoves all spaces.
	 * @param  string $str Input string
	 * @return string      Studlied string
	 */
	public static function studly(string $str) {
		return str_replace(' ', '', ucwords(str_replace(['_', '-'], ' ', $str)));
	}

}