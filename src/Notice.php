<?php

namespace Aeria;

/**
 * Handle notices on top of admin post-types pages.
 */
class Notice {

	public static function format(
			string $message,
			string $class_name = 'updated',
			bool $dismissible = false) : string {
		return '<div class="notice '
			. $class_name
			. ($dismissible ? ' is-dismissible' : '')
			. '"><p>'
			. $message
			. '</p></div>';
	}

	public static function render(
			string $message,
			string $class_name = 'updated',
			bool $dismissible = false) {
		echo static::format($message, $class_name, $dismissible);
	}

	/**
	 * Register a new Notice to show on top of post-types admin pages. Accept
	 * one argument specifyng all the notice options.
	 */
	public static function register($o) {
		$o = Conf::load($o);
		if (!isset($o['type']) && !isset($o['types'])) {
			throw new \Exception("Missing attribute 'type' in Notice definition");
		}

		$post_types = Utils::getPluralParam($o, 'type', 'types');

		$notice_data = [
			'message'			=> Utils::getParam($o, 'message'),
			'class'				=> Utils::getParam($o, 'class', 'updated'),
			'dismissible'	=> Utils::getParam($o, 'dismissible', false)
		];

		if (empty($notice_data['message'])) {
			throw new \Exception("Missing attribute 'message' in Notice definition");
		}

		$taxonomies = Utils::getPluralParam($o, 'taxonomy', 'taxonomies');

		Action::add('admin_notices', function()
				use ($post_types, $notice_data, $taxonomies) {

			global $pagenow;

			$args = Params::get();

			if (in_array($pagenow, ['edit.php', 'edit-tags.php'])
					&& (empty($taxonomies) || in_array($args['taxonomy'], $taxonomies))
					&& in_array($args['post_type'], $post_types)) {

				switch ($notice_data['class']) {
					case 'warning':
					case 'error':
					case 'info':
						$notice_data['class'] = 'notice-' . $notice_data['class'];
						break;
				}
				static::render(
					$notice_data['message'],
					$notice_data['class'],
					$notice_data['dismissible']
				);

			}

		});
	}

}