<?php

namespace Aeria;

/**
 * Register scripts and styles and add them to the app when needed.
 * 
 * NOTE: it needs the 'PUBLIC_URL' env variable setted, as it's used as the
 * prefix of scripts and styles.
 */
class AssetsHandler {

	use Singleton;

	protected $scripts_list = [];
	protected $styles_list = [];

	protected function __construct() {
		Action::add('admin_enqueue_scripts', function() {
			$base_url = getenv('PUBLIC_URL') !== false ? getenv('PUBLIC_URL') : '';
			// enqueue scripts
			foreach ($this->scripts_list as $script_name => $script_data) {
				$script_url = $script_data['url'];
				$opts = isset($script_data['opts']) ? $script_data['opts'] : [];
				$script_deps = isset($opts['deps']) ? $opts['deps'] : [];
				$script_ver = isset($opts['ver']) ? $opts['ver'] : false;
				$script_in_footer = isset($opts['in_footer'])
					? $opts['in_footer']
					: false;
				wp_enqueue_script(
					$script_name,
					$base_url . $script_url,
					$script_deps,
					$script_ver,
					$script_in_footer
				);
			}
			// enqueue styles
			foreach ($this->styles_list as $style_name => $style_data) {
				$style_url = $style_data['url'];
				$opts = isset($style_data['opts']) ? $style_data['opts'] : [];
				$style_deps = isset($opts['deps']) ? $opts['deps'] : [];
				$style_ver = isset($opts['ver']) ? $opts['ver'] : false;
				$style_media = isset($opts['media']) ? $opts['media'] : 'all';
				wp_enqueue_style(
					$style_name,
					$base_url . $style_url,
					$style_deps,
					$style_ver,
					$style_media
				);
			}
		});
	}

	protected function & getQueue(string $queue_name) {
		$queue = null;
		switch ($queue_name) {
			case 'scripts':
				$queue = &$this->scripts_list;
				break;

			case 'styles':
				$queue = &$this->styles_list;
				break;

			default:
				throw new \Exception("Unknown '$queue_name' queue");
		}

		return $queue;
	}

	/**
	 * Add an element to the specified list, if the element isn't already in list;
	 * it uses the element name to choose if it must be ignored or not.
	 * @param string $queue_name	name of the queue to use
	 * @param string $name 				the element name
	 * @param string $url  				the url to the element, relative to the
	 *                         		'PUBLIC_URL' env setting
	 * @param array $opts  				other options to pass to the final wp method to
	 *                         		call ('wp_enqueue_script' for scripts,
	 *                         		'wp_enqueue_style' for styles, etc.)
	 */
	public function add(
			string $queue_name,
			string $name,
			string $url = '',
			array  $opts = []) {
		$queue = &$this->getQueue($queue_name);
		if (!isset($queue[$name])) {
			$queue[$name] = ['url' => $url, 'opts' => $opts];
		}
	}

	/**
	 * Remove an element from the specified list, searching it by name.
	 * @param string $queue_name	name of the queue to use
	 * @param string $name 				the element name
	 */
	public function remove(string $queue_name, string $name) {
		$queue = &$this->getQueue($queue_name);
		if (isset($queue[$name])) {
			unset($queue[$name]);
		}
	}

	/**
	 * Replace an already added element in the specified list with a new one.
	 * @param string $queue_name	name of the queue to use
	 * @param string $name 				the element name
	 * @param string $url  				the new url
	 * @param array $opts  				new options
	 */
	public function replace(
			string $queue_name,
			string $name,
			string $url = '',
			array  $opts = []) {
		$queue = &$this->getQueue($queue_name);
		$queue->remove($name);
		$queue->add($name, $url, $opts);
	}

	/**
	 * Remove all added elements to the specified queue.
	 */
	public function clear(string $queue_name) {
		$queue = &$this->getQueue($queue_name);
		$queue = [];
	}

}

/**
 * Small utility class to enqueue assets (scripts or styles) on admin pages.
 */
class Assets {

	# scripts

	/**
	 * Add a script to the scripts list, if the script wasn't already in list; it
	 * uses the script name to choose if the script must be ignored or not.
	 * @param string $name the script name
	 * @param string $url  the url to the script, relative to the 'PUBLIC_URL'
	 *                     env setting
	 * @param array $opts  other options for wp_enqueue_script (deps, ver,
	 *                     in_footer)
	 */
	public static function addScript(string $name, string $url = '', array $opts = []) {
		AssetsHandler::getInstance()->add('scripts', $name, $url, $opts);
	}

	/**
	 * Remove a script from the list, searching it by name.
	 * @param  string $name the script name
	 */
	public static function removeScript(string $name) {
		AssetsHandler::getInstance()->remove('scripts', $name);
	}

	/**
	 * Replace an already added script in the list with a new one (in fact it
	 * changes its url)
	 * @param string $name the script name
	 * @param string $url  the new url
	 * @param array $opts  other options for wp_enqueue_script (deps, ver,
	 *                     in_footer)
	 */
	public static function replaceScript(string $name, string $url = '', array $opts = []) {
		AssetsHandler::getInstance()->replace('scripts', $name, $url, $opts);
	}

	/**
	 * Remove all added scripts.
	 */
	public static function clearScripts() {
		AssetsHandler::getInstance()->clear('scripts');
	}

	# styles

	/**
	 * Add a style to the styles list, if the style wasn't already in list; it
	 * uses the style name to choose if the style must be ignored or not.
	 * @param string $name the style name
	 * @param string $url  the url to the style, relative to the 'PUBLIC_URL'
	 *                     env setting
	 * @param array $opts  other options for wp_enqueue_script (deps, ver,
	 *                     in_footer)
	 */
	public static function addStyle(string $name, string $url = '', array $opts = []) {
		AssetsHandler::getInstance()->add('styles', $name, $url, $opts);
	}

	/**
	 * Remove a style from the list, searching it by name.
	 * @param  string $name the style name
	 */
	public static function removeStyle(string $name) {
		AssetsHandler::getInstance()->remove('styles', $name);
	}

	/**
	 * Replace an already added style in the list with a new one (in fact it
	 * changes its url)
	 * @param string $name the style name
	 * @param string $url  the new url
	 * @param array $opts  other options for wp_enqueue_script (deps, ver,
	 *                     in_footer)
	 */
	public static function replaceStyle(string $name, string $url = '', array $opts = []) {
		AssetsHandler::getInstance()->replace('styles', $name, $url, $opts);
	}

	/**
	 * Remove all added styles.
	 */
	public static function clearStyles() {
		AssetsHandler::getInstance()->clear('styles');
	}

}