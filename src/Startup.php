<?php

namespace Aeria;

class StartupFactory {

	public $key;

	public $class_name;

	public $order;

	public $path = '';

	public function __construct(string $key, string $class_name, int $order = 999) {
		$this->key = $key;
		$this->class_name = $class_name;
		$this->order = $order;
	}

}

class StartupFactoryList {

	private $factories = [];

	private $factories_index = [];

	public function __construct($base_factories) {
		if (!is_array($base_factories)) {
			$base_factories = [$base_factories];
		}
		foreach ($base_factories as $factory) {
			$this->register($factory);
		}
	}

	public function register(StartupFactory $factory) {
		$this->factories[] = $factory;
		$this->factories_index[$factory->key] = count($this->factories) - 1;
		$this->reorder();
	}

	protected function reorder() {
		usort($this->factories, function($a, $b) {
			return ($a->order < $b->order
				? -1
				: ($a->order > $b->order
					? 1
					: 0
				)
			);
		});
	}

	public function setPath(string $factory_name, string $path) {
		if (!$this->hasFactory($factory_name)) {
			throw new \Exception("Unknown StartupFactory '$factory_name'");
		}
		$this->factories[$this->factories_index[$factory_name]]->path = $path;
	}

	public function hasFactory(string $factory_name) {
		return isset($this->factories_index[$factory_name]);
	}

	public function getOrderedFactories() {
		$this->reorder();
		return $this->factories;
	}

}

class StartupHandler {

	use Singleton;

	private $factories;

	const DISABLEABLE = ['sections', 'syncmaps'];

	protected function __construct() {
		$this->factories = new StartupFactoryList([
			new StartupFactory('SyncMaps', 		'Aeria\SyncMap',	0),
			new StartupFactory('Types', 			'Aeria\Type', 		1),
			new StartupFactory('Taxonomies', 	'Aeria\Taxonomy', 2),
			new StartupFactory('Sections', 		'Aeria\Sections',	3),
			new StartupFactory('Metas', 			'Aeria\Meta', 		4),
			new StartupFactory('Notices', 		'Aeria\Notice',		5),
			new StartupFactory('Options', 		'Aeria\Options',	6),
			new StartupFactory('Reorders', 		'Aeria\Reorder',	7),
			new StartupFactory('Tables',	 		'Aeria\Table',		8)
		]);
	}

	public function register(StartupFactory $new_factory) {
		$this->factories->register($new_factory);
	}

	public function setup(array $opts, string $prefix = '') {
		foreach ($opts as $aeria_element => $path) {
			if (!$this->factories->hasFactory($aeria_element)) {
				throw new \Exception("Unknown Aeria element '{$aeria_element}'");
			} else if (!is_string($path) || !is_dir($prefix . $path)) {
				throw new \Exception("Unknown Startup setting for '{$aeria_element}': "
						. " '{$path}' is not a valid path");
			}

			$this->factories->setPath($aeria_element, $prefix . $path);
		}
	}

	public function run() {
		// Enables Optimizations
		if (Settings::isEnabled('modules.optimize', true)) {
			Optimize::enable();
		}
		// Startup Factories
		$factories = $this->factories->getOrderedFactories();
		foreach ($factories as $factory) {
			$class_name = $factory->class_name;
			if (in_array($class_name, static::DISABLEABLE)
					&& Settings::isDisabled('modules.' . strtolower($class_name), false)) {
				continue;
			}
			if (!empty($factory->path)) {
				$element_path = $factory->path . (Utils::endsWith($factory->path, '/') ? '*' : '/*');
				$elements_list = (array)glob($element_path);
				foreach ($elements_list as $def_file) {
					try {
						$o = Conf::load($def_file);

						$class_name::register($o);
					} catch (\Exception $e) {
					    wp_die('Startup Error: ' . $e->getMessage());
					}
				}
			}
		}
	}

}

class Startup {

	public function run(array $opts, string $prefix = '') {
		StartupHandler::getInstance()->setup($opts, $prefix);
		StartupHandler::getInstance()->run();
	}

}