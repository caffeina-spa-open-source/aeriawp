<?php

namespace Aeria;

/**
 * Utility class to handle menu elements; this is useful as some method
 * (i.e.: 'add_submenu_page') are available only when hooking to a WordPress
 * event; so, to avoid to specify the hook everytime we want to add a menu
 * element, we do it once and we collect the menu pages we want 'til then.
 */
class MenuHandler {

	use Singleton;

	protected 	$to_add = [],
				$to_remove = [],
				$to_replace = [],
				$order_override = [],
				$menu,
				$submenu;

	protected function __construct() {
		global $menu, $submenu;

		$this->menu = &$menu;
		$this->submenu = &$submenu;

		Action::add('admin_menu', function() {
			// replace voices
			foreach ($this->to_replace as $voice_data) {
				$target = $voice_data['to_replace'];
				$opts = $voice_data['with'];
				$voice = Utils::getParam($target, 'voice');
				$level = Utils::getParam($target, 'level', null);
				$data = $this->findMenuItem($voice, $level);
				// if there is no menu-voice to replace, this works as add
				if ($data !== false) {
					if ($data['level'] == 0 && !isset($opts['position'])) {
						$opts['position'] = $data['position'];
					} else {
						$opts['parent'] = $data['parent'];
					}
					$this->remove($voice, $data['level']);
				}
				$this->add($opts);
			}

			// remove voices
			foreach ($this->to_remove as $voice_data) {
				$voice = Utils::getParam($voice_data, 'voice');
				$level = Utils::getParam($voice_data, 'level', null);
				$menu_element = $this->findMenuItem($voice, $level);
				if ($menu_element === false) {
					continue;
				}
				if ($menu_element['level'] == 0) {
					if (function_exists('remove_menu_page')) {
						remove_menu_page($menu_element['slug']);
						if (isset($this->submenu[$menu_element['slug']])) {
							foreach ($this->submenu[$menu_element['slug']] as $sub_voice) {
								// second element is the menu-voice's slug
								remove_submenu_page($menu_element['slug'], $sub_voice[2]);
							}
						}
					}
				} else {
					if (function_exists('remove_menu_page')) {
						remove_submenu_page($menu_element['parent'], $menu_element['slug']);
					}
				}
			}

			// add voices
			$top_menu_voices = [];
			$sub_menu_voices = [];
			$sub_order_reference = [];
			foreach ($this->to_add as $opts) {
				$menu_element = $this->minData($opts);
				array_shift($this->to_add);

				if (in_array($menu_element['slug'], wp_list_pluck($this->to_add, 'slug'))) {
					throw new \Exception(
						"Menu: there is already a '".$menu_element['slug']."' page in the menu"
					);
				}

				$parent = Utils::getParam($opts, 'parent');
				if ($parent == null) {
					$menu_element['icon'] = Utils::getParam($opts, 'icon', null);
					$top_menu_voices[] = $menu_element;
				} else {
					$parent = static::menuSlug($parent);
					$menu_element['parent'] = $parent;
					$sub_menu_voices[] = $menu_element;
					if (!isset($sub_order_reference[$parent])) {
						$sub_order_reference[$parent] = [
							'firsts'	=> [],
							'lasts'		=> []
						];
					}
					if ($menu_element['position'] === 0) {
						$sub_order_reference[$parent]['firsts'][] = $menu_element['slug'];
					} else if ($menu_element['position'] == null) {
						$sub_order_reference[$parent]['lasts'][] = $menu_element['slug'];
					}
				}
			}

			foreach ($top_menu_voices as $menu_element) {
				add_menu_page(
					$menu_element['title'],
					$menu_element['menutitle'],
					$menu_element['capability'],
					$menu_element['slug'],
					$menu_element['view'],
					$menu_element['icon'],
					$menu_element['position']
				);
			}
			foreach ($sub_menu_voices as $menu_element) {
				add_submenu_page(
					$menu_element['parent'],
					$menu_element['title'],
					$menu_element['menutitle'],
					$menu_element['capability'],
					$menu_element['slug'],
					$menu_element['view']
				);
			}

			// to sort submenus in WordPress we need to add a filter and resort
			// manually the already generated $submenu object;
			// NOTE: to keep it simple, in submenus we only handle first and last
			// specifications
			$submenu = &$this->submenu;
			Filter::add('custom_menu_order', function($menu_order)
					use (&$submenu, $sub_order_reference) {
				foreach ($submenu as $top_menu => &$sub_voices) {
					if (!isset($sub_order_reference[$top_menu])) {
						continue;
					}
					$ref = $sub_order_reference[$top_menu];
					usort($sub_voices, function($a, $b) use ($ref) {
						$a_title = $a[2];
						$b_title = $b[2];

						$a_pos = 1;
						if (in_array($a_title, $ref['firsts'])) {
							$a_pos = 0;
						} else if (in_array($a_title, $ref['lasts'])) {
							$a_pos = 2;
						}

						$b_pos = 1;
						if (in_array($b_title, $ref['firsts'])) {
							$b_pos = 0;
						} else if (in_array($b_title, $ref['lasts'])) {
							$b_pos = 2;
						}

						if ($a_pos < $b_pos) {
							return -1;
						} else if ($a_pos > $b_pos) {
							return 1;
						}

						return 0;
					});
				}

				return true;
			});

			// to sort menus, we need to pray the gods of blood and terror.
			Filter::add('menu_order', function($ordered_menu_voices) {
				if (empty($this->order_override)) {
					return $ordered_menu_voices;
				}

				$last_index = count($ordered_menu_voices) - 1;
				$to_override = [];
				$grouped_override = [];

				$separators_plus_index = 0;
				$separators_plus_indexes = [];

				foreach ($ordered_menu_voices as $voice) {
					if (strpos($voice, 'separator') !== false) {
						$separators_plus_index++;
					}
					$separators_plus_indexes[] = $separators_plus_index;
				}

				foreach ($this->order_override as $voice => $position) {
					if (is_null($position)) {
						$position = $last_index + 1;
					} else {
						$position = $position
							+ (isset($separators_plus_indexes[$position])
								? $separators_plus_indexes[$position]
								: 0);
					}
					if (!isset($grouped_override[$position])) {
						$grouped_override['p' . $position] = [];
					}
					$grouped_override['p' . $position][] = $voice;
					$to_override[$voice] = true;
				}

				$encountered = [];
				$override_index = 0;

				$new_order = [];
				for ($i = 0; $i < $last_index; ++$i) {
					$original_voice = $ordered_menu_voices[$i];
					$skip = isset($to_override[$original_voice]);

					if (!$skip && strpos($original_voice, 'separator') !== false) {
						$new_order[] = $original_voice;
						$skip = true;
					}

					while (isset($grouped_override['p' . $i])
							&& !empty($grouped_override['p' . $i])) {
						$new_order[] = array_shift($grouped_override['p' . $i]);
					}

					if (!$skip) {
						$new_order[] = $original_voice;
					}
				}

				return $new_order;
			});

		});
	}

	protected function orderVoice(string $voice, $position = null) 
	{
		if ($position === 'first') {
			$this->order_override[$voice] = 0;
		} else if ($position === 'last') {
			$this->order_override[$voice] = null;
		} else {
			$this->order_override[$voice] = $position;
		}
	}

	public function order($voices, $position = null) 
	{
		if (is_string($voices)) {
			$this->orderVoice($voices, $position);
		} else if (is_array($voices)) {
			foreach ($voices as $voice_key => $voice_value) {
				if (is_numeric($voice_key)) {
					$this->orderVoice($voice_value, $voice_key);
				} else if (is_string($voice_key)
						&& (
							is_numeric($voice_value)
							|| in_array($voice_value, [ 'first', 'last' ] )
						)) {
					$this->orderVoice($voice_value, $voice_key);
				} else {
					throw new \Exception("Cannot decide position of Menu voice "
						. "'{$voice_key}'");
				}
			}
		}
	}

	protected static function menuSlug(string $voice) 
	{
		if (Utils::startsWith($voice, 'post_type=')) {
			$voice = 'edit.php?' . $voice;
		} else if (Utils::startsWith($voice, 'taxonomy=')) {
			$voice = 'edit-tags.php?' . $voice;
		} else if (strpos($voice, '.php') === false) {
			$voice .= '.php';
		}
		return $voice;
	}

	protected function findMenuItem($voice, $level = null) 
	{
		$voice = static::menuSlug($voice);
		if ($level === 0 || $level == null) {
			foreach ($this->menu as $index => $menu_voice) {
				// second element is the menu-voice's slug
				if ($menu_voice[2] == $voice) {
					return [
						'position'	=> $index,
						'level'			=> 0,
						'slug'			=> $voice,
						'data'			=> $menu_voice
					];
				}
			}
		}
		if ($level == 1 || $level == null) {
			foreach ($this->submenu as $group => $menu_group) {
				foreach ($menu_group as $index => $menu_voice) {
					if (html_entity_decode($menu_voice[2]) == html_entity_decode($voice)) {
						return [
							'position'	=> $index,
							'level'			=> 1,
							'parent'		=> $group,
							'slug'			=> $voice,
							'data'			=> $menu_voice
						];
					}
				}
			}
		}
		return false;
	}

	protected function menuItemExists($voice, $level = null) 
	{
		return $this->findMenuItem($voice, $level) !== false;
	}

	protected function minData(array $opts)
	{
		if (! isset($opts['title'])) {
			throw new \Exception("'title' needed to define a new Menu voice");
		}
		if (! isset($opts['slug'])) {
			throw new \Exception("'slug' needed to define a new Menu voice");
		}
		if (! isset($opts['capability'])) {
			throw new \Exception("'capability' needed to define a new Menu voice");
		}

		$data = [];
		$data['title'] = Utils::getParam($opts, 'title');
		$data['slug'] = static::menuSlug(Utils::getParam($opts, 'slug'));
		$data['capability'] = Utils::getParam($opts, 'capability');

		$view = Utils::getParam($opts, 'view', '');

		$data['view'] = (is_callable($view) || $view === '')
			? $view
			: function () use ($view) { echo $view; };

		$data['position'] = Utils::getParam($opts, 'position', 'last');

		if ($data['position'] === 'first') {
			$data['position'] = 0;
		} else if ($data['position'] === 'last') {
			$data['position'] = null;
		}

		$data['menutitle'] = Utils::getParam($opts, 'menutitle', $data['title']);

		return $data;
	}

	public function add(array $opts) {
		$this->to_add[] = $opts;

		if (isset($opts['parent'])) {
			return;
		}

		$children = Utils::getParam($opts, 'children', []);
		foreach ($children as $child) {
			$child['parent'] = $opts['slug'];
			$this->add($child);
		}
	}

	public function remove(string $voice, $level = null) {
		$voice_data = [ 'voice' => $voice ];
		if ($level) {
			$voice_data['level'] = $level;
		}
		$this->to_remove[] = $voice_data;
	}

	public function replace(string $voice, array $opts, $level = null) {
		$voice_data = [
			'to_replace'	=> [ 'voice' => $voice ],
			'with'				=> $opts
		];
		if ($level) {
			$voice_data['to_replace']['level'] = $level;
		}
		$this->to_replace[] = $voice_data;
	}

}

/**
 * MenuHandler static Facade
 */
class Menu {

	public static function add(array $opts) {
		return MenuHandler::getInstance()->add($opts);
	}

	public static function remove(string $slug, $level = null) {
		return MenuHandler::getInstance()->remove($slug, $level);
	}

	public static function replace(string $slug, array $opts, $level = null) {
		return MenuHandler::getInstance()->replace($slug, $opts, $level);
	}

	public static function order($voices, $position = null) {
		return MenuHandler::getInstance()->order($voices, $position);
	}

}