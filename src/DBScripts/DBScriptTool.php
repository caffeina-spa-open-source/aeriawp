<?php

namespace Aeria\DBScripts;

interface DBScriptTool {

	public function apply(string $script) : string;

	public function addParams(array &$params);

}