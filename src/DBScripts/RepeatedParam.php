<?php

namespace Aeria\DBScripts;

use Aeria;

/**
 * RepeatedParam is a quick way to explode
 *
 * NOTE: the `switch` snippet (from `$$` to `$$`) must be on a single line.
 */
class RepeatedParam implements DBScriptTool {

	private $value = null;

	private $times = 1;

	public function __construct($value, int $times) {
		$this->value = $value;
		$this->times = $times;
	}

	public static function create($value, int $times) {
		return new static($value, $times);
	}

	public function getValue() {
		return $this->value;
	}

	public function getTimes() {
		return $this->times;
	}

	public function apply(string $script) : string {
		// do nothing
		return $script;
	}

	public function addParams(array &$params) {
		$value = $this->getValue();
		for ($i = 0; $i < $this->getTimes(); ++$i) {
			if ($value instanceof DBScriptTool) {
				$value->addParam($params);
			} else {
				array_push($params, $value);
			}
		}
	}

}