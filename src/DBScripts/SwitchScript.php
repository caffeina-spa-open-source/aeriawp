<?php

namespace Aeria\DBScripts;

use Aeria;

/**
 * Switch gives us a way to switch between parts of the script; note that this
 * uses the passed arguments (so, they'll need to be passed in the correct order
 * anyway), but will never add 'em to the array of arguments to prepare (they
 * will be stripped).
 * It's useful to create, for example, a small list of possible `ORDER BY`s
 * based on a passed value.
 * Example:
 *
 * 		```
 *		##! SELECT::get_ordered_posts !##
 * 
 * 		SELECT *
 * 		FROM wp_posts
 * 		WHERE post_type = %s AND post_status = 'publish'
 * 		ORDER BY
 * 		$$ status(( post_status ASC )) created_at(( created_at DESC )) name(( post_name ASC )) default(( created_at DESC )) $$
 * 		```
 * We can call this script this way:
 *
 * 		```
 * 		DBscript::call(
 * 			'SELECT::get_ordered_posts',
 * 			[
 * 				'page',
 * 				new \Aeria\DBSCripts\Switch($_GET['orderby'])
 * 			]
 * 		)
 * 		```
 *
 * When `$_GET['orderby']` is 'status', 'post_status ASC' will be added; when it
 * is 'name', 'post_name ASC' will be used, and so on.
 * If there is no match, 'default' will be used ('created_at DESC', in this
 * example).
 * The default clause *MUST ALWAYS* exist (but it can be empty) and *MUST
 * ALWAYS* be the last one.
 * Also note that this different cases with the same output must all be listed
 * (no fall through).
 *
 * NOTE: the `switch` snippet (from `$$` to `$$`) must be on a single line.
 */
class SwitchScript implements DBScriptTool {

	public function __construct(
		private $case_value = null
		) {
		//
	}

	public static function create($case_value) 
	{
		return new static($case_value);
	}

	public function getCaseValue() 
	{
		return $this->case_value === null
			? 'default'
			: (string)$this->case_value;
	}

	public function apply(string $script): string
	{
		$script = preg_replace(
			'/\$\$.*(' . $this->getCaseValue() . '|default)\(\((((?!\(\().)+)\)\).*\$\$/U',
			'$2',
			$script,
			1
		);
		return $script;
	}

	public function addParams(array &$params)
	{
		//
	}
}
