<?php

namespace Aeria\DBScripts;

use Aeria;

/**
 * Conditional gives us a way to `build` conditionally parts of the script.
 * Example:
 *
 * 		```
 *		##! SELECT::get_posts !##
 * 
 * 		SELECT *
 * 		FROM wp_posts
 * 		WHERE post_type = %s
 * 			?? AND post_status = %s :: AND post_status = 'publish' ??
 * 		ORDER BY created_at
 * 		```
 *
 * We can call this script this way:
 *
 * 		```
 * 		DBscript::call(
 * 			'SELECT::get_posts',
 * 			[
 * 				'page',
 * 				new \Aeria\DBSCripts\Conditional(
 * 					isset($_GET['post_status']),
 * 					$_GET['post_status']
 * 				)
 * 			]
 * 		)
 * 		```
 *
 * This sql snippet:
 *
 * 		```
 *   	?? AND post_status = %s :: AND post_status = 'publish' ??
 *   	```
 *
 * is read as a `then`/`else`; so, based on the passed condition, it will use
 * the part before the double semi-colon, or after.
 *
 * Something like doing a:
 *
 *		```
 *		$sql_query = [...];
 *		$args = [ 'page' ];
 *		if (isset($_GET['post_status'])) {
 *			$sql_query .= ' AND post_status = %s ';
 *			$args[] = $_GET['post_status'];
 *		} else {
 *			$sql_query .= ' AND post_status = 'publish' ';
 *		}
 *  	```
 * 
 * Note that if the condition is evaluated as `true`, it will also add the
 * passed value (`$_GET]`); otherwise it will be ignored (so the snippet should)
 * be written with that in mind.
 *
 * Also note that the `else` part may be skipped; this snippet:
 *
 * 		```
 *   	?? AND post_status = %s ??
 *   	```
 *
 * will work as well; it will simply print nothing when the condition is not
 * met.
 * 
 * NOTE: the `conditional` snippet (from `??` to `??`) must be on a single line.
 */
class ConditionalScript implements DBScriptTool {

	private $check = false;

	private $value = null;

	public function __construct($condition, $value) {
		if (is_callable($condition)) {
			$this->check = (bool)($condition($value));
		} else {
			$this->check = (bool)$condition;
		}
		$this->value = $value;
	}

	public static function create($condition, $value) {
		return new static($condition, $value);
	}

	public function isTrue() {
		return $this->check;
	}

	public function isFalse() {
		return !$this->check;
	}

	public function getValue() {
		return $this->value;
	}

	public function apply(string $script) : string {
		$script = preg_replace(
			'/\?\?(((?!\:\:|\?\?).)+)(\:\:){0,1}(((?!\:\:|\?\?).)*)\?\?/',
			($this->isTrue() ? '$1' : '$4'),
			$script,
			1
		);
		return $script;
	}

	public function addParams(array &$params) {
		if ($this->isTrue()) {
			$value = $this->getValue();
			if ($value instanceof DBScriptTool) {
				$value->addParams($params);
			} else {
				array_push($params, $value);
			}
		}
	}

}