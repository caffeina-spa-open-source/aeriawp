<?php

namespace Aeria;

/**
 * Wraps the 'add_filter' and 'remove_filter' functions.
 */
class Filter {

	public static function add(
			string $name,
			callable $func,
			int $accepted_args = 1,
			int $priority = 10) {
		if (!is_callable($func)) {
			throw new \Exception("Must pass a callable as the second argument");
		}

		add_filter($name, $func, $priority, $accepted_args);
	}

	public static function remove(
			string $name,
			callable $func,
			int $priority = 10) {
		remove_filter($name, $func, $priority);
	}

}