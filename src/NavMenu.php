<?php

namespace Aeria;

/**
 * Helper for retrieving custom wordpress menus in a more readable way.
 */
class NavMenuHandler {

	use Singleton;

	protected $items_cache = [];

	protected $menus_cache = [
		'by_id' => [],
		'by_name' => [],
		'by_slug' => []
	];

	protected $navmenu_metas = [];

	public function __construct() 
	{
		Action::add('wp_update_nav_menu', function($menu_id, $menu_data = null) {
			if (!isset($menu_data)) {
				return;
			}

			do_action(
				'aeria_update_nav_menu',
				$menu_id,
				$menu_data,
				$this->tree($menu_data['menu-name'])
			);
		}, 2);

		Action::add('admin_head-nav-menus.php', function() {
			foreach ($this->navmenu_metas as $meta) {
				add_meta_box(
					$meta['id'],
					$meta['label'],
					$meta['view'],
					'nav-menus',
					'side',
					$meta['priority'],
					$meta['callback_args']
				);
			}
		});
	}

	public function addMeta($options) 
	{
		$id = Utils::getParam($options, 'id', null);
		
		if (is_null($id)) {
			throw new \Exception("NavMenu: Missing attribute 'id'");
		}

		$label = Utils::getParam($options, 'label', $id);
		$view = Utils::getParam($options, 'view', '');
		$priority = Utils::getParam($options, 'priority', 'default');
		$callback_args = Utils::getParam($options, 'callback_args', null);

		$this->navmenu_metas[] = [
			'id'			=> $id,
			'label'			=> $label,
			'view'			=> $view,
			'priority'		=> $priority,
			'callback_args'	=> $callback_args
		];
	}

	public function get($menu_id) 
	{
		if (is_integer($menu_id) && isset($this->menus_cache['by_id']['_' . $menu_id])) {
			return $this->menus_cache['by_id']['_' . $menu_id];
		}

		if (is_string($menu_id)) {
			if (isset($this->menus_cache['by_name'][$menu_id])) {
				return $this->menus_cache['by_name'][$menu_id];
			}
			if (isset($this->menus_cache['by_slug'][$menu_id])) {
				return $this->menus_cache['by_slug'][$menu_id];
			}
		}

		$menu_term = wp_get_nav_menu_object($menu_id);

		$this->menus_cache['by_id']['_' . $menu_term->id] = $menu_term;
		$this->menus_cache['by_name'][$menu_term->name] = $menu_term;
		$this->menus_cache['by_slug'][$menu_term->slug] = $menu_term;

		return $menu_term;
	}

	public function items(string $menu_name) 
	{
		if (!isset($this->items_cache[$menu_name])) {
			$this->items_cache[$menu_name] = wp_get_nav_menu_items(
				$this->get($menu_name)
			);
		}

		return $this->items_cache[$menu_name];
	}

	/**
	 * Return a tree with all the specified menu items.
	 * @param  string $menu_name  Name of the menu location
	 * @param  array  $attributes Additional attributes for the items of the menu 
	 * @return array              Array of menu items in a tree-like structure
	 */
	public function tree(string $menu_name, array $attributes = []): array 
	{
		$menu_items = $this->items($menu_name);

		if (empty($menu_items)) {
			return [];
		}

		return array_reduce(
			$this->transform($menu_items, $attributes),
			function($tree, $item) {
				$tree[$item['parent_id']]['children'][] = $item;
				$tree[$item['id']] = &$tree[$item['parent_id']]['children'][count($tree[$item['parent_id']]['children']) - 1];
				return $tree;
			},
			[ 0 => [ 'children' => [] ] ]	// starting state
		)[0]['children'];
  }

	protected function transform($menu_items, array $attributes = []) 
	{
		return array_map(
			function($item) use($attributes) {
				return array_merge(
					[
						'title'				=> $item->title,
						'url'					=> $item->url,
						'object_id'		=> $item->object_id,
						'description' => $item->description
					],
					$this->attributes($item, $attributes),
					[
						'id'					=> $item->ID,
						'parent_id'		=> $item->menu_item_parent,
						'children'		=> []
					]
				);
			},
			$menu_items
		);
	}

	protected function attributes($menu_item, array $attributes = []) 
	{
		if (empty($attributes)) {
			return [];
		}

		$attrs = [];

		foreach ($attributes as $attribute) {
			if (is_callable($attribute)) {
				$attrs[$attribute] = $attrs($menu_item);
			} else {
				$attrs[$attribute] = $menu_item->{$attribute};
			}
		}

		return $attrs;
	}

	public function add($menu_names, array $options = []) 
	{
		if (!is_array($menu_names)) {
			$menu_names = [ $menu_names ];
		}

		foreach ($menu_names as $menu_name) {
			if (!wp_get_nav_menu_object($menu_name)) {
				wp_create_nav_menu($menu_name);
			}
		}

		foreach ($options as $option_name => $option_value) {
			// right now the only option is 'on_update', which wants a callable as a
			// value; in the future it could be useful to have other options.
			switch ($option_name) {

				case 'on_update':
					if (!is_callable($option_value)) {
						throw new \Exception("NavMenu: option 'on_update' needs a callable "
							. "value; '" . gettype($option_value) . "' passed");
					}
					Action::add(
						'wp_update_nav_menu',
						function($menu_id, $menu_data = null) use($menu_names, $option_value) {
							if (!in_array($this->get($menu_id)->name, $menu_names)) {
								return;
							}
							$option_value($menu_id, $menu_data);
						},
						2 // num args
					);
					break;

				default:
					throw new \Exception("NavMenu: unknown option '{$option_name}' for "
						. "menu voice(s) '" . implode(', ', $menu_names) . "'");

			}
		}
	}

}

class NavMenu 
{
	public static function addMeta($options) {
		NavMenuHandler::getInstance()->addMeta($options);
	}

	public static function get($menu_id) {
		return NavMenuHandler::getInstance()->get($menu_id);
	}

	public static function items(string $menu_name) {
		return NavMenuHandler::getInstance()->items($menu_name);
	}

	public static function tree(string $menu_name, array $attributes = []) {
		return NavMenuHandler::getInstance()->tree($menu_name, $attributes);
	}

	public static function add($menu_names, array $options = []) {
		NavMenuHandler::getInstance()->add($menu_names, $options);
	}
}
