<?php

namespace Aeria;

class Optimize {

	protected static $enabled = false;

	public static function enable() {
		if (static::$enabled) {
			return;
		}

		// Disable auto-drafts
		if (false === defined('WP_POST_REVISIONS')) {
			define('WP_POST_REVISIONS', false);
		}

		// Remove unneeded widgets that have undesirable query overhead
		Action::add( 'widgets_init', function() {
			unregister_widget('WP_Widget_Pages');
			unregister_widget('WP_Widget_Calendar');
			unregister_widget('WP_Widget_Tag_Cloud');
			unregister_widget('WP_Nav_Menu_Widget');
		});


		Action::add('init',function() {
			Filter::add('index_rel_link',            '__return_false');
			Filter::add('parent_post_rel_link',      '__return_false');
			Filter::add('start_post_rel_link',       '__return_false');
			Filter::add('previous_post_rel_link',    '__return_false');
			Filter::add('next_post_rel_link',        '__return_false');

			// remove junk from head
			Action::remove('wp_head', 'rel_canonical');
			Action::remove('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
			Action::remove('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
			Action::remove('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
			Action::remove('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
			Action::remove('wp_head', 'index_rel_link'); // index link
			Action::remove('wp_head', 'parent_post_rel_link', 10, 0); // prev link
			Action::remove('wp_head', 'start_post_rel_link', 10, 0); // start link
			Action::remove('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
			Action::remove('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
			Action::remove('wp_head', 'wp_shortlink_wp_head');

			// remove_all_admin_bar_items
			Action::remove('admin_bar_menu', 'wp_admin_bar_my_account_menu', 10 );
			Action::remove('admin_bar_menu', 'wp_admin_bar_my_sites_menu', 20 );
			Action::remove('admin_bar_menu', 'wp_admin_bar_edit_menu', 30 );
			Action::remove('admin_bar_menu', 'wp_admin_bar_shortlink_menu', 80 );
			Action::remove('admin_bar_menu', 'wp_admin_bar_updates_menu', 70 );
			Action::remove('admin_bar_menu', 'wp_admin_bar_new_content_menu', 40 );
			Action::remove('admin_bar_menu', 'wp_admin_bar_comments_menu', 50 );
			Action::remove('admin_bar_menu', 'wp_admin_bar_appearance_menu', 60 );

			// Disable RSS feeds
			$__disable_feed = function() {
				static $url = null;
				if (null === $url) {
					$url = get_bloginfo('url');
				}
				wp_redirect($url);
			};
			Action::add('do_feed',               '__disable_feed', 1);
			Action::add('do_feed_rdf',           '__disable_feed', 1);
			Action::add('do_feed_rss',           '__disable_feed', 1);
			Action::add('do_feed_rss2',          '__disable_feed', 1);
			Action::add('do_feed_atom',          '__disable_feed', 1);
			Action::add('do_feed_rss2_comments', '__disable_feed', 1);
			Action::add('do_feed_atom_comments', '__disable_feed', 1);

			// optimize_rewrites
			Filter::add('rewrite_rules_array', function($rules) {
				foreach ($rules as $rule => $rewrite) {
					if (preg_match('(feed|rss2|atom|comment|attachment|trackback)i', $rule)) {
						unset($rules[$rule]);
					}
				}
				return $rules;
			});

			// Disable Emojis
			Action::remove('wp_head',             'print_emoji_detection_script', 7);
			Action::remove('admin_print_scripts', 'print_emoji_detection_script');
			Action::remove('wp_print_styles',     'print_emoji_styles');
			Action::remove('admin_print_styles',  'print_emoji_styles');
			Filter::remove('the_content_feed',    'wp_staticize_emoji');
			Filter::remove('comment_text_rss',    'wp_staticize_emoji');
			Filter::remove('wp_mail',             'wp_staticize_emoji_for_email');
			Filter::add('tiny_mce_plugins',       function($plugins) {
				if (is_array($plugins)) {
					return array_diff($plugins, array('wpemoji'));
				} else {
					return array();
				}
			});

			Action::remove('welcome_panel', 'wp_welcome_panel');
		});

		Action::add('wp_dashboard_setup', function() {
			foreach (['dashboard', 'dashboard-network'] as $hook) {
				remove_meta_box('dashboard_plugins',        $hook, 'normal');
				remove_meta_box('dashboard_primary',        $hook, 'side');
				remove_meta_box('dashboard_secondary',      $hook, 'side');
				remove_meta_box('icl_dashboard_widget',     $hook, 'side');
				remove_meta_box('wpseo-dashboard-overview', $hook, 'side');
			}
		});

		static::$enabled = true;
	}

	public static function isEnabled() {
		return static::$enabled;
	}

}