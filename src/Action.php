<?php

namespace Aeria;

/**
 * Wraps the 'add_action' and 'remove_action' functions to hook WordPress
 * actions.
 * Note that this is not to be used when creating ajax endpoints.
 */
class Action {

	protected static $registered_shutdown_func = [];

	public static function handleRegisteredShutdownFunctions() {
		if (empty(static::$registered_shutdown_func)) {
			return;
		}
		usort(static::$registered_shutdown_func, function($a, $b) {
			return ($a['priority'] < $b['priority']
				? -1
				: ($a['priority'] > $b['priority']
					? 1
					: 0
				)
			);
		});
		
		foreach (static::$registered_shutdown_func as $registered_func) {
			$registered_func['call']();
		}
	}

	public static function add(
			string $name,
			callable $func,
			int $accepted_args = 1,
			int $priority = 10) {
		if (strpos($name, 'wp_ajax_') !== false) {
			throw new \Exception("Cannot add Action '$name' as it contains 'wp_ajax'"
				. " in its name; use Aeria\Ajax");
		}
		if (!is_callable($func, false, $callable_name)) {
			throw new \Exception("Must pass a callable as the second argument");
		}

		/**
		 * non-existent event 'on_shutdown' will be used to register handlers to
		 * bind at the end of execution; it will ignore accepted args.
		 */
		if ($name === 'on_shutdown') {
			static::$registered_shutdown_func[] = [
				'callable_name'	=> $callable_name,
				'call'					=> $func,
				'priority'			=> $priority
			];
		} else {
			add_action($name, $func, $priority, $accepted_args);
		}
	}

	public static function remove(
			string $name,
			callable $func,
			int $priority = 10) {
		if ($name === 'on_shutdown') {
			foreach (static::$registered_shutdown_func as $i => $registered_func) {
				if ($registered_func['call'] == $func
						&& $registered_func['priority'] == $priority) {
					array_splice(static::$registered_shutdown_func, $i, 1);
					break;
				}
			}
		} else {
			remove_action($name, $func, $priority);
		}
	}

}

// register 'Action::handleRegisteredShutdownFunctions' shutdown function; this
// way we can use Action to register shutdown functions too through the
// 'on_shutdown' fake event.
// NOTE: removing a registered function and handling the shutdown functions
// priorities will only work for callbacks registered through the Action
// methods; functions registered directly through 'register_shutdown_function'
// will live their own life.
register_shutdown_function([Action::class, 'handleRegisteredShutdownFunctions']);