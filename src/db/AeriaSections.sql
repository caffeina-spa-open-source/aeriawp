##! CREATE::aeria_sections !##

CREATE TABLE IF NOT EXISTS `{{ prefix }}aeria_sections` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`type` VARCHAR(50) NOT NULL,
	`postmeta_id` BIGINT(20) UNSIGNED NOT NULL,
	`post_id` BIGINT(20) UNSIGNED NOT NULL,
	`sectionorder` INT(11) UNSIGNED NOT NULL DEFAULT 0,
	`title` VARCHAR(255) NOT NULL DEFAULT '',
	`sectiongroup` VARCHAR(255) NOT NULL DEFAULT '',
	`status` VARCHAR(20) NOT NULL DEFAULT '',
	`createdby_id` BIGINT(20) UNSIGNED,
	`createdat` TIMESTAMP NOT NULL DEFAULT 0,
	`updatedby_id` BIGINT(20) UNSIGNED,
	`updatedat` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
							ON UPDATE CURRENT_TIMESTAMP,

	CONSTRAINT `pk_aeria_sections` PRIMARY KEY(`id`),
	CONSTRAINT `fk_aeria_sections_posts`
		FOREIGN KEY(`post_id`) REFERENCES `{{ prefix }}posts`(`ID`)
		ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `fk_aeria_sections_postmeta`
		FOREIGN KEY(`postmeta_id`) REFERENCES `{{ prefix }}postmeta`(`meta_id`)
		ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `fk_aeria_sections_users_createdby`
		FOREIGN KEY(`createdby_id`) REFERENCES `{{ prefix }}users`(`ID`)
		ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `fk_aeria_sections_users_updatedby`
		FOREIGN KEY(`updatedby_id`) REFERENCES `{{ prefix }}users`(`ID`)
		ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

##! CREATE::aeria_sections_fields !##

CREATE TABLE IF NOT EXISTS `{{ prefix }}aeria_sections_fields` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`section_id` INT(11) UNSIGNED NOT NULL,
	`fieldkey` VARCHAR(255) NOT NULL DEFAULT '',
	`fieldvalue` LONGTEXT DEFAULT NULL,
	`createdby_id` BIGINT(20) UNSIGNED,
	`createdat` TIMESTAMP NOT NULL DEFAULT 0,
	`updatedby_id` BIGINT(20) UNSIGNED,
	`updatedat` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
							ON UPDATE CURRENT_TIMESTAMP,

	CONSTRAINT `pk_aeria_sections_fields` PRIMARY KEY(`id`),
	CONSTRAINT `fk_aeria_sections_fields_aeria_sections`
		FOREIGN KEY(`section_id`) REFERENCES `{{ prefix }}aeria_sections`(`id`)
		ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `fk_aeria_sections_fields_users_createdby`
		FOREIGN KEY(`createdby_id`) REFERENCES `{{ prefix }}users`(`ID`)
		ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `fk_aeria_sections_fields_users_updatedby`
		FOREIGN KEY(`updatedby_id`) REFERENCES `{{ prefix }}users`(`ID`)
		ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

##! SELECT::get_sections !##

SELECT
	s.`id` AS `section_id`,
	s.`type` AS `section_type`,
	s.`postmeta_id` AS `section_postmeta_id`,
	s.`post_id` AS `section_post_id`,
	s.`sectionorder` AS `section_sectionorder`,
	s.`title` AS `section_title`,
	s.`sectiongroup` AS `section_sectiongroup`,
	s.`status` AS `section_status`,
	s.`createdby_id` AS `section_createdby_id`,
	s.`createdat` AS `section_createdat`,
	s.`updatedby_id` AS `section_updatedby_id`,
	s.`updatedat` AS `section_updatedat`,
	sf.`id` AS `field_id`,
	sf.`section_id` AS `field_section_id`,
	sf.`fieldkey` AS `field_key`,
	sf.`fieldvalue` AS `field_value`,
	sf.`createdby_id` AS `field_createdby_id`,
	sf.`createdat` AS `field_createdat`,
	sf.`updatedby_id` AS `field_updatedby_id`,
	sf.`updatedat` AS `field_updatedat`
FROM `{{ prefix }}aeria_sections_fields` AS sf
JOIN `{{ prefix }}aeria_sections` AS s ON s.`id` = sf.`section_id`
WHERE s.`post_id` = %d
ORDER BY s.`sectionorder`

##! SELECT::get_sections_group !##

SELECT
	s.`id` AS `section_id`,
	s.`type` AS `section_type`,
	s.`postmeta_id` AS `section_postmeta_id`,
	s.`post_id` AS `section_post_id`,
	s.`sectionorder` AS `section_sectionorder`,
	s.`title` AS `section_title`,
	s.`sectiongroup` AS `section_sectiongroup`,
	s.`status` AS `section_status`,
	s.`createdby_id` AS `section_createdby_id`,
	s.`createdat` AS `section_createdat`,
	s.`updatedby_id` AS `section_updatedby_id`,
	s.`updatedat` AS `section_updatedat`,
	sf.`id` AS `field_id`,
	sf.`section_id` AS `field_section_id`,
	sf.`fieldkey` AS `field_key`,
	sf.`fieldvalue` AS `field_value`,
	sf.`createdby_id` AS `field_createdby_id`,
	sf.`createdat` AS `field_createdat`,
	sf.`updatedby_id` AS `field_updatedby_id`,
	sf.`updatedat` AS `field_updatedat`
FROM `{{ prefix }}aeria_sections_fields` AS sf
JOIN `{{ prefix }}aeria_sections` AS s ON s.`id` = sf.`section_id`
WHERE s.`post_id` = %d AND s.`sectiongroup` = %s
ORDER BY s.`sectionorder`

##! SELECT::check_section !##

SELECT COUNT(`id`) AS res
FROM `{{ prefix }}aeria_sections`
WHERE `id` = %d AND `post_id` = %d AND `sectiongroup` = %s
LIMIT 0,1

##! SELECT::get_section !##

SELECT
	s.`id` AS `section_id`,
	s.`type` AS `section_type`,
	s.`postmeta_id` AS `section_postmeta_id`,
	s.`post_id` AS `section_post_id`,
	s.`sectionorder` AS `section_sectionorder`,
	s.`title` AS `section_title`,
	s.`sectiongroup` AS `section_sectiongroup`,
	s.`status` AS `section_status`,
	s.`createdby_id` AS `section_createdby_id`,
	s.`createdat` AS `section_createdat`,
	s.`updatedby_id` AS `section_updatedby_id`,
	s.`updatedat` AS `section_updatedat`
FROM `{{ prefix }}aeria_sections` AS s
WHERE s.`id` = %d
LIMIT 0,1

##! SELECT::get_section_fields !##

SELECT
	s.`id` AS `section_id`,
	s.`type` AS `section_type`,
	s.`postmeta_id` AS `section_postmeta_id`,
	s.`post_id` AS `section_post_id`,
	s.`sectionorder` AS `section_sectionorder`,
	s.`title` AS `section_title`,
	s.`sectiongroup` AS `section_sectiongroup`,
	s.`status` AS `section_status`,
	s.`createdby_id` AS `section_createdby_id`,
	s.`createdat` AS `section_createdat`,
	s.`updatedby_id` AS `section_updatedby_id`,
	s.`updatedat` AS `section_updatedat`,
	sf.`id` AS `field_id`,
	sf.`section_id` AS `field_section_id`,
	sf.`fieldkey` AS `field_key`,
	sf.`fieldvalue` AS `field_value`,
	sf.`createdby_id` AS `field_createdby_id`,
	sf.`createdat` AS `field_createdat`,
	sf.`updatedby_id` AS `field_updatedby_id`,
	sf.`updatedat` AS `field_updatedat`
FROM `{{ prefix }}aeria_sections_fields` AS sf
JOIN `{{ prefix }}aeria_sections` AS s ON s.`id` = sf.`section_id`
WHERE s.`id` = %d
ORDER BY s.`sectionorder`