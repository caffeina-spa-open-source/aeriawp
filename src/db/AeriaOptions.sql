##! SELECT::get_multi_options !##

SELECT
	o.`option_id` AS `option_id`,
	o.`option_name` AS `option_name`,
	o.`option_value` AS `option_value`,
	o.`autoload` AS `autoload`
FROM `{{ prefix }}options` AS o
WHERE o.`option_name` RLIKE CONCAT('^', %s, '--')
ORDER BY o.`option_id`