##! CREATE::aeria_jobs_queue !##

CREATE TABLE IF NOT EXISTS `{{ jobs_queue_table }}` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`jobkey` VARCHAR(65) NOT NULL,
	`jobname` VARCHAR(50) NOT NULL,
	`jobdata` TEXT NOT NULL,
	`metadata` TEXT NOT NULL,
	# statuses:
	# 	- simple: 'new', 'waiting', 'in_progress', 'done', 'error';
	# 	- complete: 'new', 'waiting', 'in_progress', 'error', 'done', 'undoing'.
	`status` VARCHAR(50) NOT NULL,
	`retries` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`message` TEXT DEFAULT NULL, # small log
	`createdat` TIMESTAMP NOT NULL DEFAULT 0,
	`updatedat` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
							ON UPDATE CURRENT_TIMESTAMP,

	CONSTRAINT `pk_{{ jobs_queue_table }}` PRIMARY KEY(`id`),
	INDEX `jobsqueue_jobkey_index`(`jobkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

##! SELECT::get_jobs_queue !##

SELECT
	q.`id` AS `job_id`,
	q.`jobkey` AS `jobkey`,
	q.`jobname` AS `jobname`,
	q.`jobdata` AS `jobdata`,
	q.`metadata` AS `metadata`,
	q.`status` AS `status`,
	q.`message` AS `message`,
	q.`createdat` AS `createdat`,
	q.`updatedat` AS `updatedat`
FROM `{{ jobs_queue_table }}` AS q
WHERE q.`jobname` = %s
	AND q.`status` IN ([[ %s ]])
	AND q.`createdat` < CURRENT_TIMESTAMP
ORDER BY q.`createdat` ASC
LIMIT 0, %d

##! CHECK::check_jobkey !##

SELECT
	q.`id` AS `job_id`,
	q.`jobkey` AS `jobkey`,
	q.`createdat` AS `createdat`,
	q.`updatedat` AS `updatedat`
FROM `{{ jobs_queue_table }}` AS q
WHERE q.`jobkey` IN ([[ %s ]])
	AND q.`jobname` = %s
	AND q.`status` IN ([[ %s ]])
	AND CURRENT_TIMESTAMP < TIMESTAMPADD(MINUTE, %d, q.`updatedat`)
ORDER BY q.`createdat` DESC
LIMIT 0, 1

##! UPDATE::jobs_status !##

UPDATE `{{ jobs_queue_table }}`
SET status = %s
WHERE id IN ([[ %d ]]) AND status IN ([[ %s ]])