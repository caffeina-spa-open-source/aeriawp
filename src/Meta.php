<?php

namespace Aeria;

class MetaHandler {

	use Singleton;

	private $meta_list = [];

	private function saveDraft($post_id, $post_data) {
		$post_content = Utils::getParam($post_data, 'postdata--content', '');
		$post_title = Utils::getParam($post_title, 'postdata--title', '');
		$post_type = Utils::getParam($post_type, 'postdata--type', '');

		Filter::add('wp_insert_post_empty_content', '__return_false');

		$current_post = get_post($post_id);

		if (is_null($current_post)) {
			$post_arr = [];

			$post_arr['post_author'] = get_current_user_id();
			$post_arr['post_content'] = $post_content;
			$post_arr['post_title'] = $post_title;
			$post_arr['post_type'] = $post_type;
			$post_arr['post_excerpt'] = '';
			$post_arr['post_status'] = 'draft';

			$post_id = wp_insert_post($post_arr, true);
		} else if ($current_post->post_status == 'draft'
				|| $current_post->post_status == 'auto-draft') {
			$current_post->post_content = $post_content;
			$current_post->post_title = $post_title;
			$current_post->post_type = $post_type;
			$current_post->post_status = 'draft';

			$post_id = wp_update_post($current_post, true);
		} else {
			Response::error("Cannot save post $post_id as a draft");
		}

		Filter::remove('wp_insert_post_empty_content', '__return_false');

		if (is_wp_error($post_id)) {
			Response::error($post_id->get_error_message());
		}
		return $post_id;
	}

	/**
	 * Insert/update a collection of meta-data linked to the specified post (via
	 * post_id).
	 * @param  int    $post_id  	id of the post to update
	 * @param  array  $meta_data 	list of key-value pairs representin metadata
	 *                            that needs saving
	 * @return array            	list of outcomes (one per key); outcomes are the
	 *                            result of 'update_post_meta', so:
	 *                            - meta_id if the meta didn't exist before;
	 *                            - true if the existing meta value has changed;
	 *                            - false otherwise.
	 */
	public static function updateMeta(int $post_id, array $meta_data) {
		$outcomes = [];
		foreach ($meta_data as $key => $value) {
			$value = apply_filters(
				'aeria_meta_' . $key . '_update',
				$value,
				$key,
				$meta_data,
				$post_id
			);
			// NOTE: 'update_post_meta' serializes the value if needed all by itself;
			// *DON'T* serialize it manually or it will be serialized 2 times.
			$outcomes[$key] = update_post_meta($post_id, $key, $value);
			do_action(
				'aeria_meta_' . $key . '_updated',
				[
					$value,
					$key,
					$meta_data,
					$post_id
				]
			);
		}

		return $outcomes;
	}

	/**
	 * Init method: if this is the first meta encountered bind some common
	 * actions to wordpress hooks.
	 */
	protected function __construct() 
	{
		Assets::addScript('aeriaVendors', '/aeria/assets/js/aeria-vendors.js', ['in_footer' => true]);
		Assets::addScript('aeria', '/aeria/assets/js/aeria.js', ['in_footer' => true]);

		// Insert an hidden `wp_editor`; it will be removed client side
		Action::add('edit_form_after_title', function($post) {
			if (!post_type_supports($post->post_type, 'editor')) {
				echo '<div id="___preload_mce_config___container" style="display:none">';
				wp_editor('', '___preload_mce_config___');
				echo '</div>';
			}
		});

		/**
		 * Register Ajax Call(s)
		 */

		// -- save post meta data
		Ajax::add('save_meta', function() {
			$args = Params::post();
			if (! isset($args['post_id'])) {
				Response::error("No 'post_id' specified.");
			} else if (! is_numeric($args['post_id'])) {
				Response::error("'post_id' must be a int.");
			}

			$post_id = $args['post_id'];
			$orig_post_id = $post_id;

			$save_draft = Utils::getParam($args, 'saveDraft', false);

			$meta_data = Utils::mapPrefixedData($args, false);
			$post_data = [];
			if (isset($meta_data['postdata'])) {
				$post_data = $meta_data['postdata'];
				unset($meta_data['postdata']);
			}

			if ($save_draft) {
				$this->saveDraft($post_id, $post_data);
			} else {
				$current_post = get_post($post_id);
				if (is_null($current_post)) {
					Response::error("Couldn't find post " . $post_id);
				}
			}

			$msg = [];

			foreach ($meta_data as $domain_meta_data) {
				$outcomes = $this->updateMeta($post_id, $domain_meta_data);
			}

			$result = [
				'post_id'	=> $post_id,
				'metas' 	=> $outcomes
			];

			if ($orig_post_id != $post_id) {
				$msg[] = "New post_id: " . $post_id;
			}

			if ($save_draft) {
				$msg[] = "Set status: 'draft'";
				$result['savedDraft'] = true;
			}

			$msg[] = 'Updated metas: ' . implode(', ', array_keys($meta_data));

			Response::success($result, implode('; ', $msg));
		});

		// -- save aeria metaboxes on post save
		Action::add('save_post', function($post_id) {
			$args = Params::post();
			$meta_boxes = $args['aeria--meta'] ?? [];
			$meta_data = Utils::mapPrefixedData($args, $meta_boxes);
			foreach ($meta_data as $domain_meta_data) {
				$this->updateMeta($post_id, $domain_meta_data);
			}
			do_action('save_post_meta', [ $post_id, $meta_data ]);
		});

		// -- set window.aeriaMetaboxes inside the page
		Action::add('admin_head', function() {
			echo '<script>window.aeriaMetaboxes = [];</script>';
		});
	}

	public function register($o) {
		$o = Conf::load($o);
		$post_types = Utils::getPluralParam($o, 'type', 'types');

		try {
			if (! isset($o['id'])) {
				throw new \Exception("Missing attribute 'id': post_type(s): ".implode(', ', $o));
			}

			$id = Utils::getParam($o, 'id');

			if (empty($post_types)) {
				throw new \Exception("Meta '{$id}': No post_type(s) specified");
			}
			if (!isset($o['label'])) {
				throw new \Exception("Meta '{$id}': Missing attribute 'label'");
			}

			$label = Utils::getParam($o, 'label');
			$fields = Utils::getParam($o, 'fields', []);
			$async = Utils::getParam($o, 'async', false);
			$exclude = Utils::getParam($o, 'exclude', []);
			if (!is_array($exclude)) {
				$exclude = [$exclude];
			}

			// 'normal', 'side', 'advanced' <-- standard wp contexts
			// 'after_title' <-- aeria specific contexts
			$metabox_context = Utils::getParam($o, 'context', 'advanced');

			// 'high', 'core', 'default', 'low' <-- standard wp priorities
			$priority = Utils::getParam($o, 'priority', 'default');

			/**
			 * Get Meta Fields & Data
			 */
			Ajax::add("get_{$id}_meta_fields", function() use ($id, $fields) {
				$args = Params::request();
				if (! isset($args['post_id'])) {
					Response::error("No 'post_id' specified.");
				} else if (! is_numeric($args['post_id'])) {
					Response::error("'post_id' must be a int.");
				}

				$post_id = $args['post_id'];

				try {
					$meta_fields = $this->getMetaData($post_id, $fields, $id);
				} catch (\Exception $e) {
					Response::error("Meta Error: " . $e->getMessage());
				}

				Response::success($meta_fields, "MetaBox: {$id}, post_id: {$post_id}");
			});

			/**
			 * Register Metabox
			 */
			foreach ($post_types as $post_type) {
				/**
				 * Save Meta definition indexed by post_type and id for quick retrieval
				 */
				if (!isset($this->meta_list[$post_type])) {
					$this->meta_list[$post_type] = [];
				}
				$this->meta_list[$post_type][$id] = $o;

				$slug = null;
				// check if a slug is specified
				if (Utils::contains($post_type, '/')) {
					list($post_type, $slug) = explode('/', $post_type, 2);
				}

				/**
				 * Add MetaBox
				 */
				Action::add(
					'add_meta_boxes_' . $post_type,
					function($post) use (
							$post_type,
							$id,
							$label,
							$fields,
							$async,
							$slug,
							$exclude,
							$metabox_context,
							$priority) {
						$metabox_id = 'aeriaMetabox-' . $id;
						$filter_id = 'postbox_classes_' . $post_type . '_' . $metabox_id;

						if (!empty($slug) && $post->post_name !== $slug) {
							return;
						}

						if (!empty($exclude) && in_array($post->post_name, $exclude)) {
							return;
						}

						Filter::add($filter_id, function($classes) {
							$classes[] = 'aeriaMetabox';
							return $classes;
						});

						add_meta_box(
							$metabox_id,
							$label,
							function($post) use ($id, $post_type, $fields, $async) {
								$meta_fields = $this->getMetaData(
									$post->ID,
									$fields,
									$id,
									$post_type
								);
								$meta_descr = [
									'id'			=> $id,
									'async' 	=> $async,
									'fields'	=> $async ? [] : $meta_fields
								];

								echo '
									<script>
										window.aeriaMetaboxes.push(' . json_encode($meta_descr) . ');
									</script>';
								echo '
									<div
										id="aeriaHidden-' . $id . '"
										class="aeriaHidden"
										data-aeriametabox
										data-aeriametaboxid="' . $id . '">
										<input
											type="hidden"
											name="aeria--meta[]"
											value="' . $id . '"/>';
								foreach ($meta_fields as $meta_field) {
									$field_name = $id . '--' . $meta_field['id'];
									echo '
										<input
											type="hidden"
											id="aeriaField--' . $field_name . '"
											name="' . $field_name . '"
											value="' . htmlentities($meta_field['value'] ?? null) . '"
										/>';
									do_action(
										'aeria_render_field_' . $meta_field['type'],
										[
											'post_id'		=> $post->ID,
											'post_type'		=> $post_type,
											'metabox_id'	=> $id,
											'meta_field'	=> $meta_field
										]
									);
								}
								echo '
									</div>
									<div id="aeriaApp-' . $id . '" class="aeriaApp"></div>';
							},
							$post_type,
							$metabox_context,
							$priority
						);
					}
				);

				/**
				 * Handle After-title MetaBoxes
				 */
				Action::add('edit_form_after_title',
					function($post) use ($post_type) {
						global $post, $wp_meta_boxes;

						if (get_post_type($post) != $post_type) {
							return;
						}

						if (!isset($wp_meta_boxes[$post_type])
								|| !isset($wp_meta_boxes[$post_type]['after_title'])) {
							return;
						}

						$after_title_metaboxes = [];

						// we reorder after-title metaboxes and we all insert them in the
						// 'sorted' priority
						foreach (['high', 'sorted', 'core', 'default', 'low'] as $priority) {
							if (!isset($wp_meta_boxes[$post_type]['after_title'][$priority])) {
								continue;
							}
							foreach ($wp_meta_boxes[$post_type]['after_title'][$priority] as $metabox) {
								$after_title_metaboxes[] = $metabox;
							}
							unset($wp_meta_boxes[$post_type]['after_title'][$priority]);
						}

						$wp_meta_boxes[$post_type]['after_title']['sorted'] = $after_title_metaboxes;

						do_meta_boxes($post_type, 'after_title', $post);

						unset($wp_meta_boxes[$post_type]['after_title']);
					}
				);
			}
		} catch (\Exception $e) {
			wp_die('Meta Error: ' . $e->getMessage());
		}

		/**
		 * Trigger custom event 'meta_registered'
		 */
		do_action('meta_registered', $id, $post_type, $fields, $o);
	}

	public function getDefinition(string $post_type, string $meta_id) {
		if (!isset($this->meta_list[$post_type])) {
			return null;
		}

		$pieces = explode("--", $meta_id, 2);
		$field_id = '';
		if (count($pieces) == 2) {
			$meta_id = $pieces[0];
			$field_id = $pieces[1];
		}

		if (!isset($this->meta_list[$post_type][$meta_id])) {
			return null;
		}
		$result = $this->meta_list[$post_type][$meta_id];
		if (empty($field_id)) {
			return $result;
		}
		$fields = $this->meta_list[$post_type][$meta_id]['fields'];
		foreach ($fields as $field) {
			if ($field['id'] === $field_id) {
				return $field;
			}
		}
		return null;
	}

	public function getMetaData(
			int $post_id,
			array $fields,
			string $meta_id,
			string $post_type = '') {
		$custom_data = get_post_custom($post_id);

		foreach ($fields as $index => $field) {
			$field_name = $meta_id . "--" . $field['id'];
			if (isset($custom_data[$field_name])) {
				if (count($custom_data[$field_name]) == 1) {
					$filter_args = [
						'post_id'			=> $post_id,
						'metabox_id'	=> $meta_id,
						'meta_field' 	=> $field
					];
					if (!empty($post_type)) {
						$filter_args['post_type'] = $post_type;
					}
					$fields[$index]['value'] = apply_filters(
						'aeria_format_meta_' . $field['type'],
						maybe_unserialize(
							$custom_data[$field_name][0]
						),
						$filter_args
					);
				} else {
					// TODO...
				}
			}
		}
		Utils::flatCallParams($fields);
		return $fields;
	}

	public function getMetasFromPostType(string $post_type) {
		$list = isset($this->meta_list[$post_type])
			? $this->meta_list[$post_type]
			: [];
		return wp_list_pluck($list, 'id');
	}

}

class Meta {

	public static function register($o) {
		MetaHandler::getInstance()->register($o);
	}

	public static function getDefinition(string $post_type, string $meta_id) {
		return MetaHandler::getInstance()->getDefinition($post_type, $meta_id);
	}

	public static function getMetasFromPostType(string $post_type) {
		return MetaHandler::getInstance()->getMetasFromPostType($post_type);
	}

}