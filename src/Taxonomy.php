<?php

namespace Aeria;

class TaxonomyHandler {

	use Singleton;

	protected $radio_taxonomies = [];

	protected $ordered_taxonomies = [];

	protected function __construct() {
		Filter::add(
			'wp_terms_checklist_args',
			function($taxonomy_args) {
				$opts = [];

				if (isset($this->ordered_taxonomies[$taxonomy_args['taxonomy']])) {
					$opts['ordered'] = $this->ordered_taxonomies[$taxonomy_args['taxonomy']];
				}

				if (in_array($taxonomy_args['taxonomy'], $this->radio_taxonomies)) {
					$opts['radio'] = true;
				}

				if (empty($opts)) {
					return $taxonomy_args;
				}

				$taxonomy_args['walker'] = new Helpers\CustomWalkerCategoryCheckList($opts);
				$taxonomy_args['checked_ontop'] = false;
				return $taxonomy_args;
			}
		);

		/**
		 * Expose a generic ajax endpoint to search taxonomies
		 */
		Ajax::add('search_taxonomies_head', function() {
			$params = Params::request();
			$needle = $params['needle'] ?? '';

			Response::success(
				[
					'taxonomies' => static::searchTaxonomies($needle)
				],
				"Searched taxonomies '$needle'"
			);
		});

		/**
		 * Expose a generic ajax endpoint to search taxonomies mapped as a list of
		 * select's options
		 */
		Ajax::add('options_taxonomies_head', function() {
			$params = Params::request();
			$needle = $params['needle'] ?? '';

			Response::success(
				array_map(
					function($taxonomy) {
						return [
							'value'	=> $taxonomy->name,
							'label' => $taxonomy->label
						];
					},
					static::searchTaxonomies($needle)
				),
				"Searched taxonomies '$needle'"
			);
		});

		/**
		 * Expose a generic ajax endpoint to search taxonomies' terms
		 */
		Ajax::add('search_terms_head', function() {
			$args = Params::request();
			$needle = isset($args['needle']) ? $args['needle'] : '';
			$taxonomy = isset($args['taxonomy']) ? $args['taxonomy'] : '';
			$hide_empty = isset($args['hide_empty']) ? $args['hide_empty'] : null;
			Response::success(
				[
					'terms' => static::searchTerms($needle, $taxonomy, $hide_empty)
				],
				"Searched terms '$needle'" .
					(!empty($taxonomy) ? " for taxonomy '$taxonomy'" : '')
			);
		});

		/**
		 * Expose a generic ajax endpoint to search taxonomies' terms
		 */
		Ajax::add('options_terms_head', function() {
			$args = Params::request();
			$needle = isset($args['needle']) ? $args['needle'] : '';
			$taxonomy = isset($args['taxonomy']) ? $args['taxonomy'] : '';
			$hide_empty = isset($args['hide_empty']) ? $args['hide_empty'] : null;
			Response::success(
				array_map(
					function($term) {
						return [
							'value'	=> $term->name,
							'label' => $term->name
						];
					},
					static::searchTerms($needle, $taxonomy, $hide_empty)
				),
				"Searched terms '$needle'" .
					(!empty($taxonomy) ? " for taxonomy '$taxonomy'" : '')
			);
		});
	}

	protected static function searchTaxonomies(string $needle = '') {
		$results = get_taxonomies(null, 'objects');
		if (!empty($needle)) {
			$results = array_filter($results, function($taxonomy) use($needle) {
				return strpos($taxonomy->name, $needle) !== false;
			});
		}
		return (array)$results;
	}

	protected static function searchTerms(string $needle = '', string $taxonomy = '', $hide_empty = null) 
	{
		$get_terms_args = [];

		if (! empty($taxonomy)) {
			$get_terms_args['taxonomy'] = $taxonomy;
		}

		if (! is_null($hide_empty)) {
			$get_terms_args['hide_empty'] = (bool) $hide_empty;
		}

		if (! empty($needle)) {
			$get_terms_args['search'] = $needle;
		}

		return get_terms($get_terms_args);
	}

	protected function addTerms(string $taxonomy, array $terms, int $parent = 0) 
	{
		foreach ($terms as $term_data) {
			if (is_string($term_data)) {
				$term_data = ['id' => $term_data];
			}

			$term_name = Utils::getParam($term_data, 'id');
			if ($term_name == null) {
				throw new \Exception("Terms must have an 'id'");
			}

			$args = Utils::getParam($term_data, 'args', null);
			$explicit_parent = $parent;
			if (isset($args['parent'])) {
				$explicit_parent = Utils::getParam($args, 'parent', $parent);
				if (is_string($explicit_parent)) {
					$parent_id = get_term_by('name', $explicit_parent, $taxonomy);
					if ($parent_id === false) {
						throw new \Exception("Cannot find parent Term '$explicit_parent' in"
							. " Taxonomy '$taxonomy'");
					}
					$explicit_parent = $parent_id->term_id;
				}

				$args['parent'] = $explicit_parent;
			}
			/* quick(er) version: only creates the term the first time; doesn't update
			them */
			$term_id = null;
			if (! term_exists($term_name, $taxonomy, $explicit_parent)) {
				$term = wp_insert_term($term_name, $taxonomy, $args);
				if (is_wp_error($term)) {
					throw new \Exception($term_name . ": " . $term->get_error_message());
				}
				$term_id = $term['term_id'];
			} else {
				$term = get_term_by('name', $term_name, $taxonomy);
				$term_id = $term->term_id;
			}
			/* slow version: every time a term is already present it does a
			'wp_update_term' *\/
			$term = null;
			if (!term_exists($term_name, $taxonomy, $explicit_parent)) {
				$term = wp_insert_term($term_name, $taxonomy, $args);
			} else {
				$term = get_term_by('name', $term_name, $taxonomy);
				$term = wp_update_term($term->term_id, $taxonomy, $args);
			}
			if (is_wp_error($term)) {
				throw new \Exception($term_name . ": " . $term->get_error_message());
			}
			$term_id = $term['term_id'];
			/**/
			// NOTE: renaming terms via db or wordpress means that we will recreate
			// the term with its original name when reloading the page.
			$children = Utils::getParam($term_data, 'children', []);
			if (!empty($children)) {
				$this->addTerms($taxonomy, $children, $term_id);
			}
		}
	}

	public function register($o) {
		$o = Conf::load($o);
		$post_types = Utils::getPluralParam($o, 'type', 'types');

		if (!isset($o['id'])) {
			throw new \Exception("Taxonomy: Missing attribute 'id': post_type(s): "
				. implode(', ', $post_types));
		}

		$id = Utils::getParam($o, 'id');

		if (empty($post_types)) {
			throw new \Exception("Taxonomy '{$id}': No post_type(s) specified");
		}

		$name = Utils::getParam($o, 'name', $id);
		$singular_name = Utils::getParam($o, 'singularname', $name);
		$options = Utils::getParam($o, 'options', []);
		$terms = Utils::getParam($o, 'terms', []);
		$force = Utils::getParam($o, 'force', false);

		// if the taxonomy already exists and we are trying to redefine its options
		// or its name, we throw an exception
		if (static::exists($id) && !$force) {
			if (!empty($options) || isset($o['name']) || isset($o['singularname'])) {
				throw new \Exception("Taxonomy '{$id}': Taxonomy already exists; "
					. "you can register a post_type to an existing Taxonomy, but you "
					. "cannot redefine its 'name', 'singularname' or 'options'; specify "
					. "only the 'id' to register a post_type to an existing Taxonomy");
			}
		}

		$default_options = [
			'query_var'					=> true,
			'hierarchical'			=> true,
			'public'						=> true,
			'show_ui'						=> true,
			'show_admin_column'	=> true,
			'show_in_nav_menus'	=> true,
			'show_tagcloud'			=> true,
			'rewrite'						=> [
				'slug'									=> $id
			],
			// TODO: find real default values that we want here
			'labels'						=> [
				'name'									=> $name,
				'singular_name'					=> $singular_name,
				'add_new'								=> 'Add new',
				'add_new_item'					=> 'Add new item',
				'edit_item'							=> 'Edit',
				'new_item'							=> 'New',
				'all_items'							=> 'Show all',
				'view_item'							=> 'Show item',
				'search_items'					=> 'Search',
				'not_found'							=> 'Not found',
				'not_found_in_trash'		=> 'Not found in trash',
				'choose_from_most_used' => 'Choose from most used',
				'parent_item_colon'			=> null,
				'parent_item'						=> null,
				'menu_name'							=> $name,
				'menu_icon'							=> null
			]
		];

		// NOTE: here we could do an array_merge_replace like the old Aeria; right
		// now we do a simple array_merge just to avoid including any other
		// maybe unnecessary third-party library
		$options = array_merge($default_options, $options);

		// If we want immutable terms, we simply add the needed capabilities to a
		// non existent role; by default, it uses the `__god` role; if we already
		// have a `__god` role (really?), we can set a new one with:
		// 
		// 	Aeria\Settings::set('taxonomies.godrole', 'my-god-role');
		// 
		// Based on this:
		// https://whoischris.com/remove-add-new-category-custom-taxonomy-link-wordpress/
		if (isset($options['immutable_terms'])
				&& (bool)$options['immutable_terms']
				&& !empty($terms)
				&& !isset($options['capabilities'])) {

			$god_role = Settings::get('taxonomies.godrole', '__god');

			$options['capabilities'] = [
				'assign_terms'	=> 'manage_options',
				'edit_terms'		=> $god_role,
				'manage_terms'	=> $god_role
			];

		}


		if (!static::exists($id)) {
			register_taxonomy($id, $post_types, $options);
		} else {
			$tax_object = get_taxonomy($id);
			$linked_post_types = $tax_object->object_type;
			foreach ($post_types as $post_type) {
				if ($force && in_array($post_type, $linked_post_types)) {
					continue;
				}
				if (in_array($post_type, $linked_post_types)) {
					throw new \Exception("post_type '{$post_type}' is already registered "
						. "to Taxonomy '{$id}'");
				}
				register_taxonomy_for_object_type($id, $post_type);
			}
		}

		/**
		 * Add Terms
		 */
		$this->addTerms($id, $terms);

		$radio = Utils::getParam($o, 'radio', false);

		if ($radio && !in_array($id, $this->radio_taxonomies)) {
			$this->radio_taxonomies[] = $id;
		}

		$ordered = Utils::getParam($o, 'ordered', false);

		if (is_callable($ordered) && !isset($this->ordered_taxonomies[$id])) {
			$this->ordered_taxonomies[$id] = $ordered;
		} else if ($ordered === true && !empty($terms) && !isset($this->ordered_taxonomies[$id])) {
			$this->ordered_taxonomies[$id] = $terms;
		}
	}

	/**
	 * Proxy function to 'taxonomy_exists' (to eventually add future features,
	 * like checking that the taxonomy was create through Aeria)
	 */
	public static function exists($taxonomy_name) {
		return taxonomy_exists($taxonomy_name);
	}

}

class Taxonomy {

	public static function register($o) {
		TaxonomyHandler::getInstance()->register($o);
	}

	public static function exists($taxonomy_name) {
		return TaxonomyHandler::exists($taxonomy_name);
	}

}