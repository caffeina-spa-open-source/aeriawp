<?php

namespace Aeria;

class TableHandler {

	use Singleton;

	protected static $default_limit = 20;

	protected static $table_pages = [];

	protected function __construct() 
	{
		Assets::addScript('jquery-ui-datepicker', 'jquery-ui-datepicker');
		wp_register_style('jquery-ui', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css');
		Assets::addStyle('jquery-ui');
		Assets::addScript('aeriaUtils', '/aeria/assets/js/aeriaUtils.js');
		Assets::addScript('aeriaTable', '/aeria/assets/js/aeriaTable.js');
	}

	protected function renderColumns($columns, $actions, $styles, $has_bulk_actions, $base_page_url, $accepted_filters, $filters, $position) 
	{
		if (!empty($actions['row_actions'])) {
			$columns[] = [
				'id' => 'actions',
				'label' => 'Azioni',
				'style' => isset($styles['row_actions_style'])
					? $styles['row_actions_style']
					: 'text-align: right;'
			];
		}

		if ($has_bulk_actions) {
	?>
		<td id="cb-<?= $position ?>" class="manage-column column-cb check-column">
			<label class="screen-reader-text" for="cb-select-all-<?= $position ?>">
				Seleziona tutto
			</label>
			<input id="cb-select-all-<?= $position ?>" name="cb-select-all-<?= $position ?>" class="select-all-checkbox" type="checkbox">
		</td>
	<?php
		}

		foreach ($columns as $column) {
			$col_id = $column['id'];
			$col_label = $column['label'] ?? '';
			$col_style = $column['style'] ?? null;
			$col_is_sortable = $column['sortable'] ?? false;

			if ($col_is_sortable) {
				$args = Params::get();
				$is_curr_orderby = isset($args['orderby']) && $args['orderby'] === $col_id;
				$asc_desc = 'asc';
				if ($is_curr_orderby && isset($args['order'])) {
					$asc_desc = $args['order'] === 'asc'
						? 'desc'
						: 'asc';
				}
				$asc_desc_class = (!$is_curr_orderby ? '' : ($asc_desc === 'asc' ? 'desc' : 'asc'));
				$sortable_classes = 'sortable ' . $asc_desc_class . ($is_curr_orderby ? ' sorted' : '');
	?>
		<th scope="col" id="<?= $col_id ?>-<?= $position ?>" class="manage-column column-id <?= $sortable_classes ?>"<?= is_null($col_style) ? '' : ' style="' . $col_style . '"' ?>>
			<a href="<?= static::buildFilteredUrl($base_page_url, $accepted_filters, $filters, [ 'orderby' => $col_id, 'order' => $asc_desc ]) ?>">
				<span><?= $col_label ?></span>
				<span class="sorting-indicator"></span>
			</a>
		</th>
	<?php
			} else {
	?>
		<th scope="col" id="<?= $col_id ?>-<?= $position ?>" class="manage-column column-id"<?= is_null($col_style) ? '' : ' style="' . $col_style . '"' ?>>
			<?= $col_label ?>
		</th>
	<?php
			}
		}
	}

	protected function renderTableNav($id, $base_page_url, $accepted_filters, $filters, $actions, $pagination_data, $position, $filters_fields = []) 
	{
		$total = $pagination_data['total'];
		$last_page = $pagination_data['last_page'];
		$paged = $pagination_data['current_page'];

		$args = Params::request();
	?>
			<div class="tablenav <?= $position ?>">
	<?php
				if (!empty($actions['bulk_actions'])) {
	?>
				<div class="alignleft actions bulkactions">
					<label for="bulk-action-selector-<?= $position ?>" class="screen-reader-text">
						Seleziona l'azione di gruppo
					</label>
					<select name="action" id="bulk-action-selector-<?= $position ?>">
						<option value="-1">Azioni di gruppo</option>
					<?php
						foreach ($actions['bulk_actions'] as $action) {
							$action_id = $action['id'];
							$action_label = $action['label'];
							$action_method = isset($action['method']) ? $action['method'] : 'POST';
							$action_confirm = isset($action['confirm']) ? $action['confirm'] : '';

							$action_selected = '';
							if (isset($args['action'])
									&& Utils::startsWith($args['action'], 'bulk_')
									&& $args['action'] === 'bulk_' . $action_id) {
								$action_selected = ' selected';
							}
					?>
						<option value="<?= $id . '--' . $action_id ?>"
										data-method="<?= $action_method ?>"
										data-confirm="<?= $action_confirm ?>"
										<?= $action_selected ?>
										class="hide-if-no-js">
							<?= $action_label ?>
						</option>
					<?php
						}
					?>
					</select>
					<input type="submit" id="doaction-<?= $position ?>" data-select-action="bulk-action-selector-<?= $position ?>" class="button action bulk-action" value="Applica">
				</div>

				<?php
					if ($position === 'top' && !empty($filters_fields)) {
				?>
					<div id="table-filters" class="alignleft actions">
					<?php
						$reset_filters_fields = [];
						foreach ($filters_fields as $filter_field) {
							$filter_field_id = $filter_field['id'];
							$reset_filters_fields[$filter_field_id] = null;

							$field_type = isset($filter_field['type'])
								? $filter_field['type']
								: 'select';
							$label = isset($filter_field['label'])
								? $filter_field['label']
								: $filter_field['id'];
							$placeholder = isset($filter_field['placeholder'])
								? $filter_field['placeholder']
								: $label;
							$value = isset($args[$filter_field_id]) && !empty($args[$filter_field_id])
								? $args[$filter_field_id] . ''
								: '';

							switch($field_type) {

								/**
								 * Type Select
								 */
								case 'select':
					?>
						<label for="filter-by-<?= $filter_field_id ?>" class="screen-reader-text"><?= $label ?></label>
						<select name="filter-by-<?= $filter_field_id ?>"
										id="filter-by-<?= $filter_field_id ?>"
										data-filter="<?= $filter_field_id ?>"
										class="table-filter">
							<option value="-1"><?= $filter_field['default_option'] ?></option>
						<?php
							foreach ($filter_field['options'] as $option) {
								$selected = ($value === $option['value'])
									? ' selected'
									: '';
						?>
							<option<?= $selected ?> value="<?= $option['value'] ?>"><?= $option['label'] ?></option>
						<?php
							}
						?>
						</select>
					<?php
								break;
								/**
								 * /End Type Select
								 */

								/**
								 * Type text
								 */
								case 'text':
						?>
							<label for="filter-by-<?= $filter_field_id ?>" class="screen-reader-text"><?= $label ?></label>
							<input name="filter-by-<?= $filter_field_id ?>"
										id="filter-by-<?= $filter_field_id ?>"
										data-filter="<?= $filter_field_id ?>"
										placeholder="<?= $placeholder ?>"
										type="text"
										class="table-filter"
										value="<?= $value ?>">
						<?php
								break;
								/**
								 * /End Type text
								 */

								/**
								 * Type date
								 */
								case 'date':
						?>
										<label for="filter-by-<?= $filter_field_id ?>" class="screen-reader-text"><?= $label ?></label>
										<input name="filter-by-<?= $filter_field_id ?>"
													id="filter-by-<?= $filter_field_id ?>"
													data-filter="<?= $filter_field_id ?>"
													type="text"
													placeholder="<?= $placeholder ?>"
													class="table-filter filters_date"
													value="<?= $value ?>">
						<?php
								break;
								/**
								 * /End Type date
								 */

							}
						}
						$reset_filters_fields['paged'] = null;
					?>
						<input type="submit"
									 data-filter-href="<?= static::buildFilteredUrl($base_page_url, $accepted_filters, $filters, $reset_filters_fields) ?>"
									 name="filter_action"
									 id="post-query-submit"
									 class="button"
									 value="Filtra"/>
					</div>
				<?php
					}
				?>

	<?php
				}
	?>
				<div class="tablenav-pages"><span class="displaying-num"><?= $total ?> elementi</span>
					<span class="pagination-links">
						<?php if (($paged - 1) > 1): ?>
						<a class="first-page" href="<?= static::buildFilteredUrl($base_page_url, $accepted_filters, $filters, [ 'paged' => 1 ]) ?>">
							<span aria-hidden="true">«</span>
						</a>
						<?php else: ?>
						<span class="tablenav-pages-navspan" aria-hidden="true">«</span>
						<?php endif;
						if (($paged - 1) > 0): ?>
						<a class="prev-page" href="<?= static::buildFilteredUrl($base_page_url, $accepted_filters, $filters, [ 'paged' => $paged - 1 ]) ?>">
							<span aria-hidden="true">‹</span>
						</a>
						<?php else: ?>
						<span class="tablenav-pages-navspan" aria-hidden="true">‹</span>
						<?php endif; ?>
						<span class="paging-input"><?= $paged ?> di <span class="total-pages"><?= $last_page ?></span></span>
						<?php if (($paged) < $last_page): ?>
						<a class="next-page" href="<?= static::buildFilteredUrl($base_page_url, $accepted_filters, $filters, [ 'paged' => $paged + 1 ]) ?>">
							<span aria-hidden="true">›</span>
						</a>
						<?php else: ?>
						<span class="tablenav-pages-navspan" aria-hidden="true">›</span>
						<?php endif; ?>
						<?php if (($paged + 1) < $last_page): ?>
						<a class="last-page" href="<?= static::buildFilteredUrl($base_page_url, $accepted_filters, $filters, [ 'paged' => $last_page ]) ?>">
							<span aria-hidden="true">»</span>
						</a>
						<?php else: ?>
						<span class="tablenav-pages-navspan" aria-hidden="true">»</span>
						<?php endif; ?>
					</span>
				</div>
				<br class="clear">
			</div>
	<?php
	}

	protected function buildFilteredUrl($base_page_url, $accepted_filters, $filters, $new_filters = []) 
	{
		$url = $base_page_url;
		$filters = array_merge($filters, $new_filters);
		if (!empty($filters)) {
			$tmp = [];
			foreach ($filters as $filter_name => $filter_value) {
				if (!in_array($filter_name, $accepted_filters) || $filter_value === null) {
					continue;
				}
				$tmp[] = $filter_name . '=' . $filter_value;
			}
			if (!empty($tmp)) {
				$url .= '&' . implode('&', $tmp);
			}
		}

		return $url;
	}

	protected function render($render_params) 
	{
		$args = Params::request();

		$id = $render_params['id'];
		$columns = $render_params['columns'];
		$slug = $render_params['slug'];
		$name = $render_params['name'];
		$per_page = $render_params['per_page'];
		$emptymessage = $render_params['emptymessage'];
		$singularname = $render_params['singularname'];
		$actions = $render_params['actions'];
		$styles = $render_params['styles'];
		$searchbox = $render_params['searchbox'];
		$subsubsub = $render_params['subsubsub'];
		$filters_fields = $render_params['filters_fields'];

		$accepted_filters = [ 'paged', 'limit', 'orderby', 'order' ];
		if ($searchbox) {
			$accepted_filters[] = 's';
		}

		if (!empty($filters_fields)) {
			foreach ($filters_fields as $filter_field) {
				$accepted_filters[] = $filter_field['id'];
			}
		}

		$reset_subsubsub = [];
		$current_found = false;
		foreach ($subsubsub as $subsubsub_index => $subsubsub_element) {
			$reset_subsubsub[$subsubsub_element['key']] = null;
			if (!in_array($subsubsub_element['key'], $accepted_filters)) {
				$accepted_filters[] = $subsubsub_element['key'];
			}
			if (isset($args[$subsubsub_element['key']])
					&& $args[$subsubsub_element['key']] === $subsubsub_element['value']) {
				$subsubsub[$subsubsub_index]['current'] = 1;
				$current_found = true;
			}
		}
		if (!empty($subsubsub)) {
			array_unshift(
				$subsubsub,
				[
					'label' => 'Tutti',
					'current' => $current_found ? 0 : 1
				]
			);
		}

		$base_page_url = 'admin.php?page=' . $slug . '.php';
		$filters = $args;

		if (empty($columns)) {
			$columns = [];
			do_action_ref_array('aeria_table_' . $id . '_get_columns', [ &$columns ]);
			$columns = apply_filters('aeria_table_' . $id . '_format_columns', $columns);
		}

		$cols = [];
		foreach ($columns as $column) {
			$col_id = null;
			$col_label = null;
			$col_style = null;
			$col_is_sortable = false;
			if (is_string($column)) {
				$col_id = $column;
				$col_label = $column;
			} else if (is_array($column)) {
				$col_id = isset($column['id']) ? $column['id'] : null;
				$col_label = isset($column['label']) ? $column['label'] : $col_id;
				$col_style = isset($column['style']) ? $column['style'] : null;
				$col_is_sortable = isset($column['sortable']) ? (bool)$column['sortable'] : false;
			}
			if (is_null($col_id)) {
				continue;
			}
			$cols[] = [
				'id' => $col_id,
				'label' => $col_label,
				'style' => $col_style,
				'sortable' => $col_is_sortable
			];
		}

		$paged = isset($args['paged']) ? $args['paged'] : 1;
		$limit = isset($args['limit']) ? $args['limit'] : $per_page;

		$has_bulk_actions = !empty($actions['bulk_actions']);
		$has_row_actions = !empty($actions['row_actions']);

		$num_cols = count($cols) + ($has_bulk_actions ? 1 : 0) + ($has_row_actions ? 1 : 0);

		$prechecked_rows = [];
		if ($has_bulk_actions && isset($args['prechecked'])) {
			$prechecked = explode(',', $args['prechecked']);
			// pre-checked rows
			foreach ($prechecked as $prechecked_id) {
				if (is_numeric($prechecked_id)) {
					$prechecked_rows[$prechecked_id] = 1;
				}
			}
		}

		$to_filter = [];
		$order_by = null;
		$order = null;
		foreach ($filters as $filter_name => $filter_value) {
			 if ($filter_name === 'orderby') {
				$order_by = $filter_value;
			} else if ($filter_name === 'order') {
				$order = $filter_value;
			} else if (in_array($filter_name, $accepted_filters)) {
				$to_filter[$filter_name] = $filter_value;
			}
		}

		// retrieve source
		$source_data = [
			'results' => [],
			'pagination' => [
				'total' => 0,
				'per_page' => 0,
				'current_page' => 1,
				'last_page' => 0
			]
		];
		do_action_ref_array('aeria_table_' . $id . '_get_source', [ &$source_data, $paged, $limit, $to_filter, $order_by, $order ]);
		$source = $source_data['results'];
		$source = apply_filters('aeria_table_' . $id . '_format_source', $source, $paged, $limit, $to_filter, $order_by, $order);

		$pagination_data = isset($source_data['pagination'])
			? $source_data['pagination']
			: [
				'total' => 0,
				'per_page' => 0,
				'current_page' => 1,
				'last_page' => 0
			];

		// we prepare a query_params fragment with no base url and all filters
		$rerender_filters = static::buildFilteredUrl('', $accepted_filters, $filters, []);
		?>
		<div id="wrap-table--<?= $id ?>" class="wrap-table">
			<div id="table-overlay" class="block-overlay">
				<img alt="" src="images/spinner-2x.gif"/>
			</div>
			<input type="hidden"
						 id="rerender-filters"
						 name="rerender-filters"
						 value="<?= $rerender_filters ?>"/>

		<?php
			if (!empty($subsubsub)) {
		?>
			<ul class="subsubsub">
			<?php
				foreach ($subsubsub as $subsubsub_index => $subsubsub_element) {
					$is_last = $subsubsub_index == (count($subsubsub) - 1);
					$is_current = isset($subsubsub_element['current']) && $subsubsub_element['current'] == 1;

					$key = isset($subsubsub_element['key']) ? $subsubsub_element['key'] : null;
					$value = isset($subsubsub_element['value']) ? $subsubsub_element['value'] : null;
					$label = $subsubsub_element['label'];

					$this_subsubsub = array_merge(
						$reset_subsubsub,
						($key !== null && $value !== null ? [ $key => $value ] : []),
						[ 'paged' => null ]
					);
			?>
				<li>
					<a href="<?= static::buildFilteredUrl($base_page_url, $accepted_filters, $filters, $this_subsubsub) ?>"<?= ($is_current ? ' class="current"' : '') ?>>
						<?= $label ?>
					</a>
					<?= (!$is_last ? ' |' : '') ?>
				</li>
			<?php
				}
			?>
			</ul>
		<?php
			}
		?>

		<?php
			if ($searchbox) {
		?>
			<p class="search-box">
				<label class="screen-reader-text" for="search-input">Ricerca <?= $singularname ?></label>
				<input type="search" id="table-search-input" name="s" value="<?= (isset($args['s']) ? $args['s'] : '') ?>"/>
				<input type="submit"
							 data-search-href="<?= static::buildFilteredUrl($base_page_url, $accepted_filters, $filters, [ 'paged' => null, 's' => null ]) ?>"
							 id="table-search-submit"
							 class="button"
							 value="Ricerca <?= $name ?>"/>
			</p>
		<?php
			}
		?>

			<?php
				$this->renderTableNav($id, $base_page_url, $accepted_filters, $filters, $actions, $pagination_data, 'top', $filters_fields);
			?>

			<table aria-label="" class="wp-list-table widefat fixed striped posts aeria-table <?= $id ?>-table" data-table-id="<?= $id ?>">
				<thead>
					<tr>
					<?php
						$this->renderColumns($columns, $actions, $styles, $has_bulk_actions, $base_page_url, $accepted_filters, $filters, 'top');
					?>
					</tr>
				</thead>
				<tbody id="<?= $id ?>--table">
				<?php
					if (empty($source)) {
				?>
					<tr style="text-align:center;font-weight:700;" class="no-items"><td class="colspanchange" colspan="<?= $num_cols ?>"><?= $emptymessage ?></td></tr></tbody>
				<?php
					} else {
						foreach ($source as $index => $row) {
				?>
					<tr id="<?= $id . '--' . $row['id'] ?>" class="index-<?= $index ?>">
				<?php
							if ($has_bulk_actions) {
								$checked = isset($prechecked_rows[$row['id']]) && $prechecked_rows[$row['id']] == 1;
				?>
					<th scope="row" class="check-column">
						<label class="screen-reader-text" for="cb-select-<?= $row['id'] ?>">
							Seleziona
						</label>
						<input id="cb-select-<?= $row['id'] ?>" class="row-check" type="checkbox" name="post[]" value="<?= $row['id'] ?>"<?= $checked ? ' checked' : '' ?>/>
					</th>
				<?php
							}
							foreach ($cols as $col) {
								$col_id = $col['id'];
								$col_label = $col['label'];
								$col_style = $col['style'];

								$value = isset($row[$col_id]) ? $row[$col_id] : '-';

								// format column value
								$value = apply_filters(
									'aeria_table_format_value_' . $id . '_' . $col_id,
									$value,
									$row
								);
				?>
						<td class="<?= $col_id ?> column-<?= $col_id ?>"
								data-colname="<?= $col_id ?>"
								data-col-label="<?= $col_label ?>"
								<?= ($col_style !== null ? ' style="' . $col_style . '"' : '') ?>>
							<?= $value ?>
						</td>
				<?php
							}

							if (!empty($actions['row_actions'])) {
				?>
						<td class="actions column-actions" data-colname="actions" style="text-align: right;">
							<?php
								foreach ($actions['row_actions'] as $action) {
									$action_id = $action['id'];
									$action_label = $action['label'];
									$action_method = isset($action['method']) ? $action['method'] : 'POST';
									$action_icon = isset($action['icon']) ? $action['icon'] : null;
									$with_icon_class = ($action_icon == null ? '' : ' with-icon');
									$action_confirm = isset($action['confirm']) ? $action['confirm'] : '';
							?>
							<a class="button tips <?= $action_id ?> <?= $action_id ?>-<?= $id ?><?= $with_icon_class ?> row-action"
								 id="row_<?= $action_id . '_' . $row['id'] ?>"
								 data-action-id="<?= $id . '--' . $action_id ?>"
								 data-id="<?= $row['id'] ?>"
								 data-index="<?= $index ?>"
								 data-method="<?= $action_method ?>"
								 data-confirm="<?= $action_confirm ?>"
								 style="float: right;">
								<?php
									if ($action_icon !== null) {
								?>
								<span class="dashicons <?= $action_icon ?>" title="<?= $action_label ?>">&nbsp;</span>
								<?php
									} else {
								?>
								<?= $action_label ?>
							<?php
									}
							?>
							</a>
							<?php
								}
							?>
						</td>
				<?php
							}
				?>
					</tr>
				<?php
						}
					}
				?>
				</tbody>
				<tfoot>
					<tr>
					<?php
						$this->renderColumns($columns, $actions, $styles, $has_bulk_actions, $base_page_url, $accepted_filters, $filters, 'bottom');
					?>
					</tr>
				</tfoot>
			</table>

			<?php
				$this->renderTableNav($id, $base_page_url, $accepted_filters, $filters, $actions, $pagination_data, 'bottom');
			?>

		</div>
		<?php
		return $source;
	}

	public function register($o) {
		$o = Conf::load($o);
		if (!isset($o['id'])) {
			throw new \Exception("Table: Missing attribute 'id'");
		}

		$id = Utils::getParam($o, 'id');
		$name = Utils::getParam($o, 'name', $id);
		$singularname = Utils::getParam($o, 'singularname', $name);

		$columns = Utils::getParam($o, 'columns', null);

		$capability = Utils::getParam($o, 'capability', 'edit_posts');

		$options = Utils::getParam($o, 'options', []);

		$per_page = Utils::getParam($o, 'per_page', static::$default_limit);

		$searchbox = Utils::getParam($o, 'searchbox', false);

		$actions = Utils::getParam($o, 'actions', []);

		$row_actions_style = Utils::getParam($o, 'row_actions_style', null);

		$subsubsub = Utils::getParam($o, 'subsubsub', []);

		$filters_fields = Utils::getParam($o, 'filters_fields', []);

		if (!isset($actions['row_actions'])) {
			$actions['row_actions'] = [];
		}
		if (!isset($actions['bulk_actions'])) {
			$actions['bulk_actions'] = [];
		}
		if (!isset($actions['single_actions'])) {
			$actions['single_actions'] = [];
		}

		// preformat common row_actions
		$handlers_reference = [];
		foreach ($actions as $group => $group_actions) {
			$to_remove = [];
			foreach ($group_actions as $action_index => $action) {
				if (is_string($action)) {
					switch ($action) {
						case 'add':
							$actions[$group][$action_index] = [
								'id' => 'add',
								'label' => 'Aggiungi',
								'icon' => 'dashicons-plus'
							];
							break;

						case 'edit':
							$actions[$group][$action_index] = [
								'id' => 'edit',
								'label' => 'Modifica',
								'icon' => 'dashicons-edit',
								'method' => 'POST'
							];
							break;

						case 'delete':
							$actions[$group][$action_index] = [
								'id' => 'delete',
								'label' => 'Elimina',
								'icon' => 'dashicons-trash',
								'method' => 'POST',
								'confirm' => 'Sei sicuro di voler rimuovere '
									. ($group_actions === 'bulk'
										? 'questi elementi?'
										: 'questo elemento?'
									)
							];
							break;

						default:
							$actions[$group][$action_index] = [
								'id' => $action,
								'label' => $action
							];
							break;
					}
				} else if (is_array($action)) {
					if (!isset($action['id'])) {
						$to_remove[$action_index];
						continue;
					}
					$action_id = $action['id'];
					$action_label = isset($action['label']) ? $action['label'] : $action_id;
					$actions[$group][$action_index] = [
						'id' => $action_id,
						'label' => $action_label
					];
					if (isset($action['icon'])) {
						$actions[$group][$action_index]['icon'] = $action['icon'];
					}
					if (isset($action['method'])) {
						$actions[$group][$action_index]['method'] = $action['method'];
					}
					if (isset($action['confirm'])) {
						$actions[$group][$action_index]['confirm'] = $action['confirm'];
					}
					if (isset($action['handler'])) {
						$handlers_reference[$group . '__' . $action_id] = $action['handler'];
					}
				}
			}
			// in case of wrongly compiled actions, remove 'em
			$to_remove = array_reverse($to_remove);
			foreach ($to_remove as $to_remove_index) {
				array_splice($actions[$group], $to_remove_index, 1);
			}
		}
		// actions are in float right, so we reverse 'em to mantain visual order
		$actions['row_actions'] = array_reverse($actions['row_actions']);

		$emptymessage = Utils::getParam($o, 'emptymessage', 'Nessun elemento');

		$slug = 'table--' . $id;

		$styles = [];
		if (!is_null($row_actions_style)) {
			$styles['row_actions_style'] = $row_actions_style;
		}

		$render_params = [
			'id' => $id,
			'columns' => $columns,
			'slug' => $slug,
			'name' => $name,
			'per_page' => $per_page,
			'emptymessage' => $emptymessage,
			'singularname' => $singularname,
			'actions' => $actions,
			'styles' => $styles,
			'searchbox' => $searchbox,
			'subsubsub' => $subsubsub,
			'filters_fields' => $filters_fields
		];

		$view = function() use ($render_params, $handlers_reference, $name) {
			$id = $render_params['id'];
			$actions = $render_params['actions'];
	?>
	<div id="manage-table--<?= $id ?>" class="wrap manage-table manage-table--<?= $id ?>">
	   <h2>
		   <?= $name ?>
		   <?php
			   foreach ($actions['single_actions'] as $action) {
				   $action_id = $action['id'];
					$action_label = $action['label'];
					$action_method = isset($action['method']) ? $action['method'] : 'POST';
					$action_confirm = isset($action['confirm']) ? $action['confirm'] : '';
		   ?>
		   <a class="page-title-action <?= $action_id ?>-<?= $id ?> single-action"
				id="single_<?= $action_id ?>"
				data-action-id="<?= $id . '--' . $action_id ?>"
				 data-method="<?= $action_method ?>"
				 data-confirm="<?= $action_confirm ?>">
			   <?= $action['label'] ?>
		   </a>
		   <?php
			   }
		   ?>
	   </h2>

	   <br/>

	<?php
			$source = $this->render($render_params);
	?>
	</div>
	<script>
		// ----------- before scripts ---
	<?php
		do_action('aeria_table_' . $id . '_before_scripts');
	?>
		// ----------- scripts ---
		window.aeriaTableData = {};
		window.aeriaTableData.rowsReference = <?= json_encode($source, JSON_NUMERIC_CHECK) ?>;
		window.aeriaTableData.handlersReference = {};
	<?php
		foreach ($handlers_reference as $action_id => $handler) {
			echo "window.aeriaTableData.handlersReference['{$action_id}'] = {$handler};";
		}
	?>

		jQuery(function() {
			jQuery('.filters_date').datepicker({ dateFormat: 'yy-mm-dd' });
		});

		// ----------- after scripts ---
	<?php
		do_action('aeria_table_' . $id . '_after_scripts');
	?>
	</script>
	<style>
		.aeria-table .actions.column-actions .button {
			float: left;
			display: inline-block;
			margin-right: 5px;
		}
		.aeria-table .actions.column-actions .button.with-icon {
			padding: 0;
		}
		.aeria-table .actions.column-actions .button span.dashicons {
			padding: 3px 5px;
		}
		.wrap-table {
			position: relative;
		}
		.block-overlay {
			position: absolute;
			z-index: 1000;
			background: rgba(255, 255, 255, 0.6);
			display: none;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
		}
		.block-overlay > img {
			position: fixed;
			top: 50%;
			left: 50%;
			margin-top: -20px;
			margin-left: -20px;
			width: 40px;
			height: 40px;
		}
	<?php
		do_action('aeria_table_' . $id . '_styles');
	?>
	</style>
	<?php
			do_action('aeria_table_' . $id . '_after_styles');
		};

		$args = [
			'title'			=> $name,
			'slug'			=> $slug,
			'capability'	=> $capability,
			'icon'			=> (isset($options['menu_icon']) ? $options['menu_icon'] : ''),
			'position'		=> isset($options['position']) ? $options['position'] : 'last',
			'view'			=> $view,
		];

		Ajax::add(
			'aeria_table_ajax_' . $id . '_get_rendered_source',
			function() use($render_params) {
				try {
					$source_result = [];
					$table = Response::buffer(function() use($render_params, &$source_result) {
						$source_result = $this->render($render_params);
					});

					Response::success(
						[
							'table' => $table,
							'rows_reference' => $source_result
						],
						"Retrieved `" . $render_params['name'] . "`"
					);
				} catch (\Exception $e) {
					Response::error(
						"Error: could not retrieve `" . $render_params['name'] . "` ('" . $e->getMessage() . "')"
					);
				}
			}
		);

		Menu::add($args);

		do_action('aeria_table_registered_' . $id, $o);
	}

}

class Table {

	public static function register($o) {
		TableHandler::getInstance()->register($o);
	}

}