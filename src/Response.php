<?php

namespace Aeria;

class Response {

	private static $buffer_mode = false;

	public static function startBufferMode() {
		static::$buffer_mode = true;
	}

	public static function stopBufferMode() {
		static::$buffer_mode = false;

		return ob_get_clean();
	}

	public static function buffer(callable $func) {
		if (!is_callable($func)) {
			throw new \Exception("'buffer' needs a callable to be passed");
		}

		static::startBufferMode();

		ob_start();

		$func();

		return static::stopBufferMode();
	}

	public static function success($data = [], $mes = '') {
		header('Content-Type: text/json');
		if (is_string($data) && $mes === '') {
			$mes = $data;
		}

		echo (json_encode([
			'success'		=> true,
			'data'			=> $data,
			'message'		=> $mes
		], JSON_NUMERIC_CHECK));
		if (!static::$buffer_mode) {
			wp_die();
		}
	}

	public static function error($mes = '', $code = 500) {
		header('Content-Type: text/json');
		echo (json_encode([
			'success'		=> false,
			'error'			=> true,
			'code'			=> $code,
			'message'		=> $mes
		]));
		if (!static::$buffer_mode) {
			http_response_code($code);
			wp_die();
		}
	}

}