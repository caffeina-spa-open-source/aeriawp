<?php

namespace Aeria;

class TypeHandler {

	use Singleton;

	protected static $registered_types = [];

	protected function __construct() {
		/**
		 * Expose a generic ajax endpoint to search posts with the specified
		 * post_types (that must be passed)
		 */
		Ajax::add('search_posts_head', function() {
			$args = static::getArgs();

			$results = static::search($args);

			$data = [
				'posts' => $results
			];

			if (isset($args['post_types']) && count($args['post_types']) == 1) {
				$data['hierarchical'] = is_post_type_hierarchical($args['post_types'][0]);
			}

			$data = apply_filters('search_posts_head_filter', $data, $args);

			Response::success(
				$data,
				"Searched '{$args['needle']}'"
					. (!empty($args['post_types'])
						? " in '" . implode(',', $args['post_types']) . "'"
						: "")
			);
		});

		/**
		 * Expose a generic ajax endpoint to search posts with the specified
		 * post_types (that must be passed) mapped as a list of select's options
		 */
		Ajax::add('options_posts_head', function() {
			$args = static::getArgs();
			Response::success(
				static::mapInOptions(static::search($args)),
				"Searched '{$args['needle']}'"
					. (!empty($args['post_types'])
						? " in '" . implode(',', $args['post_types']) . "'"
						: "")
			);
		});

		add_theme_support('post-thumbnails');
	}

	/**
	 * Search posts with the specified post_type; if no params are specified, it
	 * returns all the posts with the specified post_type(s) ordered by menu_order
	 * (ASC) and date (DESC).
	 * @param  string $post_type
	 * @param  string $needle
	 * @return array             a list with the searched posts, with only ID
	 *                           and title (indexed respectively by 'id' and
	 *                           'description')
	 */
	protected static function search(array $params = []) {
		$post_types = isset($params['post_types'])
			? (array)$params['post_types']
			: static::$registered_types;
		$needle = isset($params['needle']) ? (string)$params['needle'] : null;
		$taxonomy = isset($params['taxonomy']) ? (string)$params['taxonomy'] : null;
		$terms = isset($params['terms']) ? (array)$params['terms'] : [];
		$order_by = isset($params['orderby']) ? (string)$params['orderby'] : null;
		$ascdesc = isset($params['order']) ? (string)$params['order'] : null;

		if (empty($order_by)) {
			// $order_by = [
			// 	'menu_order'	=> 'ASC',
			// 	'date'				=> 'DESC'
			// ];
			$order_by = 'menu_order';
			$ascdesc = 'ASC';
		}

		$post_parent = isset($params['post_parent']) ? (int)$params['post_parent'] : -1;
		$post_id = isset($params['post_id']) ? (int)$params['post_id'] : -1;
		$limit = isset($params['limit']) ? (int)$params['limit'] : -1;
		$offset = isset($params['offset']) ? (int)$params['offset'] : 0;

		$args = [];
		if (!empty($post_types)) {
			$args['post_type'] = $post_types;
		} else {
			$args['post_type'] = static::$registered_types;
		}

		if (!empty($order_by)) {
			$args['orderby'] = $order_by;
		}
		if (!empty($ascdesc)) {
			$args['order'] = $ascdesc;
		}
		if (!empty($taxonomy)) {
			$tax_query = [
				'taxonomy' 	=> $taxonomy,
				'field'			=> 'slug',
				'operator'	=> 'EXISTS'
			];
			if (!empty($terms)) {
				$tax_query['terms'] = $terms;
				$tax_query['operator'] = 'IN';
			}
			$args['tax_query'] = [ $tax_query ];
		}

		if ($post_parent > -1) {
			$args['post_parent'] = $post_parent;
		}

		$args['offset'] = $offset;
		$args['posts_per_page'] = $limit;

		if ($post_id > 0) {
			$args['post__in'] = [ $post_id ];
		}

		if (!empty($needle)) {
			Filter::add(
				'posts_where',
				function($where, &$_wp_query) use($needle) {
					global $wpdb;
					return $where . $wpdb->prepare(
						' AND LOWER(' . $wpdb->posts . '.post_title) REGEXP %s',
						strtolower($needle)
					);
				},
				2 // num args
			);

			$posts_query = new \WP_Query($args);
			$posts = $posts_query->have_posts()
				? $posts_query->posts
				: [];
		} else {
			$posts = get_posts($args);
		}

		$results = [];
		foreach ($posts as $post) {
			$results[] = [
				'id' 					=> $post->ID,
				'description' => $post->post_title,
				'type'				=> $post->post_type,
				'typedescr'		=> $post->post_type,
				'order'				=> $post->menu_order,
				'parent'			=> $post->post_parent
			];
		}

		return $results;
	}

	/**
	 * Maps the result of the search method in an array of pairs <value, label>.
	 * @param  array  $posts
	 * @return array          list of pairs <value, label>
	 */
	protected static function mapInOptions(array $posts) : array {
		return array_map(function($post) {
			return [
				'value'	=> $post['id'],
				'label'	=> $post['description']
			];
		}, $posts);
	}

	protected static function getArgs($post_type = '') {
		$args = Params::request();

		$params = [];

		$params['needle'] = isset($args['needle'])
			? $args['needle']
			: '';
		$params['orderby'] = isset($args['orderby'])
			? $args['orderby']
			: '';
		$params['order'] = isset($args['order'])
			? $args['order']
			: '';

		$params['limit'] = isset($args['limit'])
			? $args['limit']
			: -1;
		$params['offset'] = isset($args['offset'])
			? $args['offset']
			: -1;

		$post_types = [];
		if (!empty($post_type)) {
			$post_types = [ $post_type ];
		} else {
			if (isset($args['post_type'])) {
				$post_types = [ $args['post_type'] ];
			} else if (isset($args['post_types'])) {
				$post_types = explode(',', $args['post_types']);
				array_walk($post_types, 'trim');
			}
		}

		$params['post_types'] = $post_types;

		if (isset($args['parent_slug'])) {
			$parent_page = get_page_by_path(
				$args['parent_slug'],
				OBJECT,
				isset($args['parent_type']) ? $args['parent_type'] : 'page'
			);
			if ($parent_page) {
				$params['post_parent'] = $parent_page->ID;
			}
		}

		if (isset($args['parent_id'])) {
			$params['post_parent'] = (int)$args['parent_id'];
		}

		$params['taxonomy'] = isset($args['taxonomy'])
			? $args['taxonomy']
			: '';
		if (isset($args['term'])) {
			$terms = [ $args['term'] ];
		} else if (isset($args['terms'])) {
			$terms = explode(',', $args['terms']);
		} else {
			$terms = [];
		}

		$params['terms'] = $terms;

		$params['post_id'] = isset($args['post_id'])
			? (int)$args['post_id']
			: -1;

		return $params;
	}

	public function register($o) {
		$o = Conf::load($o);
		if (!isset($o['id'])) {
			throw new \Exception("Type: Missing attribute 'id'");
		}

		$id = Utils::getParam($o, 'id');
		$force = Utils::getParam($o, 'force', false);

		if (!$force && static::exists($id)) {
			throw new \Exception("post_type '$id' already exists");
		}

		if (!in_array($id, static::$registered_types)) {
			static::$registered_types[] = $id;
		}

		$default_options = [
			'label' => Utils::getParam($o, 'name', $id)
		];
		$options = Utils::getParam($o, 'options', $default_options);
		if (!isset($options['public'])) {
			$options['public'] = true;
		}

		$metas = Utils::getParam($o, 'metas', []);
		$columns = Utils::getParam($o, 'columns', []);
		$taxonomies = Utils::getParam($o, 'taxonomies', []);

		$menu = Utils::getParam($o, 'menu', false);

		$notices = Utils::getPluralParam($o, 'notice', 'notices');
		$reorder = Utils::getParam($o, 'reorder', false);

		$menu_voice = $options['label'];

		/**
		 * Fix some classic 'errors' that could be made inside the options
		 */
		if (isset($options['supports'])) {
			// calls the eventual callable
			$options['supports'] = Utils::getParam(
				$options,
				'supports',
				$options['supports']
			);
			if (is_string($options['supports'])) {
				// strip spaces to prevent situations like "title, editor"
				$options['supports'] = explode(
					',',
					str_replace(' ', '', $options['supports'])
				);
			}
		}

		try {
			/**
			 * Register Post Type
			 */
			if (static::exists($id)) {
				Action::add(
					'register_post_type_args',
					function($args, $post_type) use($options) {
						return array_merge($args, $options);
					},
					2 // num arguments
				);
			} else {
				$options = apply_filters(
					'aeria_register_type_options',
					$options,
					$id
				);
				$p = register_post_type($id, $options);
			}

			/**
			 * Menu-voice
			 */
			if ($menu !== false) {
				$menu_icon = (isset($options['menu_icon']) ? $options['menu_icon'] : '');
				Menu::replace(
					'post_type=' . $id,
					[
						'title'				=> $menu,
						'capability'	=> 'edit_posts',
						'slug'				=> 'post_type=' . $id,
						'icon'				=> $menu_icon,
						'children'		=> [
							[
								'title'				=> $menu_voice,
								'capability'	=> 'edit_posts',
								'slug'				=> 'post_type=' . $id,
								'position'		=> 'first'
							]
						]
					]
				);
			}

			/**
			 * Register Metaboxes
			 */
			foreach ($metas as $meta) {
				if (!is_array($meta)) {
					throw new \Exception(
						"Type '{$id}': Array needed to define Metabox"
					);
				} else if (isset($meta['type']) || isset($meta['types'])) {
					throw new \Exception(
						"Type '{$id}': Metas cannot specify Type(s) when defined inside a "
						. "Type definition"
					);
				}

				$meta['type'] = $id;
				Meta::register($meta);
			}

			/**
			 * Register SyncMap
			 */
			if (Settings::isEnabled('modules.syncmaps', true)) {
				$sync_to = Utils::getParam($o, 'sync_to_table', false);
				if ($sync_to === false) {
					$sync_to = Utils::getParam($o, 'sync', false);
				}

				$sync_id = ($sync_to === true ? $id : $sync_to);

				$map = Utils::getParam($o, 'sync_map', []);

				// we loop through metas and metas-fields; this is not optimized as we
				// loop through them inside the Meta registration too; still, this is a
				// quick operation and doing it here gives us the opportunity to
				// register the SyncMap without waiting the 'add_meta_boxes_[post_type]'
				// event.
				foreach ($metas as $meta) {
					foreach ($meta['fields'] as $field) {
						$sync_to_col = Utils::getParam($field, 'sync_to_column', false);
						if ($sync_to_col === false) {
							$sync_to_col = Utils::getParam($field, 'sync', false);
						}

						if ($sync_to_col !== false) {
							// here we can retrieve 'id' directly, as it would have failed
							// during:
							// 		Meta::register($meta);
							// in case of missing attribute 'id', both for the 'meta' and
							// 'meta_field' elements.
							$column_name = ($sync_to_col === true
								? $field['id']
								: $sync_to_col);
							if (isset($map[$column_name])) {
								throw new \Exception("Type: Column '{$column_name}' already "
									. "present in '{$sync_id}/{$id}' mapping");
							}
							$map[$column_name] = $meta['id'] . '--' . $field['id'];
						}
					}
				}

				if ($sync_to !== false && !empty($map)) {
					$sync_obj = [
						'id'	=> $sync_id,
						'type'=> $id,
						'map'	=> $map
					];

					$pk = Utils::getParam($o, 'primary_key');
					if (!is_null($pk)) {
						$sync_obj['primary_key'] = $pk;
					}
					SyncMap::register($sync_obj);
				}
			}

			/**
			 * Register Columns
			 */
			if (!empty($columns)) {
				if (!is_array($columns)) {
					throw new \Exception(
						"Type '{$id}': Array needed to define Columns"
					);
				} else if (isset($columns['type']) || isset($columns['types'])) {
					throw new \Exception(
						"Type '{$id}': Columns cannot specify Type(s) when defined "
						. "inside a Type definition"
					);
				}

				$columns['type'] = $id;
				Columns::register($columns);
			}

			/**
			 * Register Taxonomies
			 */
			foreach ($taxonomies as $taxonomy) {
				if (is_string($taxonomy) || Utils::isCallableParam($taxonomy)) {
					$taxonomy = ['id' => $taxonomy];
				} if (!is_array($taxonomy)) {
					throw new \Exception(
						"Type '{$id}': Array or String needed to define a Taxonomy"
					);
				} else if (isset($taxonomy['type']) || isset($taxonomy['types'])) {
					throw new \Exception(
						"Type '{$id}': Taxonomies cannot specify Type(s) when "
						. "defined inside a Type definition"
					);
				}

				$taxonomy['type'] = $id;
				Taxonomy::register($taxonomy);
			}

			/**
			 * Register Notices
			 */
			foreach ($notices as $index => $notice) {
				if (is_string($notice) || Utils::isCallableParam($notice)) {
					$notice = ['message' => $notice];
				} else if (!is_array($notice)) {
					throw new \Exception(
						"Type '{$id}': Array or String needed to define a Notice"
					);
				} else if (isset($notice['type']) || isset($notice['types'])) {
					throw new \Exception(
						"Type '{$id}': Notices cannot specify Type(s) when defined inside "
							. "a Type definition"
					);
				}

				$notice['type'] = $id;

				Notice::register($notice);
			}

			/**
			 * Enable posts reorder
			 */
			if ($reorder) {
				if (is_array($reorder)
						&& (isset($reorder['type']) || isset($reorder['types']))) {
					throw new \Exception(
						"Type '{$id}': Reorder cannot specify Type(s) when defined inside "
							. "a Type definition"
					);
				}

				Reorder::register(['type' => $id]);
			}

			/**
			 * Register Sections
			 */
			if (Settings::isEnabled('modules.sections', true)) {
				$sections = Utils::getParam($o, 'sections', []);

				foreach ($sections as $section) {
					if (!is_array($section)) {
						throw new \Exception(
							"Type '{$id}': Array needed to define Section"
						);
					} else if (isset($section['type']) || isset($section['types'])) {
						throw new \Exception(
							"Type '{$id}': Sections cannot specify Type(s) when defined "
							. "inside a Type definition"
						);
					}

					$section['type'] = $id;
					Sections::register($section);
				}
			}

			/**
			 * Expose an ajax endpoint to search this post_type's posts
			 */
			Ajax::add('search_' . $id . '_posts', function() use ($id) {
				Response::success(
					[
						'posts'					=> static::search(static::getArgs($id)),
						'hierarchical'	=> is_post_type_hierarchical($id)
					],
					"Searched '$id'"
				);
			});

			/**
			 * Expose an ajax endpoint to search this post_type's posts mapped as a
			 * list of select's options
			 */
			Ajax::add('options_' . $id . '_posts', function() use ($id) {
				Response::success(
					static::mapInOptions(static::search(static::getArgs($id))),
					"Searched '$id'"
				);
			});

			/**
			 * Trigger custom event 'type_registered'
			 */
			do_action('type_registered', $id, $o);

			/**
			 * Trigger custom event 'type_[post_type]_registered'
			 */
			do_action('type_' . $id . '_registered', $o);
		} catch (\Exception $e) {
		    wp_die('Type Error: ' . $e->getMessage());
		}
	}

	/**
	 * Proxy function to 'post_type_exists' (to eventually add future features,
	 * like checking that the post_type was create through Aeria)
	 */
	public static function exists($post_type) {
		return post_type_exists($post_type);
	}

}

class Type {

	public static function register($o) {
		TypeHandler::getInstance()->register($o);
	}	

	/**
	 * Proxy function to 'post_type_exists' (to eventually add future features,
	 * like checking that the post_type was create through Aeria)
	 */
	public static function exists($post_type) {
		TypeHandler::exists($post_type);
	}

}