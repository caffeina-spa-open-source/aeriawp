<?php

namespace Aeria\Helpers;

class CustomWalkerCategoryCheckList extends \Walker_Category_Checklist {

	protected $radio = false;

	protected $ordered = false;

	public function __construct(array $opts = []) {
		$this->radio = isset($opts['radio']) ? (bool)$opts['radio'] : false;
		$this->ordered = isset($opts['ordered']) ? $opts['ordered'] : false;
	}

	public function walk($elements, $max_depth, ...$args) 
	{
		if ($this->ordered !== false) {
			if (is_callable($this->ordered)) {
				$elements = $this->ordered($elements);
			} else if (is_array($this->ordered)) {
				// default ordered works only for the first level of depth and uses the
				// basic order of ; if you want
				// more, do your own damn sortin' function, pal.
				$ordered_elements = [];
				foreach ($this->ordered as $term) {
					$filtered_element = array_filter($elements, function($element) use($term) {
						return $element->name === $term;
					});
					if (!empty($filtered_element) && count($filtered_element) == 1) {
						$el_index = array_keys($filtered_element)[0];
						$ordered_elements[] = $elements[$el_index];
						unset($elements[$el_index]);
					}
				}
				foreach ($elements as $key => $value) {
					$ordered_elements[] = $value;
				}
				$elements = $ordered_elements;
			}
		}
		$new_args = array_merge([ $elements, $max_depth ], $args);
		return parent::walk(...$new_args);
	}

	public function start_el(&$output, $category, $depth = 0, $args = [], $id = 0) {
		parent::start_el($output, $category, $depth, $args, $id);
		if ($this->radio) {
			if (empty($args['list_only'])) {
				$output = str_replace('type="checkbox"', 'type="radio"', $output);
			}
		}
	}

}