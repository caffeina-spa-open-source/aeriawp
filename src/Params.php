<?php

namespace Aeria;

class ParamsHandler {

	use Singleton;

	protected $get_args;

	protected $post_args;

	protected $request_args;

	protected $backup = [
		'get'			=> [],
		'post'		=> [],
		'request'	=> []
	];

	protected $mocking = false;

	protected function setValues(array $get_values, array $post_values) {
		foreach ($get_values as $key => $val) {
			$this->get_args[$key] = $val;
			$this->request_args[$key] = $val;
		}
		foreach ($post_values as $key => $val) {
			$this->post_args[$key] = $val;
			$this->request_args[$key] = $val;
		}
	}

	protected function __construct() {
		$this->setValues($_GET, $_POST);
	}

	public function startMocking(array $get_values, array $post_values) {
		if ($this->mocking) {
			throw new \Exception("Already in mocking mode");
		}
		$this->mocking = true;
		$this->backup['get'] = $this->get_args;
		$this->backup['post'] = $this->post_args;
		$this->backup['request'] = $this->request_args;
		$this->setValues($get_values, $post_values);
	}

	public function stopMocking() {
		if (!$this->mocking) {
			throw new \Exception("Not in mocking mode");
		}
		$this->get_args = $this->backup['get'];
		$this->post_args = $this->backup['post'];
		$this->request_args = $this->backup['request'];
		$this->mocking = false;
		$this->backup['get'] = [];
		$this->backup['post'] = [];
		$this->backup['request'] = [];
	}

	public function mock(array $get_values, array $post_values, callable $func) {
		$this->startMocking($get_values, $post_values);
		$func();
		$this->stopMocking();
	}

	public function get() : array {
		return $this->get_args ?? [];
	}

	public function post() : array {
		return $this->post_args ?? [];
	}

	public function request() : array {
		return $this->request_args ?? [];
	}

}

/**
 * Proxy class to handle request params; gives rapid access to $_POST, $_GET and
 * $_REQUEST arguments and gives the ability to 'mock' the arguments of a
 * request with custom values (as long as the request handler uses this same
 * class to retrieve the arguments), falling back to the original arguments
 * when finished.
 *
 * NOTA: right now it mocks only the 3 array $_POST, $_GET and $_REQUEST; it
 * could obviously be extended to also handle $_FILES, $_COOKIE, $_SERVER,
 * header informations, etc.
 */
class Params {

	public static function startMocking(array $get_values, array $post_values) {
		ParamsHandler::getInstance()->startMocking($get_values, $post_values);
	}

	public static function stopMocking() {
		ParamsHandler::getInstance()->stopMocking();
	}

	public static function mock(
			array $get_values,
			array $post_values,
			callable $func) {
		ParamsHandler::getInstance()->mock($get_values, $post_values, $func);
	}

	public static function get() : array {
		return ParamsHandler::getInstance()->get();
	}

	public static function post() : array {
		return ParamsHandler::getInstance()->post();
	}

	public static function request() : array {
		return ParamsHandler::getInstance()->request();
	}

}
