<?php

namespace Aeria;

class Shortcode {

	static public function register(
			$tag,
			callable $callback,
			$default_params = []) {
    add_shortcode($tag, function($attrs, $content = '')
    		use (&$callback, &$default_params){
			$params = wp_parse_args($attrs, (array)$default_params);
			ob_start();
			$args = array_values($params);
			array_unshift($args, empty($content) ?  '' : $content);
			$callback(...$args);
			//call_user_func_array($callback, [$args, empty($content) ?  '' : $content]);
			$buffer = ob_get_contents();
			ob_end_clean();
			return $buffer;
    });
	}

}