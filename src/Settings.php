<?php

namespace Aeria;

/**
 * Registry of settings related to Aeria.
 * Uses an internal dictionary accessible through dot-notation, so we can set
 * elements through array or one at a time and access them the same way.
 * 
 * For example:
 *
 * both:
 * ```
 * 		Aeria\Settings::set('modules.jobsqueue.enabled', true);
 * 		Aeria\Settings::set('modules.jobsqueue.tablename', 'jobsqueue');
 *```
 * and:
 * ```
 * 		Aeria\Settings::set([
 * 			'modules'	=> [
 * 				'jobsqueue' => [
 * 					'enabled' => true,
 * 					'tablename' => 'jobsqueue'
 * 				]
 * 			]
 * 		]);
 * ```
 * will have the same effect:
 * ```
 * 		print_r(Aeria\Settings::get('modules.jobsqueue'));
 * 		// Array ( [enabled] => 1 [tablename] => jobsqueue )
 * ```
 * 
 * The 'get' method accept a 'default' parameter that specifies the default
 * value if there is no specified setting.
 */
class Settings {

	protected static $settings = [];

	protected static function getDotAccess(string $key, $default = null, array &$repo = null) {
		$pieces = explode('.', $key, 2);
		if (!isset($repo[$pieces[0]])) {
			return $default;
		}
		if (count($pieces) == 1) {
			return $repo[$pieces[0]];
		}
		return static::getDotAccess($pieces[1], $default, $repo[$pieces[0]]);
	}

	protected static function setDotAccess(string $key, $value, array &$repo = null) {
		$pieces = explode('.', $key, 2);
		if (count($pieces) == 1) {
			$repo[$pieces[0]] = $value;
			return;
		}
		if (!isset($repo[$pieces[0]])) {
			$repo[$pieces[0]] = [];
		}
		return static::setDotAccess($pieces[1], $value, $repo[$pieces[0]]);
	}

	protected static function recursiveSet(array $opts, string $prefix = '') {
		foreach ($opts as $key => $val) {
			if (!is_array($val)) {
				static::setDotAccess($prefix . $key, $val, static::$settings);
			} else {
				static::recursiveSet($val, $prefix . $key . '.');
			}
		}
	}

	public static function set($opts, $value = null) {
		if (is_string($opts)) {
			if (is_null($value)) {
				throw new \Exception("Settings: cannot set '{$opts}' without "
					. "specifying its value");
			}
			static::setDotAccess($opts, $value, static::$settings);
		} else if (is_array($opts)) {
			if (!is_null($value)) {
				throw new \Exception("Settings: cannot specify the value ('{$value}') "
					. "when setting multiple arguments through an array");
			}
			static::recursiveSet($opts);
		} else {
			throw new \Exception("Settings: cannot set '{$opts}'; must pass a string "
				. "or an array as the first parameter");
		}
	}

	public static function get(string $key, $default = null) {
		return static::getDotAccess($key, $default, static::$settings);
	}

	/**
	 * Lookup the attribute 'enabled' as a child of the specified attribute and
	 * look if it exists and if it is true.
	 */
	public static function isEnabled(string $key, $default = null) {
		return (bool)(static::get($key . '.enabled', (bool)$default)) == true;
	}

	/**
	 * Opposite of 'isEnabled'.
	 */
	public static function isDisabled(string $key, $default = null) {
		return (bool)(static::get($key . '.enabled', !((bool)$default))) == false;
	}

}