<?php

namespace Aeria;

interface CacheDriver {

	public function get($key, $group);

	public function set($data, $key, $group, $expire);

	public function delete($key, $group);

	public function deleteGroup($group);

	public function clear();

}

/**
 * Default driver; does nothing.
 */
class CacheBypassDriver implements CacheDriver {

	public function get($key, $group = '') {
		return false;
	}

	public function set($data, $key, $group = '', $expire = 0) { }

	public function delete($key, $group = '') { }

	public function deleteGroup($group) { }

	public function clear() { }

}

/**
 * Cache Facade
 */
class Cache {

	protected static $driver;

	protected static $cache_hashes = [];

	protected static $driver_name = "CacheBypassDriver";

	public static function setDriver(string $new_driver) {
		$new_driver = Utils::startsWith($new_driver, 'Cache')
			? $new_driver
			: 'Cache' . $new_driver;
		$new_driver = Utils::endsWith($new_driver, 'Driver')
			? $new_driver
			: $new_driver . 'Driver';
		$new_driver = 'Aeria\\Cache\\' . $new_driver;

		if (!class_exists($new_driver)) {
			throw new \Exception("Unknown Cache Driver '$new_driver'");
		}

		static::$driver = new $new_driver();
	}

	protected static function getDriver() {
		if (static::$driver == null) {
			static::setDriver(static::$driver_name);
		}
		return static::$driver;
	}

	public static function hash($key) {
		return (is_object($key) || is_array($key)) ? sha1(serialize($key)) : $key;
	}

	public static function set($data, $key, $group = false, $expire = 0) {
		$d = static::getDriver();
		$hash = static::hash($key);
		$data = serialize($data);
		if (!isset(static::$cache_hashes[$group])) {
			static::$cache_hashes[$group] = [];
		}
		static::$cache_hashes[$group][$hash] = time();
		return $d::set($data, $hash, $group, $expire);
	}

	public static function get($key, $group = false, $default = null) {
		$d = static::getDriver();
		$hash = static::hash($key);
		$r = $d::get($hash, $group, $default);
		return is_serialized($r) ? unserialize($r) : $r;
	}

	public static function delete($key, $group = false){
		$d = static::getDriver();
		$hash = static::hash($key);
		return $d::delete($hash, $group);
	}

	public static function deleteGroup($group) {
		$d = static::getDriver();
		$d::deleteGroup($group);
	}

	public static function clear() {
		$d = static::getDriver();
		$d::clear();
	}

}