<?php

namespace Aeria;

trait Syncable {

	abstract public function tableName() : string;

	abstract public function getSyncMap() : array;

	public function syncMapData() {
		$map = $this->getSyncMap();
		$data = [];
		foreach ($map as $column_name => $target_name) {
			$callable_name = 'get' . Utils::studly($target_name) . 'Attribute';
			if (method_exists($this, $callable_name)) {
				$data[$column_name] = $this->{$callable_name}();
			} else if (isset($this->{$target_name})) {
				$data[$column_name] = $this->{$target_name};
			} else if (isset($this->_t_post) && isset($this->_t_post->{$target_name})) {
				$data[$column_name] = $this->_t_post->{$target_name};
			} else if (isset($this->fields) && isset($this->fields->{$target_name})) {
				$data[$column_name] = $this->fields->{$target_name};
			} else {
				$data[$column_name] = null;
			}
		}
		return $data;
	}

	public function sync() {
		global $wpdb;

		return ($wpdb->replace($this->tableName(), $this->syncMapData()) == 1);
	}

}