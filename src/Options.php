<?php

namespace Aeria;

class OptionsHandler {

	use Singleton;

	protected $grouped_options = [];

	protected function __construct() {
		Assets::addScript('aeriaVendors', '/aeria/assets/js/aeria-vendors.js', ['in_footer' => true]);
		Assets::addScript('aeria', '/aeria/assets/js/aeria.js', ['in_footer' => true]);

		// enqueue client-side media API
		Action::add('admin_enqueue_scripts', 'wp_enqueue_media');

		// Small sql snippet to load multiple options at once
		DBScripts::load(__DIR__ . '/db/AeriaOptions.sql');
	}

	protected static function getOptionFields(array $fields, string $prefix)
			: array {
		foreach ($fields as $index => $field) {
			$field_name = $prefix . $field['id'];

			$option_value = get_option($field_name);
			if (!empty($option_value)) {
				$fields[$index]['value'] = $option_value;
			}
		}

		return $fields;
	}

	protected static function saveOptionsPage(string $id, array $args) {
		if (!check_ajax_referer('options-form-' . $id)) {
			throw new \Exception("Error: invalid 'nonce'");
		}

		$errors = [];
		$option_fields = [];
		foreach ($args as $key => $value) {
			try {
				if (!Utils::startsWith($key, $id . '--')) {
					continue;
				}
				$option_fields[$key] = get_option($key);

				$value = apply_filters(
					'aeria_option_' . $key . '_update',
					stripcslashes($value),
					$key,
					$option_fields
				);

				if ($option_fields[$key] !== $value) {
					if (!update_option($key, $value)) {
						$errors[] = "L'opzione '{$key}' non è stata aggiornata";
					} else {
						$option_fields[$key] = $value;
					}
				}
			} catch(\Exception $e) {
				$errors[] = "Errore durante l'aggiornamento dell'opzione '{$key}': "
					. $e->getMessages();
			}
		}

		if (!empty($errors)) {
			throw new \Exception(implode('\n', $errors));
		}

		do_action('aeria_save_option_' . $id, $option_fields);

		do_action('aeria_save_option', $id, $option_fields);
	}

	public function register($o = []) 
	{
		$o = Conf::load($o);

		if (!isset($o['id'])) {
			throw new \Exception("Aeria\Options: Missing attribute 'id'");
		}

		if (!isset($o['fields'])) {
			throw new \Exception("Aeria\Options: Missing attribute 'fields'");
		}

		$id = Utils::getParam($o, 'id');
		$fields = Utils::getParam($o, 'fields', []);
		$title = Utils::getParam($o, 'title', $id);
		$async = Utils::getParam($o, 'async', false);

		$prefix = $id . "--";

		Ajax::add("get_{$id}_options_fields", function() use ($id, $title, $fields, $prefix) {
			Response::success(static::getOptionFields($fields, $prefix), 'Options: ' . $id);
		});

		Ajax::add("save_options_{$id}", function() use ($id, $title) {
			$args = Params::post();

			if (isset($args['option_page']) && $args['option_page'] == $id) {
				try {
					static::saveOptionsPage($id, $args);
					Response::success("Opzioni '{$title}' aggiornate con successo!");
				} catch (\Exception $e) {
					Response::error($e->getMessage());
				}
			} else {
				Response::error("The passed 'option_page' ('"
					. $args['option_page']
					. "') is different from the expected one ('"
					. $id
					. "')");
			}
		});

		$view = function() use ($id, $title, $fields, $prefix, $async) {
			$args = Params::post();

			$notice = [];

			if (isset($args['action']) && $args['action'] === 'save_options_' . $id) {

				try {
					static::saveOptionsPage($id, $args);
					$notice['message'] = "Opzioni '{$title}' aggiornate con successo!";
					$notice['type'] = 'updated';
					$notice['dismissible'] = true;
				} catch (\Exception $e) {
					$notice['message'] = $e->getMessage();
					$notice['type'] = 'error';
					$notice['dismissible'] = true;
				}

			}
		?>
			<div class="wrap">
				<h2><?= $title; ?></h2>
			<?php
				if (!empty($notice)) {
					Notice::render(
						$notice['message'],
						$notice['type'],
						$notice['dismissible']
					);
				}
			?>
				<form id="options-form" method="POST">
					<?php wp_nonce_field('options-form-' . $id); ?>
					<input
						type="hidden"
						id="option_page_id"
						name="option_page"
						value="<?= $id; ?>"/>
					<input type="hidden" name="action" value="save_options_<?= $id; ?>"/>
					<div>
					<?php
						$meta_descr = [
							'id'		=> $id,
							'async' 	=> $async,
							'fields'	=> $async
								? [] 
								: static::getOptionFields($fields, $prefix)
						];
					?>
						<script>
							window.aeriaMetaboxes.push(<?= json_encode($meta_descr) ?>);
						</script>
						<div
							id="aeriaHidden-<?= $id ?>"
							class="aeriaHidden"
							data-aeriametabox
							data-aeriametaboxid="<?= $id ?>">
							<input
								type="hidden"
								name="aeria--meta[]"
								value="<?= $id ?>"
							/>
					<?php
						foreach ($meta_descr['fields'] as $meta_field) {
							$field_name = $id . '--' . $meta_field['id'];
					?>
							<input
								type="hidden"
								id="aeriaField--<?= $field_name ?>"
								name="<?= $field_name ?>"
								value="<?= htmlentities($meta_field['value'] ?? null) ?>"
							/>
					<?php
							do_action(
								'aeria_render_option_field_' . $meta_field['type'],
								[
									'options_title'	=> $title,
									'metabox_id'		=> $id,
									'meta_field'		=> $meta_field
								]
							);
						}
					?>
						</div>
						<div id="aeriaApp-<?= $id ?>" class="aeriaApp"></div>
					</div>
					<p class="submit">
						<input
							type="submit"
							name="submit"
							id="submit"
							class="button button-primary"
							value="Salva le modifiche"
							<?= ($async) ? 'disabled="disabled"' : '' ?>/>
					</p>
				</form>
			</div>
			<?php
		};

		Menu::add([
			'parent'		=> 'options-general',
			'title'			=> $title,
			'slug'			=> 'options--' . $id,
			'capability'	=> 'manage_options',
			'position'		=> 'last',
			'view'			=> $view
		]);
	}

	public function getGroup(string $key) {
		if (Utils::endsWith($key, '--')) {
			$key = substr($key, 0, -2);
		}

		if (!isset($this->grouped_options[$key])) {
			$this->grouped_options[$key] = [];

			foreach (DBScripts::call('SELECT::get_multi_options', [ $key ]) as $option) {
				$grouped_name = str_replace($key . '--', '', $option->option_name);
				$this->grouped_options[$key][$grouped_name] = maybe_unserialize($option->option_value);
			}
		}

		return $this->grouped_options[$key];
	}

	public static function get(string $key, string $field_name = null) {
		if (!empty($field_name)) {
			$key .= '--' . $field_name;
		}
		$option_val = get_option($key);
		return apply_filters('aeria_format_option_' . $key, $option_val);
	}

}

class Options {

	public static function register($o = []) {
		OptionsHandler::getInstance()->register($o);
	}	

	public static function get(string $key, string $field_name = null) {
		return OptionsHandler::get($key, $field_name);
	}

	public static function getGroup(string $key) {
		return OptionsHandler::getInstance()->getGroup($key);
	}

}