<?php

namespace Aeria;

class ReorderHandler {

	use Singleton;

	protected function __construct() {
		Assets::addScript('ui-sortable', '/aeria/assets/js/jquery-ui-sortable.js');
		Assets::addScript('list', '/aeria/assets/js/jquery.mjs.nestedSortable.js');
		Assets::addScript('aeriaUtils', '/aeria/assets/js/aeriaUtils.js');
		Assets::addScript('aeriaReorder', '/aeria/assets/js/aeriaReorder.js');

		Ajax::add('reorder_search_all_posts_head', function() {
			do_action('before_reorder_search_all_posts_head');

			$posts = get_posts([
				'post_type'		=> 'any',
				'orderby'			=> 'menu_order',
				'order'				=> 'asc',
				'numberposts'	=> -1,
				'offset'			=> 0
			]);

			$results = [];
			foreach ($posts as $post) {
				$results[] = [
					'id' 					=> $post->ID,
					'description' => $post->post_title,
					'type'				=> $post->post_type,
					'typedescr'		=> $post->post_type,
					'order'				=> $post->menu_order,
					'parent'			=> $post->post_parent
				];
			}

			$data = [
				'posts' => $results,
				'hierarchical' => true
			];

			$data = apply_filters('reorder_search_all_posts_head_filter', $data);

			Response::success(
				$data,
				"All posts hierarchy"
			);
		});

		Ajax::add('reorder_search_posts_head', function() {
			do_action('before_reorder_search_posts_head');
			do_action('wp_ajax_search_posts_head');
		});

		Ajax::add('save_posts_order', function() {
			$args = Params::request();

			if (! isset($args['posttype'])) {
				throw new \Exception("'save_posts_order': no postType specified");
			}

			$order = isset($args['order'])
				? json_decode(str_replace("\\", "", $args['order']), true)
				: [];

			try {
				DBScripts::transaction(function() use ($order) {
					static::sortPosts($order);
				});
				Response::success([], "New order saved");
			} catch (\Exception $e) {
				Response::error(
					"Reorder: Cannot save new post order ('" . $e->getMessage() . "')"
				);
			}
		});
	}

	private static function sortPosts($order, $parent_id = 0) {
		for ($index = 0; $index < count($order); ++$index) {
			$post_data = $order[$index];

			$args = [
				'ID' => $post_data['id'],
				'menu_order' => $index + 1,
				'post_parent' => $parent_id
			];

			$outcome = wp_update_post($args, true);
			if (is_wp_error($outcome)) {
				throw new \Exception("Reorder: an error occurred while saving the new "
					. "posts order: '" . $outcome->get_error_message() . "'");
			}

			if (isset($post_data['children']) && !empty($post_data['children'])) {
				static::sortPosts($post_data['children'], $post_data['id']);
			}
		}
	}

	public function register($o) {
		$o = Conf::load($o);
		if (!isset($o['global']) && !isset($o['type']) && !isset($o['types'])) {
			throw new \Exception("Missing attribute 'type' in Notice definition");
		}

		$global_reorder = Utils::getParam($o, 'global', false);
		if ($global_reorder) {
			$post_types = [ '__global' ];
		} else {
			$post_types = Utils::getPluralParam($o, 'type', 'types');
		}

		$title = Utils::getParam($o, 'title', 'Reorder');

		foreach ($post_types as $post_type) {
			$menu_args = [
				'title'						=> $title,
				'slug'						=> 'reorder--' . $post_type,
				'capability'			=> 'edit_posts',
				'position'				=> 'last',
				'view'						=> function() use ($post_type, $title) {
					?>
					<div class="wrap reorder-page">
						<h2><?= $title; ?></h2>
						<p>Trascina per determinare l'ordine manuale di visualizzazione degli elementi.</p>
						<br/>
						<form id="sort-posts-form" method="POST" action="">
							<input
								type="hidden"
								id="post_type"
								name="post_type"
								value="<?= $post_type; ?>"/>
							<div id="sort-posts-container" data-aeria-sort-posts="<?= $post_type; ?>">
								<div id="post-list"></div>
							</div>
							<div class="fixed-container">
								<div id="fixed-btn" class="fixed">
									<p class="submit">
										<button
											type="submit"
											name="submit"
											id="submit"
											class="button button-primary"
											disabled="disabled">
											Salva le modifiche
										</button>
									</p>
								</div>
							</div>
						</form>
						<style>
							#sort-posts-container {
								box-sizing: border-box;
								padding-left: 25px;
								padding-right: 25px;
							}
							#sort-posts-container li ul {
								margin-left: 30px;
							}
							#sort-posts-container .menu-item-bar .menu-item-handle {
								width: 50%;
							}
							#sort-posts-container .item-title {
								padding-left: 25px;
							}
							#sort-posts-container .edit-post {
								position: absolute;
								top: 10px;
								left: 15px;
								text-decoration: none;
							}
							#sort-posts-container .sortable-placeholder {
								width: 50%;
								height: 42px;
								padding-left: 25px;
							}
							#sort-posts-container #loading-spinner {
								display: block;
								text-align: left;
								padding: 25px 0;
							}
							.reorder-page .spinner {
								float: none;
								visibility: visible;
								margin: 0;
							}
							.reorder-page .fixed-container {
								height: 60px;
								display: inline-block;
								width: 100%;
							}
							.reorder-page .fixed {
								position: fixed;
								bottom: 0;
								width: 100%;
								background: rgba(241, 241, 241, 0.75);
								box-shadow: 0 -5px 5px -5px #333;
								margin-left: -25px;
								padding-left: 50px;
							}
							.reorder-page .fixed.unfixed {
								position: relative;
								background: none;
								box-shadow: none;
								width: auto;
							}
						</style>
					</div>
					<?php
				}
			];
			if (!$global_reorder) {
				$menu_args['parent'] = 'post_type=' . $post_type;
			} else {
				$menu_args['icon'] = 'dashicons-list-view';
			}
			Menu::add($menu_args);
		}
	}

}

class Reorder {

	public static function register($o) {
		ReorderHandler::getInstance()->register($o);
	}

}