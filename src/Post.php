<?php

namespace Aeria;

class Post {

	public 	$id 			= null,
			$filtered_title	= '',
			$title			= '',
			$raw_content	= '',
			$content		= '',
			$status			= '',
			$excerpt		= '',
			$date			= '',
			$created_at		= '',
			$updated_at		= '',
			$slug			= '',
			$permalink		= '',
			$order			= '',
			$type			= '',
			$author			= '',
			$parent			= null,
			// the underlying WP_Post
			$_t_post		= null;

	protected 	$post_type  	= '',
				$_fields 		= null,
				$_sections		= null,
				$_thumbs		= null,
				$_attachments	= null,
				$_tags			= null,
				$_categories	= null,
				$_tax_cache		= [],
				$_featured		= null;

	public function __construct($id, $type = null) 
	{
		if ($id) {
			if (is_object($id)) {
				if (is_a($id, 'WP_Post')) {
					$this->_t_post = $id;
				} else if (is_a($id, 'Post')) {
					$this->_t_post = $id->_t_post;
				}
			} else if (is_numeric($id)) {
				$this->_t_post = get_post($id);
			} else if ($type) {
				$query = [
					'name'		=> $id,
					'post_type'	=> $type
				];
				$this->_t_post = current(get_posts($query));
			} else {
				$this->_t_post = get_page_by_path($id);
			}
		}

		if (!$this->_t_post) {
			throw new \Exception("Post: No WP_Post found for $id"
				. ($type ? " (type: $type)" : ""));
		}

		$t_post = $this->_t_post;

		$this->id = $t_post->ID;
		$this->title = $t_post->post_title;
		$this->filtered_title = apply_filters('the_title', $t_post->post_title);
		$this->raw_content = $t_post->post_content;
		$this->author = $t_post->post_author;
		$this->status = $t_post->post_status;
		$this->content = do_shortcode(apply_filters('the_content', $t_post->post_content));
		$this->excerpt = apply_filters('the_excerpt', $t_post->post_excerpt);
		$this->date = $t_post->post_date;
		$this->created_at = $t_post->post_date;
		$this->updated_at = $t_post->post_modified;
		$this->slug = $t_post->post_name;
		$this->order = $t_post->menu_order ?: 0;
		$this->permalink = '/' . $t_post->post_type . '/' . $t_post->post_name;
		$this->type = $type ?: $t_post->post_type;
		$this->parent = ($t_post->post_parent)
			? new static($t_post->post_parent)
			: null;

		$this->post_type = $this->type;
	}

	public function postType() : string {
		if (empty($this->post_type)) {
			throw new \Exception("Post: Unknown post_type for Post '{$this->id}'");
		}
		return $this->post_type;
	}

	public static function load($id, $type = null) {
		return new static($id, $type);
	}

	public static function loadAsPage($id) {
		$th = new static($id);
		$th->type = 'page';
		return $th;
	}

	/**
	 * Updates the underlying WP_Post on db.
	 * NOTE: this is different than the old update in AeriaPost as we don't
	 * pass the fields to the method: it save the instance current values.
	 * @return bool true on success, false otherwise
	 */
	public function update() {
		$data = [
			'ID'						=> $this->id,
			'post_title'		=> $this->title,
			'post_content'	=> $this->raw_content,
			'post_status'		=> $this->status,
			'post_author'		=> $this->author,
			'post_excerpt'	=> $this->excerpt,
			'post_date'			=> $this->date,
			'post_name'			=> $this->slug,
			'menu_order'		=> $this->order,
			'post_type'			=> $this->type,
			'post_parent'		=> $this->parent
		];
		return ($this->id == wp_update_post($data));
	}

	public function asJSON() {
		$tmp = json_decode(json_encode($this));
		$tmp->tags = $this->tags;
		$tmp->fields = $this->fields;
		$tmp->categories = $this->categories;
		return json_encode($tmp, JSON_NUMERIC_CHECK);
	}

	protected function getMeta() {
		$terms = get_post_meta($this->id);
		$results = [];
		if (!empty($terms) && !is_wp_error($terms)) {
			foreach ($terms as $key => $term) {
				$temp = [];
				foreach ($term as $value) {
					$temp[] = maybe_unserialize($value);
				}
				$final_value = count($temp) == 1 ? $temp[0] : $temp;
				$results[$key] = apply_filters(
					'aeria_format_post_meta_' . $key,
					$final_value,
					$key,
					$this->id
				);
			}
		}
		return (object)$results;
	}

	protected function getGroupedMeta() 
	{
		$grouped = [];
		foreach ($this->fields as $key => $value) {
			$exploded = explode('--', $key, 2);
			$group_name = $exploded[0];
			$field_name = $exploded[1] ?? null;

			if (empty($group_name) || empty($field_name)) {
				continue;
			}

			if (!isset($grouped[$group_name])) {
				$grouped[$group_name] = [];
			}

			$grouped[$group_name][$field_name] = $value;
		}

		return $grouped;
	}

	protected function getThumbs() {
		$results = [];
		if ($att = get_post_thumbnail_id($this->id)) {
			$imgdata = wp_get_attachment_image_src($att, 'full');
			$results['big'] = $imgdata[0];
			$imgdata = wp_get_attachment_image_src($att, 'medium');
			$results['medium'] = $imgdata[0];
			$imgdata = wp_get_attachment_image_src($att, 'thumbnail');
			$results['small'] = $imgdata[0];
		}
		return (object)$results;
	}

	public function featuredURL($full = false) {
		if ($full) {
			$th = $this->get_thumbs();
			return $th->big;
		} else {
			if (null === $this->_featured) {
				if (has_post_thumbnail($this->id)) {
					$this->_featured = wp_get_attachment_thumb_url(
						get_post_thumbnail_id($this->id)
					);
				} else {
					$this->_featured = false;
				}
			}
			return $this->_featured;
		}
	}

	public function taxonomy($taxonomy_name) {
		if (in_array($taxonomy_name, $this->_tax_cache)) {
			return $this->_tax_cache[$taxonomy_name];
		}

		$terms = wp_get_object_terms($this->id, $taxonomy_name);
		if (!empty($terms) && !is_wp_error($terms)) {

			$this->_tax_cache[$taxonomy_name] = array_map(
				function($term) use ($taxonomy_name) {
					return [
						'id'			=> $term->term_id,
						'name'			=> $term->name,
						'slug'			=> $term->slug,
						'permalink'		=> get_term_link($term, $taxonomy_name)
					];
				},
				$terms
			);
		} else {
			$this->_tax_cache[$taxonomy_name] = [];
		}

		return $this->_tax_cache[$taxonomy_name];
	 }

	private function tagRelatedPosts(array $tags, string $type, $opts = []) 
	{
		if (empty($tags)) {
			return [];
		}

		$opts['post_type'] = $type;
		$opts['tax_query'] = [
			[
				'taxonomy'	=> 'post_tag',
				'terms'		=> array_unique($tags),
				'field'		=> 'term_id',
				'operator'	=> 'IN'
			]
		];

		$posts = get_posts($opts);
		$results = [];

		foreach ($posts as $post) {
			$lang = (isset($_GET['lang']) && $_GET['lang'])
				? $_GET['lang']
				: 'it';

			$tmp = new static(real_id($post->id, $type, false, $lang));
			if ($tmp->id) {
				$results[] = $tmp;
			}
		}

		return $results;
	}

	/**
	* All posts related to this one: that means all posts with at least one
	* tag in common with this one.
	*/
	public function relatedPosts($type = null, $opts = []) {
		if (empty($this->tags)) {
			return [];
		}

		$type = $type ?: $this->type;
		$tags = array_map(function($tag) {
			return $tag['id'];
		}, $this->tags);

		return $this->tagRelatedPosts($tags, $type, $opts);
	}

	/**
	 * All posts that have the slug of this post in their tags.
	 */
	public function linkedPosts($type = null, $opts = []) {
		$type = $type ?: $this->type;

		return $this->tagRelatedPosts([$this->slug], $type, $opts);
	}

	/**
	 * Both related and linked posts.
	 */
	public function allRelatedPosts($type = null, $opts = []) {
		return $this->tagRelatedPosts(
			array_merge($this->tags, $this->slug),
			$type,
			$opts
		);
	}

	public function getSections() {
		if (Settings::isDisabled('modules.sections', false)) {
			throw new \Exception("Post: Module Sections is disabled");
		}

		if (!$this->_sections) {
			$this->_sections = Sections::getSavedSections($this->id);
		}

		return $this->_sections;
	}

	public function getSection(string $section_group) {
		if (!$this->_sections) {
			$this->getSections();
		}

		return array_filter($this->_sections, function($section) use($section_group) {
			return $section['sectiongroup'] == $section_group;
		});
	}

	public function __get($name) {
		switch($name) {
			case 'tags':
				if (!$this->_tags) {
					$this->_tags = $this->taxonomy('post_tag');
				}
				return $this->_tags;
				break;
			case 'categories':
				if (!$this->_categories) {
					$this->_categories = $this->taxonomy('category');
				}
				return $this->_categories;
				break;
			case 'fields':
				if (!$this->_fields) {
					$this->_fields = $this->getMeta();
				}
				return $this->_fields;
				break;
			case 'metas':
				return $this->getGroupedMeta();
				break;
			case 'sections':
				return $this->getSections();
				break;
			case 'thumbs':
				if (!$this->_thumbs) {
					$this->_thumbs = $this->getThumbs();
				}
				return $this->_thumbs;
				break;
			case 'attachments':
				if (!$this->_attachments) {
					$this->_attachments = $this->getAttachments();
				}
				return $this->_attachments;
				break;
			default:
				throw new \Exception("Post: Unknown property $name");
		}
	}

	protected function getAttachments() {
		$attachments = get_posts([
			'orderby'			=> 'menu_order ID',
			'order'				=> 'ASC',
			'post_type'		=> 'attachment',
			'post_parent'	=> $this->id,
			'numberposts'	=> -1
		]);

		$results = [];
		foreach ($attachments as $att) {
			$full   = wp_get_attachment_image_src($att->ID, 'full');
			$large  = wp_get_attachment_image_src($att->ID, 'large');
			$small  = wp_get_attachment_image_src($att->ID, 'small');
			$results[] = [
				'id'					=> $att->ID,
				'title'				=> $att->post_title,
				'description'	=> $att->post_excerpt,
				'content'			=> $att->post_content,
				'slug'				=> $att->post_name,
				'src'					=> $full[0],
				'width'				=> $full[1],
				'height'			=> $full[2],
				'thumb'				=> [
					'large'				=> [
						'src'					=> $large[0],
						'width'				=> $large[1],
						'height'			=> $large[2]
					],
					'small'				=> [
						'src'					=> $small[0],
						'width'				=> $small[1],
						'height'			=> $small[2]
					],
				]
			];
		}
			
		return $results;
	}

	public function fieldInfo($field_name) 
	{
		return AeriaMetabox::infoForField($this->type, $field_name);
	}

	public function fieldDisplayValue($field_name) 
	{
		$info = $this->fieldInfo($field_name);
		$raw  = $this->fields->$field_name;
		if (empty($info['options'])) return $raw;
		$value = is_callable($info['options'])
			? call_user_func($info['options'])
			: $info['options'];
		return is_array($value) && isset($value[$raw]) ? $value[$raw] : $raw;
	}

	/**
	 * fieldAsPost function.
	 *
	 * @access public
	 * @param mixed $field_name
	 * @return void
	 */
	public function fieldAsPost($field_name) 
	{
		$res = [];
		if(isset($this->fields->$field_name)) {
			foreach(preg_split('/\s*,\s*/', $this->fields->$field_name) as $_id) {
				$res[] = new self($_id);
			}
		}
		return empty($res)
			? false
			: (count($res) > 1
				? $res
				: $res[0]
			);
	}

	/**
	 * fieldAsURL function.
	 *
	 * @access public
	 * @param mixed $field_name
	 * @return void
	 */
	public function fieldAsURL($field_name) 
	{
		if(isset($this->fields->$field_name)) {
			return $this->fields->$field_name;
		}
	}

	/**
	 * Add a new post to the database.
	 *
	 * @access public
	 * @static
	 * @param mixed $data
	 * @param array $meta (default: array())
	 * @return void
	 */
	public static function insert($data, $meta = [])
	{
		$p_data = [
			'post_status'		=> isset($data['status']) ? $data['status'] : 'publish',
			'post_type'			=> isset($data['type']) ? $data['type'] : 'post',
			'post_author'		=> isset($data['author_id']) ? $data['author_id'] : 1,
			'menu_order'		=> isset($data['order']) ? $data['order'] : 0,
			'post_content'		=> isset($data['content']) ? $data['content'] : '',
			'post_category'		=> isset($data['category']) ? $data['category'] : [],
			'post_title'		=> isset($data['title']) ? $data['title'] : ''
		];

		$post_id = wp_insert_post($p_data);
		foreach((array)$meta as $key => $value) {
			add_post_meta($post_id, $key, $value);
		}
		return new static($post_id);
	}
}

function manual_order_wp($posts) {
	usort(
		$posts,
		function($a, $b) {
			if ($a->menu_order == $b->menu_order) {
				return 0;
			}
			return ($a->menu_order > $b->menu_order) ? 1 : -1;
		}
	);
	return $posts;
}
