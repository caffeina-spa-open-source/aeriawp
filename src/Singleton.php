<?php

namespace Aeria;

/**
 * Singleton trait
 */
trait Singleton {

	private static $instance;

	/**
	 * Method to retrieve the Singleton instance
	 */
	public static function getInstance() {
		if (static::$instance == null) {
			static::$instance = new static();
		}
		return static::$instance;
	}

	final public function __clone() { }

	final public function __wakeup() { }

	protected abstract function __construct();
}
