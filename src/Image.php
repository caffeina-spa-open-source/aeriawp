<?php

namespace Aeria;

class Image 
{
	protected static $focal_point_enabled = false;

	public static function enableFocalPoint() 
	{
		if (static::$focal_point_enabled) {
			return;
		}

		Action::add(
			'attachment_fields_to_edit',
			function($form_fields, $post) {
				if (!static::isImagePost($post)) {
					return $form_fields;
				}

				if (!is_array($form_fields)){
					$form_fields = [];
				}

				$focal_point = static::getFocalPoint($post->ID);

				list($url, $width, $height, $orginal) = wp_get_attachment_image_src($post->ID, 'large');
				$image_id = 'focal-point-image-' . $post->ID;
				$preview_id = 'focal-point-preview-' . $post->ID;

				ob_start();
				?>
				<div class="focal-point-media-upload">
					<script type="text/javascript">
						function initFocalPoint(tries) {
							var tries = tries || 0;
							var picker = jQuery('#<?= $image_id ?>');
							var pickerImg = picker.find('img');
							var w = pickerImg.width();


							if (w == 0) {
								setTimeout(function() {
									if (tries > 20) {
										throw("FocalPoint box is taking too much time to load");
									}
									initFocalPoint(++tries);
								}, 100);
								return;
							}

							var h = pickerImg.height();
							var saving = false;

							var inp = jQuery('input[type="hidden"][name=\'attachments[<?= $post->ID ?>][focal_position]\']');
							var focalPoint = jQuery('<div class="focal-point"/>');
							var container = jQuery('.compat-field-focal_position_picker');
							var loadingOverlay = jQuery('<div class="loading-overlay"/>');
							var resetFocalPoint = jQuery('.reset-focal-point');

							jQuery(document).ajaxComplete(function() {
								picker.children('.loading-overlay').remove();
								saving = false;
							});

							var setPosition = function(x, y, save) {
								if (saving) {
									return;
								}
								focalPoint.css({
									left: x + '%',
									top: y + '%'
								});
								inp.val(JSON.stringify({ x: x, y: y }));
								if (save) {
									saving = true;
									picker.append(loadingOverlay);
									inp.change();
								}
							};
							var setPositionHandler = function(e, save) {
								e.preventDefault();

								if (e.button != 0) {
									return;
								}
								var save = save || false;
								var offset = pickerImg.offset();

								var eX = e.pageX;
								var eY = e.pageY;

								var outOfBoundary = false;

								if (eX < offset.left) {
									eX = offset.left;
									outOfBoundary = true;
								} else if (eX > offset.left + w) {
									eX = offset.left + w;
									outOfBoundary = true;
								}
								if (eY < offset.top) {
									eY = offset.top;
									outOfBoundary = true;
								} else if (eY > offset.top + h) {
									eY = offset.top + h;
									outOfBoundary = true;
								}

								if (outOfBoundary && mouseIsDown) {
									mouseIsDown = false;
									save = true;
								}

								var x = eX - offset.left;
								var y = eY - offset.top;

								x = (x / w) * 100;
								y = (y / h) * 100;

								setPosition(x, y, save);
							};

							picker.append(focalPoint);
							focalPoint.css({
								left: '<?= $focal_point['x'] ?>%',
								top: '<?= $focal_point['y'] ?>%'
							});

							var mouseIsDown = false;

							pickerImg.on({
								mousedown: function(e) {
									mouseIsDown = true;
									setPositionHandler(e);
								},
								mousemove: function(e) {
									if (mouseIsDown) {
										setPositionHandler(e);
									}
								}
							});
							focalPoint.on({
								mousedown: function(e) {
									mouseIsDown = true;
									setPositionHandler(e);
								},
								mousemove: function(e) {
									if (mouseIsDown) {
										setPositionHandler(e);
									}
								},
								mouseup: function(e) {
									if (mouseIsDown) {
										mouseIsDown = false;
										setPositionHandler(e, true);
									}
								}
							});
							container.on({
								mousemove: function(e) {
									if (mouseIsDown) {
										setPositionHandler(e);
									}
								}
							});

							resetFocalPoint.on('click', function() {
								setPosition(50, 50, true);
							})
						}

						jQuery(document).ready(function() {
							initFocalPoint();
						});
					</script>
					<style>
						@keyframes slowglow {
							0% { opacity: 0.7; }
							50% { opacity: 1; }
							100% { opacity: 0.7; }
						}
						@keyframes roundandround {
							0% { transform: rotate(0deg); }
							100% { transform: rotate(360deg); }
						}

						.focal-point-media-upload {
							position: relative;
							padding: 0 10px;
						}
						.focal-point-media-upload .focal-point-picker {
							position: relative;
							display: inline-block;
							font-size: 0;
							line-height: 0;
							padding: 0;
							overflow: hidden;
						}
						.focal-point-media-upload .focal-point-picker img {
							cursor: crosshair;
							max-width: 100%;
							max-height: 100%;
							margin: 0;
						}
						.focal-point-media-upload .focal-point {
							cursor: crosshair;
							position: absolute;
							margin-top: -20px;
							margin-left: -20px;
							width: 40px;
							height: 40px;
							background-color: rgba(255, 255, 255, 0.5);
							border-radius: 50%;
							z-index: 10;
							animation: slowglow 3s infinite;
						}
						.focal-point-media-upload .focal-point:before {
							position: absolute;
							content: '';
							width: 20px;
							height: 20px;
							top: 10px;
							left: 10px;
							background-color: #0073aa;
							opacity: .7;
							display: block;
							border-radius: 50%;
						}
						.focal-point-media-upload .focal-point:after {
							position: absolute;
							content: '';
							width: 1px;
							height: 1px;
							top: 50%;
							left: 50%;
							background-color: #fff;
							display: block;
							border-radius: 50%;
							z-index: 11;
						}
						.compat-field-focal_position_picker th.label {
							display: none;
						}
						.compat-field-focal_position_picker td.field {
							width: 100%;
						}
						.focal-point-media-upload .loading-overlay {
							position: absolute;
							display: inline-block;
							top: 0;
							left: 0;
							right: 0;
							bottom: 0;
							z-index: 12;
							background: rgba(255, 255, 255, .45);
						}
						.focal-point-media-upload .loading-overlay:after {
							position: absolute;
							content: '';
							display: inline-block;
							width: 24px;
							height: 24px;
							top: 50%;
							left: 50%;
							margin-top: -12px;
							margin-left: -12px;
							border-top: 6px solid rgba(0, 0, 0, .7);
							border-right: 6px solid rgba(0, 0, 0, .6);
							border-bottom: 6px solid rgba(0, 0, 0, .5);
							border-left: 6px solid rgba(0, 0, 0, .4);
							border-radius: 50%;
							animation: roundandround .5s infinite linear;
						}
						.focal-point-description, .focal-point-description > * {
							line-height: 16px;
						}
						.focal-point-description .reset-focal-point {
							vertical-align: baseline;
						}
						.focal-point-description .dashicons {
							vertical-align: sub;
						}
					</style>
					<div class="focal-point-description alignleft actions">
						<span class="dashicons dashicons-location"></span>
						<?= __('Clicca sull\'immagine e seleziona il <strong>punto focale</strong>', 'aeria'); ?>
						&nbsp;
						<input type="button" class="button reset-focal-point" value="<?= __('Reset', 'aeria'); ?>"/>
					</div>
					<div id="<?= $image_id; ?>" class="focal-point-picker">
						<img alt="" src="<?= $url; ?>">
					</div>
				</div>
				<?php
				$html = ob_get_contents();
				ob_end_clean();

				$form_fields['focal_position'] = [
					'label' => '',
					'input' => 'hidden'
				];

				$form_fields['focal_position_picker'] = [
					'label' => '',
					'input' => 'html',
					'html'  => $html
				];

				return $form_fields;
			},
			2 // num arguments
		);

		Action::add(
			'attachment_fields_to_save',
			function($post, $attachment) {
				if (!static::isImagePost($post)) {
					return $post;
				}

				if (isset($attachment['focal_position']) && $attachment['focal_position']) {
					$previous_position = static::getFocalPoint($post['ID']);
					$position = json_decode(stripslashes($attachment['focal_position']), JSON_NUMERIC_CHECK);
					update_post_meta($post['ID'], 'focal_position', $position);
				}

				return $post;
			},
			2 // num arguments
		);

		static::$focal_point_enabled = true;
	}

	protected static function getFocalPoint($post_id) 
	{
		$focalPoint = get_post_meta($post_id, 'focal_position', true);

		return $focalPoint ?: [
			'x' => 50,
			'y' => 50
		];
	}

	protected static function isImagePost($post)
	{
		if (is_integer($post)) {
			$post = get_post($post);
		} else if (is_array($post) && isset($post['ID'])) {
			$post = get_post($post['ID']);
		}

		if ($post->post_type != 'attachment'
				|| !Utils::startsWith($post->post_mime_type, 'image/')) {
			return false;
		}

		return true;
	}

	public static function isFocalPointEnabled() 
	{
		return static::$focal_point_enabled;
	}

	/**
	 * Alias of add_image_size.
	 */
	public static function addSize(string $name, int $width, int $height, $crop = false) 
	{
		add_image_size($name, $width, $height, $crop);
	}

	/**
	 * Add a list of sizes.
	 * Shorthand versions (by examples):
	 * 	200					= ['width' => 200, 'height' => 200, 'crop' => false]
	 * 	340x250			= ['width' => 340, 'height' => 250, 'crop' => false]
	 * 	600!				= ['width' => 600, 'height' => 600, 'crop' => true]
	 * 	768x450!		= ['width' => 768, 'height' => 450, 'crop' => true]
	 */
	public static function addSizes(array $sizes)
	{
		foreach ($sizes as $size_name => $size_args) {
			if (is_array($size_args)) {
				$args = $size_args;
			} else {
				$args = [
					'crop' => false
				];
				if (is_string($size_args) && Utils::endsWith($size_args, '!')) {
					$args['crop'] = true;
					$size_args = substr($size_args, 0, -1);
				}
				if (is_numeric($size_args)) {
					$args = [
						'width'		=> (int)$size_args,
						'height'	=> (int)$size_args
					];
				} else if (strpos($size_args, 'x') > 0) {
					list($args['width'], $args['height']) = explode('x', $size_args);
				} else {
					throw new \Exception("Image: unknown size arguments {$size_args}");
				}
			}
			static::addSize(
				$size_name,
				(int)$args['width'],
				(int)$args['height'],
				$args['crop']
			);
		}
	}
}
