<?php

namespace Aeria\Cache;

use Aeria;

/**
 * WordPress driver; uses WordPress transient API to cache data.
 */
class CacheWordPressDriver implements Aeria\CacheDriver {

	public function get($key, $group = '', $default = null) {
		if (null === ($v = get_transient($key)) && $default) {
			$v = is_callable($default)? call_user_func($default) : $default;
			static::set($v, $key, $group);
		}
		return $v;
	}

	public function set($data, $key, $group = '', $expire = 0) {
		return set_transient($key, $data, $expire);
	}

	public function delete($key, $group = '') {
		return delete_transient($key);
	}

	public function clear() { }

	public function deleteGroup($group) { }

}