<?php

namespace Aeria\Cache;

use Aeria;

/**
 * RedisDriver
 */
class CacheRedisDriver implements Aeria\CacheDriver {

	public function & redis() {
		if (!defined('PREDIS_LOADED')) {
			require_once dirname(__DIR__) . '/vendor/Predis/Autoloader.php';
			Predis\Autoloader::register();
			define('PREDIS_LOADED', 1);
		}
		try {
			$client = getenv('PREDIS_CLIENT') !== false
				? getenv('PREDIS_CLIENT')
				: 'tcp://127.0.0.1:6379';
			$redis = new Predis\Client($client);
			return $redis;
		} catch(\Exception $e) {
			die('AeriaCacheRedis Error: ' . $e->getMessage());
		}
	}

	public function get($key, $group = false, $default = null) {
		$r = static::redis();

		try {
			$v = $group ? $r->hget($group,$key) : $r->get($key);
		} catch(\Exception $e) {
			$v = null;
		}

		if (null === $v && $default) {
			$v = is_callable($default) ? call_user_func($default) : $default;
			static::set($v, $key, $group);
		}

		return $v;

	}

	public function set($data, $key, $group = false, $expire = 0) {
		$r = redis();
		try {
			if ($group) {
				return $r->hset($group, $key, $data);
			} else {
				if (!$expire) return $r->set($key, $data);
				else return $r->setex($key, $expire, $data);
			}
		} catch(\Exception $e) {
			return null;
		}
	}

	public function delete($key, $group = false) {
		$r = redis();
		try {
			return $group ? $r->hdel($group,$key) : $r->del($key);
		} catch(\Exception $e) {
			return null;
		}
	}

	public function deleteGroup($group) {
		$r = redis();
		try {
			return $r->delete($group);
		} catch(\Exception $e) {
			return null;
		}
	}

	public function clear() {
		$r = redis();
		return $r->flushall();
	}

}