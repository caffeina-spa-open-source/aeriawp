<?php

namespace Aeria;

interface FileParser {
	public static function parse(string $path) : array;
}

// Default JsonFileParser

class JsonFileParser implements FileParser {

	public static function parse(string $path) : array {
		return json_decode(file_get_contents($path), true);
	}

}

class Conf {

	public static $parsers_list = [];

	public static function registerParser(
			array $extensions,
			string $parser_name,
			bool $replace = false) {
		if (class_exists($parser_name)
				&& in_array('Aeria\\FileParser', class_implements($parser_name))) {
			foreach ($extensions as $ext) {
				$full_ext = '.' . $ext;
				if (isset(static::$parsers_list[$full_ext]) && !$replace) {
					throw new \Exception("There is already a FileParser for '$full_ext' files");
				}
				static::$parsers_list[$full_ext] = $parser_name;
			}
		} else {
			throw new \Exception("Attention: '$parser_name' is not a valid FileParser");
		}
	}

	private static function parseFile(string $path) : array {
		$ext_start = strrpos($path, '.');
		if ($ext_start === false) {
			throw new \Exception("Cannot retrieve file extension from '$path'; add "
				. "extension to the File Name");
		}
		$ext = substr($path, $ext_start);
		if (!isset(static::$parsers_list[$ext])) {
			$accepted = array_keys(static::$parsers_list);
			throw new \Exception("Unknown file extension`$ext`: System accept the "
				. " following formats: `" . implode(PHP_EOL . '` - `', $accepted) . "`");
		}
		return (static::$parsers_list[$ext])::parse($path);
	}

	public static function load($obj) {
		// if it's a file, we load it and parse it
		if (is_string($obj)) {
			if (file_exists($obj)) {
				$obj = static::parseFile($obj);
			}

		}
		// otherwise we return it as is, simply flattening the params
		Utils::flatCallParams($obj);
		return $obj;
	}

}

Conf::registerParser(['json'], JsonFileParser::class);