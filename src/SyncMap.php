<?php

namespace Aeria;

class SyncMapElement {

	protected $table_name,
						$primary_key,
						$map;

	public function tableName() : string {
		return $this->table_name;
	}

	public function primaryKey() : string {
		return $this->primary_key;
	}

	public function __construct($opts) {
		$this->table_name = $opts['table_name'];
		$this->primary_key = $opts['primary_key'];
		$this->map = [];
	}

	public function addMapping(string $column_name, string $target_name) {
		$this->map[$column_name] = $target_name;
	}

	public function hasMapping(string $column_name) {
		return isset($this->map[$column_name]);
	}

	public function getMapping() {
		return $this->map;
	}

}

/**
 * Helper class to sync an Aeria\Post to a table.
 */
class SyncMap {

	protected static $to_sync = [];

	protected static $sync_registry = [];

	public static function register($o) {
		try {
			$o = Conf::load($o);

			$id = Utils::getParam($o, 'id');

			if (!isset($o['id'])) {
				throw new \Exception("Missing attribute 'id'");
			}

			$post_types = Utils::getPluralParam($o, 'type', 'types');
			if (empty($post_types)) {
				throw new \Exception("'{$id}': No post_type(s) specified");
			}

			foreach ($post_types as $post_type) {
				if (is_null($post_type)) {
					throw new \Exception("Missing attribute 'type'");
				}

				$primary_key = Utils::getParam($o, 'primary_key', 'id');

				if (!isset(static::$to_sync[$id])) {
					static::$to_sync[$id] = [];
				}

				if (!isset(static::$to_sync[$id][$post_type])) {
					$syncMapElement = new SyncMapElement([
						'table_name' => $id,
						'primary_key' => $primary_key
					]);
					static::$to_sync[$id][$post_type] = $syncMapElement;
				} else {
					$syncMapElement = static::$to_sync[$id][$post_type];

					if ($syncMapElement->primaryKey() !== $primary_key) {
						throw new \Exception("SyncMapElement '{$id}' already has "
								. "primary key '{$syncMapElement->primaryKey()}'; different "
								. "primary key '{$primary_key}' specified");
					}
				}

				$map = Utils::getParam($o, 'map', []);

				foreach ($map as $column_name => $target) {
					$column_name = (is_integer($column_name) ? $target : $column_name);
					if ($syncMapElement->hasMapping($column_name)) {
						throw new \Exception("Column '{$column_name}' already "
							. "present in '{$id}/{$post_type}' mapping");
					}
					$syncMapElement->addMapping($column_name, $target);
				}
			}

		} catch (\Exception $e) {
			wp_die('SyncMap Error: ' . $e->getMessage());
		}
	}

	public static function get($id, $post_type) {
		if (!isset(static::$to_sync[$id])) {
			throw new \Exception("SyncMap: Unknown SyncMap '{$id}'");
		} else if (!isset(static::$to_sync[$id][$post_type])) {
			throw new \Exception("SyncMap: Unknown SyncMap '{$id}/{$post_type}'");
		}

		return static::$to_sync[$id][$post_type];
	}

	public static function getMapping($id, $post_type) {
		$sync_map_element = static::get($id, $post_type);

		$pk = $sync_map_element->primaryKey();
		$map = $sync_map_element->getMapping();
		if (!isset($map[$pk])) {
			$map[$pk] = $pk;
		}
		return $map;
	}

	public static function record($post_id, string $post_type, string $action) {
		static::$sync_registry[] = [
			'post_id'	=> $post_id,
			'post_type'	=> $post_type,
			'action'	=> $action
		];
	}

	public static function getRegistry() {
		return static::$sync_registry;
	}

}