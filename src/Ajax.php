<?php

namespace Aeria;

/**
 * Wraps the 'add_action' and 'remove_action' functions only for ajax
 * endpoints.
 * The 'wp_ajax_' prefix can be omitted.
 */
class Ajax {

	private static function initBatch() {
		if (has_action('wp_ajax_batch')) {
			return;
		}

		add_action('wp_ajax_batch', function() {
			// maybe add some checks here?
			static::batch();
		});
	}

	public static function batch() {
		
		$args = Params::post();
		if (!isset($args['actions'])) {
			throw new \Exception("You need to specify a list of 'actions' to call "
				. "the 'batch' endpoint");
		}
		if (!is_array($args['actions'])) {
			throw new \Exception("'actions' must be an array");
		}
		$result = [];
		foreach ($args['actions'] as $action) {
			if (!isset($action['action'])) {
				throw new \Exception("Every element of 'actions' needs to specify an "
					. "'action'");
			}

			$action_ref = $action['action'];
			$action_name = 'wp_ajax_' . $action_ref;
			if (!has_action($action_name)) {
				throw new \Exception("Unknown ajax action '$action_name'");
			}

			$get_values = isset($action['get']) ? $action['get'] : [];
			$post_values = isset($action['post']) ? $action['post'] : [];

			Params::mock($get_values, $post_values, function()
					use ($action_name, $action_ref, &$result) {
				$part_response = Response::buffer(function() use ($action_name) {
					do_action($action_name);
				});
				$result[$action_ref] = json_decode($part_response, true);
			});
		}

		Response::success($result, 'Ajax batch call');
	}

	public static function add(
			string $name,
			callable $func,
			int $accepted_args = 1,
			int $priority = 10) {
		if (strpos($name, 'wp_ajax_') === false) {
			$name = 'wp_ajax_' . $name;
		}
		if (!is_callable($func)) {
			throw new \Exception("Must pass a callable as the second argument");
		}

		static::initBatch();
		add_action($name, $func, $priority, $accepted_args);
	}

	public static function remove(
			string $name,
			callable $func,
			int $priority = 10) {
		if (strpos($name, 'wp_ajax_') === false) {
			$name = 'wp_ajax_' . $name;
		}
		remove_action($name, $func, $priority);
	}

}