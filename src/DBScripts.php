<?php

namespace Aeria;

use Aeria\DBScripts\DBScriptTool;

/**
 * Register and call SQL-snippets on WordPress database, indexing them with a
 * snippet-name.
 * It's possible to load a single SQL file with multiple snippets; they should
 * be separated by the snippet-name; example:
 *
 * ```
 * ##! ALL_POSTS !##
 * SELECT * FROM posts
 *
 * ##! SEARCH_USERS !##
 * SELECT * FROM users WHERE username LIKE %s
 * ```
 *
 * will register 2 snippets, `ALL_POSTS` and `SEARCH_USERS`; then it will be
 * possible to call them this way:
 *
 * ```
 * $posts = DBScripts::call('ALL_POSTS');
 * $users = DBScripts::call('SEARCH_USERS', [ 'editor' ]);
 * ```
 *
 * It uses $wpdb `prepare` method, so SQL syntax must follow its rules, with the
 * following extensions:
 * 	-	it's possible to interpolate string using double curly braces; example:
 * 		'{{ prefix }}' will be replaced with the WordPress prefix; below there is
 * 		a list of the tweenable terms;
 *  - as it's possible to pass arguments when calling a SQL-snippet in a
 *  	`sprintf`-way, defining SQL query with variable list of arguments is
 *  	problematic; example:
 *
 *  		'SELECT * FROM table WHERE id IN ( ... )'
 *
 *  	to do that, it is possible to use the double brackets and pass an array
 *  	as the argument; example:
 *
 * 		```
 *   	##! GET_POSTS !##
 *    SELECT * FROM posts WHERE title LIKE %s AND id IN ( [[ %d ]] )
 *    ```
 *
 * 		calling it this way:
 *
 * 		```
 * 		$posts = DBScripts::call('GET_POSTS', [ 'Example', [15, 18, 22] ])
 * 		```
 *
 * 		will produce this query:
 *
 * 		```
 *    SELECT * FROM posts WHERE title LIKE 'Example' AND id IN (15, 18, 22)
 *    ``` 		
 *
 * Other special params can be defined by extending the DBScriptTool interface.
 *
 * tweenable terms:
 *  - 'prefix' => $wpdb->prefix;
 *  - 'dbname' => $wpdb->dbname.
 *
 * It's possible to add other terms to interpolate through the `addInterpData`
 * method, which accepts a key and a value.
 *
 * NOTE: it's also possible to pass a third parameter to `DBScripts::call` and
 * `DBScripts::exec` specifying how to execute the query; the third parameter is
 * an array with the following attributes:
 * 	- 'get_results': (true = `$wpdb->get_results`, false = `$wpdb->query`);
 * 	- 'single': (if 'get_results' is true, we can pass 'single' as true to get
 * 							the first element of the query-result).
 * If those attributes aren't specified, `DBScripts` will try and guess how to
 * set them; in fact it will:
 * 	- use `$wpdb->get_results` if the query starts with a 'SELECT' statement;
 * 		`$wpdb->query` otherwise;
 * 	- if it's using `$wpdb->get_results`, it will return a single element
 * 		instead of an array if the query contains a 'LIMIT' statement at the end
 * 		of the query with a `row_count` of 1.
 */
class DBScriptsHandler {

	use Singleton;

	protected $scripts_list = [];

	protected $wpdb;

	protected $interp_data = [];

	protected function __construct() {
		global $wpdb;

		$this->wpdb = $wpdb;

		$this->interp_data = [
			'prefix' => $wpdb->prefix,
			'dbname' => $wpdb->dbname
		];

		// add some small default useful scripts

		// EXISTS::table
		// check if a table exists
		$this->add(
			'EXISTS::table',
			"SELECT COUNT(tn.name) > 0 AS res
			FROM (
				SELECT TABLE_NAME AS name
				FROM information_schema.TABLES
				WHERE TABLE_SCHEMA = '{{ dbname }}'
					AND TABLE_NAME = %s
			) AS tn
			LIMIT 0,1"
		);

		// GET::meta_id
		// retrieves the meta_id given a post_id and a meta_key
		$this->add(
			'GET::meta_id',
			"SELECT meta_id
			FROM {{ prefix }}postmeta
			WHERE post_id = %d AND meta_key = %s
			LIMIT 0,1"
		);

		// GET::list_meta_ids
		// retrieves a list of couples <meta_key, meta_id> given a post_id and a
		// list of meta_keys
		$this->add(
			'GET::list_meta_ids',
			"SELECT meta_key, meta_id
			FROM {{ prefix }}postmeta
			WHERE post_id = %d AND meta_key IN ([[ %s ]])"
		);
	}

	protected function interp(string $script, array $data = []) {
		foreach ($data as $key => $val) {
			$script = str_replace("{{ $key }}", $val, $script);
		}
		return $script;
	}

	protected function flat(string &$script, array &$arr) {
		$flatted = [];
		foreach ($arr as $el) {
			if (is_array($el)) {
				$repeat_count = count($el);
				$repl_pattern = ($repeat_count > 0)
					? str_repeat('$1,', $repeat_count - 1) . '$1'
					: '';
				$script = preg_replace(
					'/\[\[ (%[s|d|u|c|o|x|X|b|g|G|e|E|f|F]) \]\]/',
					$repl_pattern,
					$script,
					1
				);
				if (!empty($el)) {
					array_push($flatted, ...$el);
				}
			} else if ($el instanceof DBScriptTool) {
				$script = $el->apply($script);
				$el->addParams($flatted);
			} else {
				array_push($flatted, $el);
			}
		}
		$arr = $flatted;
	}

	public function addInterpData(string $key, string $value) {
		if (isset($this->interp_data[$key])) {
			throw new \Exception("DBScript: Interpolation key '$key' already present "
				. "in list");
		}
		$this->interp_data[$key] = $value;
	}

	public function add(string $name, string $script, bool $replace = false) {
		if (!$replace && isset($this->scripts_list[$name])) {
			throw new \Exception("DBScript: '$name' already present in list");
		}
		$this->scripts_list[$name] = $script;
	}

	public function load(string $file_name) 
	{
		$file_handle = fopen($file_name, 'r');
		$curr_script_name = '';
		$buffer = [];

		while (($line = trim(fgets($file_handle))) !== false) {
			if (Utils::startsWith($line, '##!') && Utils::endsWith($line, '!##')
					|| feof($file_handle)) {
				if (!empty($curr_script_name)) {
					if (feof($file_handle)) {
						$buffer[] = $line;
					}
					$this->add($curr_script_name, implode("\n", $buffer));
					$buffer = [];
				}
				if (feof($file_handle)) {
					break;
				} else {
					$curr_script_name = trim(Utils::cut($line, '##!', '!##'));
				}
			} else {
				$buffer[] = $line;
			}
		}

		fclose($file_handle);
	}

	public function get(string $script_name) {
		if (!isset($this->scripts_list[$script_name])) {
			return false;
		}
		return $this->scripts_list[$script_name];
	}

	public function call(string $script_name, array $args = [], array $opts = []) {
		$script = $this->get($script_name);
		if ($script === false) {
			return false;
		}
		return $this->exec($script, $args, $opts);
	}

	public function paginate(string $script_name, int $page, int $per_page, array $args = [])
		: array {

		$total = static::call(
			$script_name,
			$args,
			[
				'single' => true,
				'before' => 'SELECT COUNT(*) AS res FROM ( ',
				'after' => ' ) AS total_results'
			]
		);

		$total = (int)$total->res;

		$offset = ($page - 1) * $per_page;

		$results = static::call(
			$script_name,
			$args,
			[
				'get_results' => true,
				'after' => " LIMIT {$offset}, {$per_page}"
			]
		);

		$last_page = ceil($total / $per_page);

		$pagination_data = [
			'results' => $results,
			'pagination' => [
				'total' => $total,
				'per_page' => $per_page,
				'current_page' => $page,
				'last_page' => $last_page
			]
		];
		if ($page > 1) {
			$pagination_data['pagination']['prev_page'] = $page - 1;
		}
		if ($page < $last_page) {
			$pagination_data['pagination']['next_page'] = $page + 1;
		}

		return $pagination_data;
	}

	/**
	 * Choose the exec options (wpdb method and single/multiple param) by looking
	 * at the query if nobodye set them.
	 * @param  string $script the query
	 * @param  array  $opts   the passed options
	 * @return array          the (eventually) updated options
	 */
	private static function chooseExecOptions(string $script, array $opts = []): array 
	{
		if (! isset($opts['insert_id']) && Utils::startsWith(strtoupper($script), 'INSERT')) {
			$opts['insert_id'] = true;
		}

		if ($opts['insert_id'] ?? false) {
			$opts['get_results'] = false;
		}

		if (!isset($opts['get_results'])) {
			$opts['get_results'] = (Utils::startsWith(strtoupper($script), 'SELECT'))
				? true
				: false;
		}
		if ($opts['get_results'] === true && !isset($opts['single'])) {
			if (preg_match("/LIMIT\s+(\d+\s*,\s*1|1\s+OFFSET\s+\d+)$/", $script) == 1) {
				$opts['single'] = true;
			} else {
				$opts['single'] = false;
			}
		}

		return $opts;
	}

	protected function transform(string $script, array $opts = []) {
		if (isset($opts['before']) && is_string($opts['before'])) {
			$script = $opts['before'] . ' ' . $script;
		}

		if (isset($opts['after']) && is_string($opts['after'])) {
			$script .= ' ' . $opts['after'];
		}

		return $script;
	}

	public function exec(string $script, array $args = [], array $opts = []) {
		$script = $this->interp($script, $this->interp_data);
		if (!empty($args)) {
			$this->flat($script, $args);
			if (!empty($args)) {
				$script = $this->wpdb->prepare($script, ...$args);
			}
		}
		$script = trim($script);

		$opts = static::chooseExecOptions($script, $opts);

		$script = static::transform($script, $opts);

		$result = null;
		if ($opts['get_results']) {
			$result = $this->wpdb->get_results($script);
			if ($opts['single'] && count($result) > 0) {
				$result = $result[0];
			}
		} else {
			$result = $this->wpdb->query($script);
		}

		if ($opts['insert_id'] ?? false) {
			$result = $this->wpdb->insert_id;
		}

		if ($result === false) {
			throw new \Exception('DBScripts: ' . $this->wpdb->last_error);
		}
		return $result;
	}

	/**
	 * Begin a Transaction through WordPress wpdb
	 */
	public function beginTransaction() {
		$this->wpdb->query("START TRANSACTION");
	}

	/**
	 * Rollback a Transaction through WordPress wpdb
	 */
	public function rollbackTransaction() {
		$this->wpdb->query("ROLLBACK");
	}

	/**
	 * Commit a Transaction through WordPress wpdb
	 */
	public function commitTransaction() {
		$this->wpdb->query("COMMIT");
	}

	/**
	 * Execute a callable inside a Transaction (through WordPress wpdb); if all
	 * goes well, it commit the transaction and returns the value returned by
	 * calling the specified callable; otherwise, if an exception is catched, it
	 * rollbacks the transaction and re-throw the exception.
	 * @param  callable   $func the transactional code to execute
	 * @return mixed            what the callable returns
	 */
	public function transaction(callable $func) {
		$result = null;
		try {
			$this->beginTransaction();

			$result = $func();

			$this->commitTransaction();
		} catch(\Exception $e) {
			$this->rollbackTransaction();
			throw $e;
		}

		return $result;
	}

	public function debug(string $script_name, array $args = [], array $opts = []) {
		$script = $this->interp($this->get($script_name), $this->interp_data);
		if (!empty($args)) {
			$this->flat($script, $args);
			if (!empty($args)) {
				$script = $this->wpdb->prepare($script, ...$args);
			}
		}

		$opts = static::chooseExecOptions($script, $opts);

		$script = static::transform($script, $opts);

		return trim($script);
	}

}

class DBScripts {

	public static function load(string $file_name) {
		DBScriptsHandler::getInstance()->load($file_name);
	}

	public static function addInterpData(string $key, string $value) {
		DBScriptsHandler::getInstance()->addInterpData($key, $value);
	}

	public static function add(string $name, string $script) {
		DBScriptsHandler::getInstance()->add($name, $script);
	}

	public static function replace(string $name, string $script) {
		DBScriptsHandler::getInstance()->add($name, $script, true);
	}

	public static function get(string $script_name) {
		return DBScriptsHandler::getInstance()->get($script_name);
	}

	public static function call(string $script_name, array $args = [], array $opts = []) {
		return DBScriptsHandler::getInstance()->call($script_name, $args);
	}

	public static function paginate(string $script_name, int $page, int $per_page, array $args = []) {
		return DBScriptsHandler::getInstance()->paginate($script_name, $page, $per_page, $args);
	}

	public static function exec(string $script, array $args = [], array $opts = []) {
		return DBScriptsHandler::getInstance()->exec($script, $args, $opts);
	}

	public static function beginTransaction() {
		DBScriptsHandler::getInstance()->beginTransaction();
	}

	public static function rollbackTransaction() {
		DBScriptsHandler::getInstance()->rollbackTransaction();
	}

	public static function commitTransaction() {
		DBScriptsHandler::getInstance()->commitTransaction();
	}

	public static function transaction(callable $func) {
		return DBScriptsHandler::getInstance()->transaction($func);
	}

	public static function debug(string $script, array $args = [], array $opts = []) {
		return DBScriptsHandler::getInstance()->debug($script, $args, $opts);
	}

}