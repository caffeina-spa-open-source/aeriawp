var AeriaTextArea = function(opts) {
	var field = new AeriaText(opts);

	field._createInput = function() {
		var inp = jQuery('<textarea/>');
		inp.attr('name', this.fieldName);
		inp.attr('title', this.description);
		inp.prop('aeria-field', true);
		
		inp.addClass("aeria-" + this.type);

		return inp;
	};

	return field;
};

AeriaFields.register('textarea', AeriaTextArea);