var AeriaList = function(opts) {
	var field = new AeriaField(opts);

	field._fieldsReference = field.fields;

	field.fields = [];

	field.showAtLeastOne = typeof opts.showatleastone !== 'undefined'
		? opts.showatleastone
		: true;

	var fieldsDef = opts.fields || [];

	var createLine = function(i) {
		var group = {
			id: field.fieldName + '--' + i,
			label: '',
			type: 'group',
			fields: field._fieldsReference
		};
		field.fields.push(group);
	};

	var removeLine = function(i) {
		field.fields.splice(i, 1);
		field._dom.input;
	};

	for (var i in fieldsDef) {
		var fieldDef = fieldsDef[i];
		fieldDef.scope = field.fieldName + '--' + i;
		field.fields.push(AeriaFields.getInstance(fieldDef));
	}

	field.setData = function(rawValue) {
		var values = jQuery.parseJSON(rawValue);
		if ((values == null || values.length) == 0 && field.showAtLeastOne) {
			values = [{}];
		}
		for (var j in values) {
			createLine(j);
		}
		for (var j in field.fields) {
			var li = jQuery('<li/>');
			field.fields[i].render(li);
			field._dom.input.append(li);
		}
		for (var i in values) {
			var value = values[i];
			var fInp = this._dom.fieldContainer.find('[name="' + i + '"]');
			if (fInp.length == 1) {
				if (fInp.data('aeria-field')) {
					fInp.data('aeria-field').setData(value);
				} else {
					fInp.val(value);
				}
			}
		}
	};

	field._createInput = function() {
		var inp;
		inp = jQuery('<ol data-aeria-fields-list/>');

		return inp;
	};

	this._createLabel = function() {
		var label = jQuery('<label/>');
		label.attr('for', field.fieldName);
		label.text(this.label);
		label.append(jQuery('<button>add</button>').on('click', function(e) {
			e.preventDefault();
			createLine(field._dom.input.children('li').length);
		}));
		label.append(jQuery('<button>clear</button>').on('click', function(e) {
			e.preventDefault();
			field._dom.input.children('li').remove();
			field.fields.length = 0;
		}));
		return label;
	};

	this.getData = function() {
		var data = [];
		for (var j in field.fields) {
			var group = field.fields[j];
			data.push(group.getData());
		}
		return data;
	};

	return field;
};

AeriaFields.register('list', AeriaList);