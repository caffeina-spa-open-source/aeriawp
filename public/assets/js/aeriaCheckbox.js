var AeriaCheckbox = function(opts) {
	var field = new AeriaField(opts);

	field.setData = function(rawValue) {
		this._dom.input.prop('checked', !!rawValue);
	};

	field._createInput = function() {
		var inp = jQuery('<input value="true"/>');
		inp.attr('type', this.type);
		inp.attr('name', this.fieldName);
		inp.attr('title', this.description);
		inp.prop('aeria-field', true);
		inp.addClass("aeria-" + this.type);

		return inp;
	};

	field.getData = function() {
		return field._dom.input.prop('checked') == true
			? 1
			: 0;
	};

	field.validate = function() {
		var data = field.getData();

		if (field.required && !data) {
			return field.requiredMessage;
		}

		var errorsMsg = field._callValidators(data);

		return errorsMsg.length == 0 ? true : errorsMsg.join("\n");
	};

	return field;
};

AeriaFields.register('checkbox', AeriaCheckbox);