var AeriaText = function(opts) {
	var field = new AeriaField(opts);

	if (field.validation.pattern !== null) {
		// NOTE: html5 pattern attribute
		field.validators.push(function(val) {
			var rx = new RegExp(field.validation.pattern);
			if (!rx.test(val)) {
				return "'" + field.label + "': Il valore indicato non è valido";
			}
			return true;
		});
	}
	
	if (field.minlength > 0) {
		// NOTE: min-length attribute
		field.validators.push(function(val) {
			if (val.length < field.validation.minlength) {
				return "'" + field.label + "': Lunghezza minima: " + field.validation.minlength;
			}
			return true;
		});
	}
	if (field.validation.maxlength !== null) {
		// NOTE: max-length attribute
		field.validators.push(function(val) {
			if (val.length > field.validation.maxlength) {
				return "'" + field.label + "': Lunghezza massima: " + field.validation.maxlength;
			}
			return true;
		});
	}

	return field;
};

AeriaFields.register('text', AeriaText);