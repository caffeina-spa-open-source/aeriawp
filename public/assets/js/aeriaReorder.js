function populateTree(cont, tree, parents, level) {
	var level = level || 0;
	var filtered = tree.filter(function(el) {
		return parents.indexOf(el.parent) >= 0;
	});
	if (filtered.length == 0) {
		return;
	}
	var newParents = [];
	for (var i in filtered) {
		var el = filtered[i];
		var nd = cont.find('#list_' + el.parent);
		if (nd.length == 0) {
			throw("Unknown parent '" + el.parent + "'");
		}
		var ul = nd.children('ul');
		if (ul.length == 0) {
			ul = jQuery('<ul></ul>');
			nd.append(ul);
		}
		newParents.push(el.id);
		var li = jQuery(
			'<li id="list_' + el.id + '" class="menu-item data-post-type="' + el.type + '" data-post-id="' + el.id + '"></li>'
		);
		li.append('<div class="menu-item-bar">'
			+ '<div class="menu-item-handle ui-sortable-handle">'
				+ '<span class="item-title">'
					+ '<span class="menu-item-title">'
						+ el.description
						+ '</span>'
				+ '</span>'
				+ '<span class="item-controls">'
					+ '<span class="item-type">' + el.typedescr + '</span>'
					// + '<a class="down"><span class="dashicons dashicons-arrow-down">&nbsp;</span></a>'
				+ '</span>'
				+ '<a class="edit-post" href="post.php?post=' + el.id + '&action=edit"><span class="dashicons dashicons-welcome-write-blog">&nbsp;</span></a>'
			+ '</div>'
		+ '</div>');
		ul.append(li);
	}
	if (newParents.length > 0) {
		level += 1;
		populateTree(cont, tree, newParents, level);
	}
}

function initAeriaReorder() {
	var form = jQuery('#sort-posts-form');
	var aeriaSortList = jQuery('#sort-posts-container');
	if (aeriaSortList.length > 1) {
		throw("The page should contain exactly one 'aeria-sort-posts'");
	} else if (aeriaSortList.length < 1) {
		return;
	}
	var rootNd = aeriaSortList.find('#post-list');
	rootNd.attr('data-post-id', '0');
	rootNd.attr('id', 'list_0');

	// the submit button starts as disabled; we re-enable it after
	// populating the form
	var submitBtn = jQuery('#submit');
	var postType = aeriaSortList.attr('data-aeria-sort-posts');
	var url = window.ajaxurl;
	if (postType === '__global') {
		url += '?action=reorder_search_all_posts_head';
	} else {
		url += '?action=reorder_search_posts_head'
			+ '&post_type='
			+ postType
			+ '&orderby=menu_order'
			+ '&order=asc';
	}

	var loadingSpinner = jQuery('<div id="loading-spinner"><span class="spinner">&nbsp;</span><br/><br/>Caricamento...</div>');
	aeriaSortList.append(loadingSpinner);

	var btnAddSpinner = function(el) {
		el.prop('disabled', true);
		jQuery('<span class="spinner" style="margin-left: 6px;">&nbsp;</span>').insertAfter(el);
	};
	var btnRemoveSpinner = function(el) {
		el.prop('disabled', false);
		el.next('span.spinner').remove();
	};

	jQuery.get({
		url: url,
		dataType: 'json',
		success: function(result) {
			populateTree(aeriaSortList, result.data.posts, [0]);
			var srtbl = rootNd.children('ul').nestedSortable({
				items: 'li',
				toleranceElement: '> div',
				placeholder: 'sortable-placeholder',
				listType: 'ul',
				excludeRoot: true,
				maxLevels: result.data.hierarchical ? 6 : 1
			});
			submitBtn.prop('disabled', false);
			form.on('submit', function(e) {
				e.preventDefault();
				btnAddSpinner(submitBtn);
				var hieried = srtbl.nestedSortable('toHierarchy', { startDepthCount: 0 });
				var data = JSON.stringify(hieried);
				var saveUrl = window.ajaxurl + '?action=save_posts_order'
					+ '&posttype='
					+ postType;
				jQuery.post({
					url: saveUrl,
					data: { order: data },
					dataType: 'json',
					success: function(result) {
						btnRemoveSpinner(submitBtn);
						jQuery(window).scrollTop(0);
						addInfo(result.message);
					},
					error: function(result) {
						btnRemoveSpinner(submitBtn);
						jQuery(window).scrollTop(0);
						addAlert(result.responseJSON.message);
					}
				})
			});
			loadingSpinner.remove();
		},
		error: function(result) {
			addAlert(result.responseJSON.message);
			loadingSpinner.remove();
		}
	});

	var fixedBtn = jQuery('#fixed-btn');
	if (fixedBtn.length > 0) {
		jQuery(window).scroll(function() {
		  if (jQuery(window).scrollTop() + jQuery(window).height() > jQuery(document).height() - 75) {
				fixedBtn.addClass('unfixed');
			} else {
				fixedBtn.removeClass('unfixed');
			}
		});
	}
}

jQuery(document).ready(function(){
	initAeriaReorder();
});