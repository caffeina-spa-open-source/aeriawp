var AeriaSelect = function(opts) {
	var field = new AeriaField(opts);

	field.options = opts.options || [];
	if (typeof opts.noOption === 'undefined') {
		field.noOption = { id: '', label: ' -- ' };
	} else {
		field.noOption = opts.noOption;
	}
	field.multiple = opts.multiple || false;
	if (typeof opts.url != 'undefined') {
		field.url = opts.url;
	}

	var populateOptions = function(inp, options, noOption) {
		if (noOption !== false) {
			options.unshift(noOption);
		}
		for (var i in options) {
			var option = options[i];
			var value = option.id || option;
			var label = option.label || option.description || value;
			inp.append('<option '
				+ (value == this.value ? ' selected="selected" ' : '')
				+ 'value="' + value + '">'
				+ label
				+ '</option>'
			);
		}
	};

	field.setData = function(rawValue) {
		this._dom.input.val(rawValue);
	}

	field._createInput = function() {
		var inp = jQuery('<select data-aeria-select/>');
		if (field.multiple) {
			inp.prop('multiple', true);
		}
		inp.attr('name', this.fieldName);
		inp.attr('title', this.description);
		inp.prop('aeria-field', true);
		inp.addClass("aeria-" + this.type);
		if (typeof field.url != 'undefined') {
			var url = window.ajaxurl + '?action=' + field.url;
			jQuery.get({
				url: url,
				dataType: 'json',
				success: function(result) {
					populateOptions(inp, result.data.posts, field.noOption);
				},
				error: function(result) {
					addAlert(result.responseJSON.message);
				}
			});
		} else if (typeof field.options != 'undefined') {
			populateOptions(inp, field.options, field.noOption);
		}
		// add select2/chosen?
		return inp;
	};

	return field;
};

AeriaFields.register('select', AeriaSelect);