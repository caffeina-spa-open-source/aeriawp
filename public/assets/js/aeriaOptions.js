function initAeriaOptions() {
	var form = jQuery('#options-form');
	// the submit button starts as disabled; we re-enable it after
	// populating the form
	var submitBtn = jQuery('#submit');
	// should be only one
	var aeriaOptions = jQuery('[data-aeria-options]');
	if (aeriaOptions.length > 1) {
		throw("The page should contain exactly one 'aeria-options'");
	} else if (aeriaOptions.length == 0) {
		return;
	}
	var optionsId = aeriaOptions.attr('data-aeria-options');
	var url = window.ajaxurl
		+ '?action=get_'
		+ optionsId
		+ '_options_fields';
	jQuery.get({
		url: url,
		dataType: 'json',
		success: function(result) {
			AeriaFields.populateFields(
				optionsId,
				aeriaOptions,
				result.data
			);
			submitBtn.prop('disabled', false);
			form.on('submit', function(e) {
				e.preventDefault();
				var saveOptionsUrl = window.ajaxurl
					+ '?action=save_options_'
					+ optionsId;
				var optionsFields = aeriaOptions.data('fields');
				var data = {};
				if (!tryGetFieldsData(optionsFields, data)) {
					return;
				}
				data.option_page = form.find('#option_page_id').val();
				data._wpnonce = form.find('#_wpnonce').val();
				data._wp_http_referer = form.find('#_wp_http_referer').val();
				jQuery.post({
					url: saveOptionsUrl,
					data: data,
					dataType: 'json',
					success: function(result) {
						addInfo(result.message);
					},
					error: function(result) {
						addAlert(result.responseJSON.message);
					}
				});
			});
		},
		error: function(result) {
			addAlert(result.responseJSON.message);
		}
	});
}

jQuery(function() {
	initAeriaOptions();
});