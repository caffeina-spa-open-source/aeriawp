var AeriaGroup = function(opts) {
	var field = new AeriaField(opts);

	field.fields = [];

	var fieldsDef = opts.fields || [];

	for (var i in fieldsDef) {
		var fieldDef = fieldsDef[i];
		fieldDef.scope = field.fieldName;
		field.fields.push(AeriaFields.getInstance(fieldDef));
	}

	return field;
};

AeriaFields.register('group', AeriaGroup);