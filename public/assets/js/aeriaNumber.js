var AeriaNumber = function(opts) {
	var field = new AeriaField(opts);

	this._createInput = function() {
		var inp;
			
		inp = jQuery('<input/>');
		inp.attr('type', this.type);
		inp.attr('name', this.fieldName);
		inp.attr('title', this.description);
		inp.prop('aeria-field', true);
		inp.addClass("aeria-" + this.type);

		if (field.validation.min !== null) {
			inp.attr('min', field.validation.min);
		}
		if (field.validation.max !== null) {
			inp.attr('max', field.validation.max);
		}
		if (field.validation.step !== null) {
			inp.attr('step', field.validation.step);
		}

		return inp;
	};

	// Validators

	// not a number
	field.validators.push(function(val) {
		if (isNaN(val)) {
			return "'" + field.label + "': Il valore indicato non è valido";
		}
		return true;
	});

	if (field.validation.min !== null) {
		// NOTE: min attribute
		field.validators.push(function(val) {
			var val = Number(val);
			if (val < field.validation.min) {
				return "'" + field.label + "': Indicare un valore maggiore o uguale a "
					+ field.validation.min;
			}
			return true;
		});
	}
	if (field.validation.max !== null) {
		// NOTE: max attribute
		field.validators.push(function(val) {
			var val = Number(val);
			if (val < field.validation.max) {
				return "'" + field.label + "': Indicare un valore minore o uguale a "
					+ field.validation.max;
			}
			return true;
		});
	}
	if (field.validation.step !== null) {
		// NOTE: step attribute
		field.validators.push(function(val) {
			var val = Number(val);
			if (val % field.validation.step != 0) {
				return "'" + field.label + "': Indicare un valore multiplo di "
					+ field.validation.step;
			}
			return true;
		});
	}

	return field;
};

AeriaFields.register('number', AeriaNumber);