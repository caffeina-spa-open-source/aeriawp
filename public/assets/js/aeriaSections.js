var AeriaSections = function(opts) {
	var field = new AeriaField(opts);

	field.fields = [];

	field.sectionTypes = {};

	field.sectionFields = {};

	field.accepts = opts.accepts || true;

	var postIdInp = jQuery('#post_ID');

	var suid = 0;

	var sectGetData = function(el) {
		var data = [];

		var fields = el.find('[data-aeria-field]');
		fields.each(function() {
			var inp = jQuery(this);
			var inpFieldName = inp.attr('data-aeria-field');
			if (!field.sectionFields[inpFieldName]) {
				return true;
			}
			var inpField = field.sectionFields[inpFieldName];
			var fieldDataToPost = {
				key: inpField.id,
				value: inpField.getData()
			};
			var fieldId = inp.attr('data__fieldId');
			if (fieldId != 0) {
				fieldDataToPost.id = fieldId;
			}

			data.push(fieldDataToPost);
		});

		return data;
	};

	var addAndRender = function(sectionId, postmetaId, where, sectionType, values, ids, action) {
		var values = values || {};
		var ids = ids || {};
		var sectionId = sectionId || 'n' + suid;
		var postmetaId = postmetaId || null;
		++suid;
		var sect = field.sectionTypes[sectionType];
		var fieldsDef = sect.fields;

		var li = jQuery('<li style="border:1px solid #444;position:relative;padding-top:15px;"/>');
		li.append(jQuery('<span style="position:absolute;top:0;left:0;font-weight:bold;">' + sectionType + '</span>'));

		var sectionsIndex = jQuery('<input id="aeria-section--' + sectionId + '" type="hidden" name="aeria--sections[]"/>');
		sectionsIndex.val(sectionId);
		li.append(sectionsIndex);

		var title = sect.title || '';
		var status = sect.status || '';

		var fieldset = jQuery('<fieldset/>');

		fieldset.append(
			'<label>Titolo: '
			+ '<input data-aeria-section="' + sectionId + '" data-section-attr="title" type="text" name="' + sectionId + '--title" value="' + title + '"/>'
			+ '</label>'
			+ '<label>Status: '
			+ '<select data-aeria-section="' + sectionId + '" data-section-attr="status" type="text" name="' + sectionId + '--status">'
			+ '<option value="draft"' + (status=='draft'?' selected':'')+'>draft</option>'
			+ '<option value="public"' + (status=='public'?' selected':'')+'>public</option>'
			+ '<option value="hidden"' + (status=='hidden'?' selected':'')+'>hidden</option>'
			+ '</select>'
			+ '</label>'
		);

		li.append(fieldset);

		for (var i in fieldsDef) {
			var fieldDef = fieldsDef[i];
			fieldDef.scope = field.fieldName;

			var fdiv = jQuery('<div data-aeria-sect-fields="' + sectionId + '"/>');

			li.append(fdiv);
			fieldDef.scope = sectionId;
			var inst = (AeriaFields.getInstance(fieldDef));
			inst.render(fdiv);
			if (typeof values[fieldDef.id] !== 'undefined') {
				inst.setData(values[fieldDef.id]);
			}
			inst._dom.fieldContainer.attr(
				'data__fieldId',
				(typeof ids[fieldDef.id] !== 'undefined')
					? ids[fieldDef.id]
					: 0
			);

			field.sectionFields[inst.fieldName] = inst;
		}

		var div = jQuery('<div data-aeria-sect-meta="' + sectionId + '"/>');

		var sectionTypeInp = jQuery('<input data-aeria-section="' + sectionId + '" data-section-attr="type" type="hidden" name="' + sectionId + '--type"/>');
		sectionTypeInp.val(sectionType);
		div.append(sectionTypeInp);

		var sectionGroupInp = jQuery('<input data-aeria-section="' + sectionId + '" data-section-attr="sectiongroup" type="hidden" name="' + sectionId + '--sectiongroup"/>');
		sectionGroupInp.val(postmetaId);
		div.append(sectionGroupInp);

		var sectionIdInp = jQuery('<input data-aeria-section="' + sectionId + '" data-section-attr="id" type="hidden" name="' + sectionId + '--section_id"/>');
		sectionIdInp.val(sectionId);
		div.append(sectionIdInp);

		var sectionRemovedInp = jQuery('<input data-aeria-section="' + sectionId + '" data-section-attr="action" type="hidden" name="' + sectionId + '--action"/>');
		sectionRemovedInp.val(action);
		div.append(sectionRemovedInp);

		li.append(div);

		var jsonBtn = jQuery('<button type="button" data-btn-aeria-section="' + sectionId + '">json</button>');
		jsonBtn.on('click', function(e) {
			e.preventDefault();
			var th = jQuery(this);
			var clickedSectionId = th.attr('data-btn-aeria-section');
			var section = {};
			li.find('[data-aeria-section]').each(function() {
				var el = jQuery(this);
				section[el.attr('data-section-attr')] = el.val();
			});
			section.data = sectGetData(li);
			var jsonned = JSON.stringify(section);
			jQuery('#aeria-section--' + clickedSectionId).val(jsonned);
console.log('jsonned:', jsonned);
		});

		var removeBtn = jQuery('<button type="button" data-btn-aeria-section="' + sectionId + '">remove</button>');
		removeBtn.on('click', function(e) {
			e.preventDefault();
			var th = jQuery(this);
			var li = th.parent();
			li.hide();
			li.find('[data-section-attr="action"]').val('delete');
			jsonBtn.trigger('click');
		});

		li.append(removeBtn);
		li.append(jsonBtn);

		jsonBtn.trigger('click');

		where.append(li);
	};

	field.setData = function(rawValue) {
		this._dom.input.val(rawValue);
	};

	jQuery.get({
		url: window.ajaxurl + '?action=get_sections_data',
		data: { post_id: postIdInp.val() },
		success: function(result) {
			var selSectType = jQuery('<select id="select-section-type"/>');
			selSectType.append('<option value="0">---</option>');
			for (var i in result.data.sections_types) {
				var sectionType = result.data.sections_types[i];
				if (field.accepts === true || field.accepts.indexOf(sectionType.id) >= 0) {
					field.sectionTypes[sectionType.id] = sectionType;
					selSectType.append('<option value="' + sectionType.id + '">' + sectionType.title + '</option>');
				}
			}
			var childrenContainer = field._dom.fields;
			var headCont = jQuery('<div/>');
			var ul = jQuery('<ul/>');
			childrenContainer.append(headCont);
			childrenContainer.append(ul);

			var btn = jQuery('<button>+</button>').on('click', function(e) {
				e.preventDefault();
				if (selSectType.val() == 0) {
					return;
				}
				addAndRender(null, field.fieldName, ul, selSectType.val(), {}, {}, 'create');
			});
			headCont.append(selSectType);
			headCont.append(btn);

			for (var i in result.data.saved_sections) {
				var savedSection = result.data.saved_sections[i];
				var values = {};
				var ids = {};
				for (var j in savedSection.fields) {
					var valueField = savedSection.fields[j];
					values[valueField.key] = valueField.value;
					ids[valueField.key] = valueField.id;
				}
				addAndRender(savedSection.id, field.fieldName, ul, savedSection.type, values, ids, 'update');
			}
		}
	});

	field._createInput = function() {
		var inp = jQuery('<input/>');
		inp.attr('type', 'hidden');
		inp.attr('name', field.fieldName);
		inp.prop('aeria-field', true);
		inp.addClass("aeria-" + field.type);

		return inp;
	};

	field.render = function(parentElement) {
		var fieldContainer = jQuery('<div class="aeria-container"/>');
		fieldContainer.attr('data-aeria-field', field.fieldName);

		var inp = field._createInput();

		// construct field container
		// NOTE: right now it's the same as the old Aeria
		var row = jQuery('<div class="row"/>');
		var inpContainer = jQuery('<div class="col-md-8"/>');
		var container = jQuery('<div class="container"/>');

		var childrenContainer = jQuery('<div/>');

		// add description

		inpContainer.append(inp);
		row.append(inpContainer);
		container.append(row);
		fieldContainer.append(container);
		fieldContainer.append(childrenContainer);

		var inpData = jQuery('<input type="hidden" name="' + inp.attr('name') + '--data" value=""/>');

		jQuery(parentElement).append(fieldContainer);

		// just in case we need to get quickly some DOM-elements
		field._dom.fieldContainer = fieldContainer;
		field._dom.input = inp;
		field._dom.inputData = inpData;
		field._dom.fields = childrenContainer;

		inp.data('aeria-field', field);

		// set field data, with default or real value
		field.setData(field.value);

		field._rendered = true;
	};

	field.getData = function() {
		var lis = field._dom.fields.children('ul').children('li');
		var list = [];
		var atLeastOne = false;
		var data = {};
		for (var i = 0; i < lis.length; ++i) {
			var li = jQuery(lis[i]);
			var inp = li.find('[aeria-field]');
			if (inp.length == 0) {
				continue;
			}

			var inpField = inp.data('aeria-field');
			data[inpField.fieldName] = inpField.getData();
			atLeastOne = true;
		}
		field._dom.inputData.val(JSON.stringify(data));
		return atLeastOne ? 1 : null;
	};

	return field;
};

AeriaFields.register('sections', AeriaSections);