jQuery(function() {
	// globals:
	// 	* rowsReference: object with every table row;
	// 	* handlersReference: object with every explicit js handler.

	var aeriaTable = null;
	var tableId = null;
	var tablePageContainer = null;
	var rerenderFilters = null;
	var tableOverlay = null;

	var updateElementsReference = function() {
		aeriaTable = jQuery('.aeria-table');
		tableId = aeriaTable.attr('data-table-id');
		tablePageContainer = jQuery('#wrap-table--' + tableId);
		rerenderFilters = jQuery('#rerender-filters').val();
		tableOverlay = jQuery('#table-overlay');

		aeriaTable.bind('reload-table', function(e, reloadData) {
			reloadTable(reloadData);
		});
		aeriaTable.bind('start-loading', function() {
			tableAddSpinner();
		});
		aeriaTable.bind('stop-loading', function() {
			tableRemoveSpinner();
		});
	};

	updateElementsReference();

	var findRowById = function(id) {
		for (var i in window.aeriaTableData.rowsReference) {
			if (window.aeriaTableData.rowsReference[i].id == id) {
				return window.aeriaTableData.rowsReference[i];
			}
		}
		return null;
	};

	var btnAddSpinner = function(el) {
		el.prop('disabled', true);
		var spinner = jQuery('<span class="spinner" style="margin: 3px 5px; visibility: visible;">&nbsp;</span>');
		if (el.is('.row-action')) {
			if (el.is('.with-icon')) {
				el.children('span').hide();
			}
			el.prepend(spinner);
		} else {
			spinner.insertAfter(el);
		}
	};
	var btnRemoveSpinner = function(el) {
		el.prop('disabled', false);
		if (el.is('.row-action')) {
			el.children('span.spinner').remove();
			if (el.is('.with-icon')) {
				el.children('span').show();
			}
		} else {
			el.next('span.spinner').remove();
		}
	};

	var tableAddSpinner = function() {
		tableOverlay.show();
	};
	var tableRemoveSpinner = function() {
		tableOverlay.hide();
	};

	var bindActions = function() {
		if (aeriaTable.length == 0) {
			return;
		}

		var searchBtn = jQuery('#table-search-submit');
		if (searchBtn.length > 0) {
			var searchInput = jQuery('#table-search-input');
			searchBtn.on('click', function(e) {
				e.preventDefault();
				btnAddSpinner(searchBtn);
				window.location.href = searchBtn.attr('data-search-href') + '&s=' + encodeURIComponent(searchInput.val());
			});
			searchInput.on('keyup', function(e) {
				if (e.keyCode == 13) {
					searchBtn.trigger('click');
				}
			});
		}

		var filterBtn = jQuery('#post-query-submit');
		if (filterBtn.length > 0) {
			var filtersValues = [];
			var filterInputs = jQuery('#table-filters').find('.table-filter');
			filterBtn.on('click', function(e) {
				e.preventDefault();
				btnAddSpinner(filterBtn);
				var q = [];
				filterInputs.each(function() {
					var filterInput = jQuery(this);
					if (filterInput.val() != -1) {
						q.push(filterInput.attr('data-filter').trim() + '=' + filterInput.val());
					}
				});
				var filtersQ = (q.length > 0)
					? '&' + q.join('&')
					: '';
				window.location.href = filterBtn.attr('data-filter-href') + filtersQ;
			});
		}

		jQuery('.bulk-action').on('click', function(e) {
			e.preventDefault();
			var actionElement = jQuery(this);

			var selectAction = actionElement.attr('data-select-action');
			var selectActionElement = jQuery('#' + selectAction);
			var actionId = selectActionElement.val();
			if (actionId === '-1') {
				return;
			}
			var optionElement = selectActionElement.find('option[value="' + actionId + '"]');
			if (actionId.length == 0) {
				return;
			}
			var method = optionElement.attr('data-method');
			var confirmMessage = optionElement.attr('data-confirm');

			var data = {
				rows: []
			};

			aeriaTable.find('.check-column input.row-check').each(function() {
				var cb = jQuery(this);
				if (cb.is(':checked')) {
					var rowData = findRowById(cb.val());
					if (rowData !== null) {
						data.rows.push(rowData);
					}
				}
				if (data.rows.length == 0) {
					return;
				}
			});

			handleAction(actionElement, actionId, data, method, confirmMessage, 'bulk');
		});

		jQuery('.single-action').on('click', function(e) {
			e.preventDefault();
			var actionElement = jQuery(this);

			var actionId = actionElement.attr('data-action-id');
			var method = actionElement.attr('data-method');
			var confirmMessage = actionElement.attr('data-confirm');

			handleAction(actionElement, actionId, {}, method, confirmMessage, 'single');
		});

		jQuery('.row-action').on('click', function(e) {
			e.preventDefault();
			var actionElement = jQuery(this);

			var actionId = actionElement.attr('data-action-id');
			var rowId = actionElement.attr('data-id');
			var method = actionElement.attr('data-method');
			var confirmMessage = actionElement.attr('data-confirm');
			var data = findRowById(rowId);

			if (data == null) {
				alert("Unknown row `" + rowId + "`");
				return;
			}

			handleAction(actionElement, actionId, data, method, confirmMessage, 'row');
		});
	};

	var reloadTable = function(reloadData) {
		var rerenderActionUrl = window.ajaxurl
			+ '?action=aeria_table_ajax_'
			+ tableId
			+ '_get_rendered_source'
			+ rerenderFilters;

		tableAddSpinner();

		jQuery.get({
			url: rerenderActionUrl,
			data: reloadData,
			method: 'GET',
			dataType: 'json',
			success: function(result) {
				tablePageContainer.html(result.data.table);
				window.aeriaTableData.rowsReference = result.data.rows_reference;

				updateElementsReference();

				bindActions();
				tableRemoveSpinner();
			},
			error: function(result) {
				addAlert(result.message);
				tableRemoveSpinner();
			}
		});
	};

	var handleAction = function(actionElement, actionId, data, method, confirmMessage, context) {
		if (confirmMessage !== '' && confirmMessage !== null && typeof confirmMessage !== 'undefined') {
			if (!window.confirm(confirmMessage)) {
				return;
			}
		}

		var refId = context + '_actions__' + actionId.replace(tableId + '--', '');
		var hasHandler = typeof window.aeriaTableData.handlersReference[refId] === 'function';

		if (hasHandler) {
			window.aeriaTableData.handlersReference[refId](actionElement, data);
		} else {
			var actionUrl = window.ajaxurl + '?action=' + context + '_' + actionId;
			btnAddSpinner(actionElement);
			jQuery.ajax({
				url: actionUrl,
				method: method,
				data: data,
				dataType: 'json',
				success: function(result) {
					btnRemoveSpinner(actionElement);
					addInfo(result.message);

					var rerenderData = {};
					if (context== 'bulk') {
						var prechecked = [];
						for (var i in data.rows) {
							prechecked.push(data.rows[i].id);
						}
						rerenderData.prechecked = prechecked.join(',');
					}

					aeriaTable.trigger('reload-table', rerenderData);
				},
				error: function(result) {
					btnRemoveSpinner(actionElement);
					addAlert(result.message);
				}
			});
		}
	};

	bindActions();
});