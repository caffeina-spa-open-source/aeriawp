//window.$ = window.jQuery;

function initAeriaMetaboxes() {
	var origStatusInp = jQuery('#original_post_status');
	var postIdInp = jQuery('#post_ID');
	var aeriaMetaboxes = jQuery('[data-aeriametaboxid]');

	var metaData = {};
	if (aeriaMetaboxes.length > 0) {
		/**\/
		var actions = [];
		var metaboxes = {};
		aeriaMetaboxes.each(function(_index, aeriaMetabox) {
			var thMetabox = jQuery(aeriaMetabox);
			var metaboxId = thMetabox.attr('data-aeriametaboxid');
			var thUrl = 'get_' + metaboxId + '_meta_fields';
			metaboxes[thUrl] = thMetabox;
			actions.push({
				action: thUrl,
				get: { post_id: postIdInp.val() }
			});
		});
		var batchUrl = window.ajaxurl + '?action=batch';
		jQuery.post({
			url: batchUrl,
			data: { actions: actions },
			dataType: 'json',
			success: function(result) {
				for (var i in result.data) {
					var metaboxResult = result.data[i];
					var aeriaMetabox = metaboxes[i];
					populateMetabox(
						postIdInp,
						origStatusInp,
						aeriaMetabox,
						metaboxResult.data
					);
				}
			},
			error: function(result) {
				addAlert(result.responseJSON.message);
			}
		});
		/**/
		aeriaMetaboxes.each(function(_index, aeriaMetabox) {
			var metaboxId = jQuery(aeriaMetabox).attr('data-aeriametaboxid');
			var url = window.ajaxurl
				+ '?action=get_'
				+ metaboxId
				+ '_meta_fields'
				+ '&post_id=' + postIdInp.val();
			jQuery.get({
				url: url,
				dataType: 'json',
				success: function(result) {
					populateMetabox(postIdInp, origStatusInp, aeriaMetabox, result.data);
				},
				error: function(result) {
					addAlert(result.responseJSON.message);
				}
			});
		});
		/**/
		jQuery('#post').on('submit', function(e) {
			var sumthinwong = false;
			for (var j = 0; j < aeriaMetaboxes.length; ++j) {
				var metabox = jQuery(aeriaMetaboxes[j]);
				var metaboxFields = metabox.data('fields');
				var _data = {};
				if (!tryGetFieldsData(metaboxFields, _data)) {
					sumthinwong = true;
					break;
				}
			}
			if (sumthinwong) {
				e.preventDefault();
			}
		});
	}
}

function populateMetabox(postIdInp, origStatusInp, aeriaMetabox, metaData) {
	var metabox = jQuery(aeriaMetabox);
	var metaboxId = metabox.attr('data-aeriametaboxid');
	var mbLabel = metabox.attr('data-aeria-label');
	var fields = metaData || {};
	var metaboxFields = AeriaFields.populateFields(metaboxId, metabox, fields);
	// NOTE: this is a brutal test; all this code must be rewritten
	var saveBtn = jQuery('<button type="button"/>');
	saveBtn.addClass('handlediv button-link');
	saveBtn.append(
		jQuery('<span class="screen-reader-text">Salva ' + mbLabel + '</span>')
	);
	saveBtn.append(
		jQuery('<span class="dashicons-before dashicons-yes"><br/></span>')
	);
	saveBtn.attr('title', 'Salva ' + mbLabel);
	var metaboxContainer = metabox.parent().parent();
	saveBtn.insertAfter(metaboxContainer.children('button.handlediv.button-link'));
	saveBtn.on('click', function(e) {
		e.preventDefault();
		var metaboxFields = metabox.data('fields');
		var data = {};
		if (!tryGetFieldsData(metaboxFields, data)) {
			return;
		}
	
		// var errors = [];
		// for (var i in metaboxFields) {
		// 	var field = metaboxFields[i];
		// 	var check = field.validate();
		// 	if (check !== true) {
		// 		errors.push(check);
		// 	} else {
		// 		data[field.fieldName] = field.getData();
		// 	}
		// }
		// if (errors.length > 0) {
		// 	addAlert(errors.join("\n"));
		// 	return;
		// }

		data['post_id'] = postIdInp.val();

		if (origStatusInp.val() == 'auto-draft') {
			data['saveDraft'] = true;
			data['postdata--title'] = jQuery('#title').val();
			data['postdata--content'] = tinymce.editors.content.getContent();
			data['postdata--type'] = jQuery('#post_type').val();
		}

		var saveMetaUrl = window.ajaxurl + '?action=save_meta';
		jQuery.post({
			url: saveMetaUrl,
			data: data,
			dataType: 'json',
			success: function(result) {
				if (result.data.postId != postIdInp.val()) {
					postIdInp.val(result.data.postId);
				}
				if (result.data.savedDraft && result.data.savedDraft === true) {
					origStatusInp.val('draft'); // not sure about this...
				}
				// ...
				addInfo(result.message);
			},
			error: function(result) {
				addAlert(result.responseJSON.message);
			}
		});
	});
}

// function createInputText(domEl, opts){
// 	var container = document.createElement("div");
// 	container.className = "aeria-group-input";

// 	var label = document.createElement("label");
// 	label.className = "aeria-label"; // set the CSS class
// 	label.innerHTML = opts.description;

// 	var input = document.createElement("input");
// 	input.type = "text";
// 	input.name = opts.id;
// 	input.value = opts.value;
// 	input.className = "aeria-text"; // set the CSS class
// 	jQuery(input).blur(function() {
// 		jQuery.ajax({
// 			type: 'POST',
// 			url: window.ajaxurl+'?action=save_meta',
// 			data: {p: window.postId, m: opts.id, v: input.value },
// 			success: function() {
// 				console.log('success');
// 			},
// 			error: function() {
// 				console.log('error');
// 			}
// 		});
// 	});
// 	container.appendChild(label);
// 	container.appendChild(input);
// 	domEl[0].appendChild(container);
// }

jQuery(document).ready(function(){
	initAeriaMetaboxes();
});