/**
 * Base AeriaField; all other AeriaField will inherit from this one and use a
 * similar structure
 */
var AeriaField = function(opts) {
	var opts = opts || {};

	if (!opts.id) {
		throw("AeriaField: No 'id' specified");
	}
	if (!opts.label) {
		throw("AeriaField: No 'label' specified");
	}
	if (!opts.type) {
		throw("AeriaField: No 'type' specified");
	}
	if (!opts.scope) {
		throw("AeriaField: No 'scope' specified");
	}

	this.id = opts.id;
	this.label = opts.label;
	this.type = opts.type;
	this.scope = opts.scope;

	// compose scope and id to obtain the scoped field-name; useful only to group
	// different fields with similar names in different contexts (i.e.: different
	// metaboxes with the 'name' input field)
	this.fieldName = this.scope + '--' + this.id;

	this.description = opts.description || '';
	this.defaultValue = opts.default || null;
	this.value = typeof opts.value === 'undefined'
		? this.defaultValue
		: opts.value;

	this.required = opts.required || false;
	this.requiredMessage = opts.requiredMessage
		|| "'" + this.label + "' dev'essere valorizzato";

	// use this to handle children fields; if it has no children, this must be
	// set to null
	this.fields = null;

	this.validation = opts.validation || {};

	// a list of validators; every element must be a callback and will be called
	// with:
	// 	(this.getData(), this)
	// must return true if it's ok, an error message to show otherwise.
	this.validators = opts.validators || [];

	// reference to DOM elements, if needed
	this._dom = {};

	// maybe some actions can be done only after rendering (i.e.: checking for
	// changes in 'getData'); so we can check if '_rendered' is 'true'
	// NOTE: other fields may need to handle this attribute correctly by
	// themselves when extending AeriaField
	this._rendered = false;

	this.setData = function(rawValue) {
		if (this.fields == null) {
			this._dom.input.val(rawValue);
		} else {
			try {
				var values = jQuery.parseJSON(rawValue);
				for (var i in values) {
					var value = values[i];
					var fInp = this._dom.fieldContainer.find('[name="' + i + '"]');
					if (fInp.length == 1) {
						if (fInp.data('aeria-field')) {
							fInp.data('aeria-field').setData(value);
						} else {
							fInp.val(value);
						}
					}
				}
			} catch(_e) {
				// prevent json parsing exception
			}
		}
	};

	/**
	 * Create the input field; useful for simple field types that only need to
	 * change the input in the representation.
	 */
	this._createInput = function() {
		var inp;
		if (this.fields == null) {
			inp = jQuery('<input/>');
			inp.attr('type', this.type);
			inp.attr('name', this.fieldName);
			inp.attr('title', this.description);
			inp.prop('aeria-field', true);
			inp.addClass("aeria-" + this.type);
		} else {
			inp = jQuery('<ul data-aeria-fields-group/>');
			inp.addClass('c-' + this.fields.length);
			for (var i in this.fields) {
				var li = jQuery('<li/>');
				this.fields[i].render(li);
				inp.append(li);
			}
		}

		return inp;
	};

	/**
	 * Create the label of the field; probably, not useful.
	 */
	this._createLabel = function() {
		var label = jQuery('<label/>');
		label.attr('for', this.fieldName);
		label.text(this.label);
		return label;
	};

	/**
	 * Render the field inside the specified DOM-element; defaults to simple
	 * 'input' field
	 * @param  DOM-element parentElement
	 */
	this.render = function(parentElement) {
		var fieldContainer = jQuery('<div class="aeria-container"/>');
		fieldContainer.attr('data-aeria-field', this.fieldName);

		var inp = this._createInput();
		var label = this._createLabel();

		// construct field container
		// NOTE: right now it's the same as the old Aeria
		var row = jQuery('<div class="row"/>');
		var labelContainer = jQuery('<div class="col-md-4"/>');
		var inpContainer = jQuery('<div class="col-md-8"/>');
		var container = jQuery('<div class="container"/>');

		// add description

		labelContainer.append(label);
		inpContainer.append(inp);
		row.append(labelContainer);
		row.append(inpContainer);
		container.append(row);
		fieldContainer.append(container);

		jQuery(parentElement).append(fieldContainer);

		// just in case we need to get quickly some DOM-elements
		this._dom.fieldContainer = fieldContainer;
		this._dom.input = inp;
		this._dom.label = label;

		inp.data('aeria-field', this);

		// set field data, with default or real value
		this.setData(this.value);

		this._rendered = true;
	};

	/**
	 * Returns the AeriaField data value; by default simply returns the input field
	 * value
	 */
	this.getData = function() {
		if (!this._rendered) {
			return this.value;
		}

		if (this.fields == null) {
			// retrieve & update the current value
			return this._dom.input.val();
		} else {
			var data = {};
			for (var i in this.fields) {
				var field = this.fields[i];
				data[field.fieldName] = field.getData();
			}
			return JSON.stringify(data);
		}
	};

	/**
	 * Call all the validators; stops at the  firs
	 */
	this._callValidators = function(data) {
		var errorsMsg = [];
		for (var i in this.validators) {
			var validator = this.validators[i];
			if (typeof validator !== 'function') {
				throw("Validator must be a function; '"
					+ (typeof validator)
					+ "' found");
			}
			var check = validator(data, this);
			if (check !== true) {
				errorsMsg.push(check);
			}
		}
		return errorsMsg;
	};

	/**
	 * Validate the field; returns the error to show if there is one; true
	 * otherwise.
	 * NOTE: as we use the 'required', we also check for 'emptiness'; if widgets
	 * become complex we may need an 'isEmpty' method; also the  returned value
	 * is kinda raw right now.
	 * @return mixed	true if is valid; a string containing the error to show
	 *                otherwise
	 */
	this.validate = function() {
		var data = this.getData();

		if (data === '' || data ===  null || typeof data === 'undefined') {
			if (this.required) {
				return this.requiredMessage;
			} else {
				// if is empty, but not required, we have no errors
				return true;
			}
		}

		var errorsMsg;
		if (this.fields == null) {
			errorsMsg = this._callValidators(data);
		} else {
			errorsMsg = [];
			for (var i in this.fields) {
				var field = this.fields[i];
				var check = field.validate();
				if (check !== true) {
					errorsMsg = errorsMsg.concat(check);
				}
			}
		}

		return errorsMsg.length == 0 ? true : errorsMsg.join("\n");
	};

	return this;
};