/**
 * Create a wp-notice on top of the page with the specified message and class.
 * @param string msg            notice message
 * @param string noticeClass    notice css class (default: 'update')
 * @param bool   notDismissible if notDismissible is true, the notice cannot be
 *                              removed (default: false)
 */
function addNotice(msg, noticeClass, notDismissible) {
	var noticeClass = noticeClass || 'updated';
	var notDismissible = notDismissible || false;
	var insertAfterMe = jQuery('div.wrap .wp-header-end');
	if (insertAfterMe.length == 0) {
		insertAfterMe = jQuery('div.wrap h2:first');
	}
	var notice = jQuery('<div/>');
	notice.addClass('notice');
	notice.addClass(noticeClass);
	if (!notDismissible) {
		notice.addClass('is-dismissible');
		var dismissBtn = jQuery(
			'<button type="button" class="notice-dismiss">'
				+ '<span class="screen-reader-text">Dismiss this notice.</span>'
			+ '</button>'
		);
		notice.append(dismissBtn);
		dismissBtn.on('click', function() {
			notice.fadeOut(75, function() { notice.remove(); });
		});
	}
	notice.append('<p>' + msg + '</p>');
	notice.insertAfter(insertAfterMe);
}

function addAlert(msg, notDismissible) {
	addNotice(msg, 'notice-error', notDismissible);
}

function addInfo(msg, notDismissible) {
	addNotice(msg, 'notice-info', notDismissible);
}

/**
 * Try to populate the passed data object; at the same time it validates the
 * fields. If the validation fails it alerts a notice with the errors.
 * Returns true if data is populated without errors, false otherwise.
 * NOTE: data must be an object!
 * @param  object fields fields list
 * @param  object data   object data (output)
 * @return bool          true if there are no validation errors,
 *                       false otherwise
 */
function tryGetFieldsData(fields, data) {
		var errors = [];
		for (var i in fields) {
			var field = fields[i];
			var check = field.validate();
			if (check !== true) {
				errors.push(check);
			} else {
				data[field.fieldName] = field.getData();
			}
		}

		if (errors.length > 0) {
			alert(errors.join("\n"));
			return false;
		}
		return true;
}