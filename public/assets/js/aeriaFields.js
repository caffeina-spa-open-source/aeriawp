window.AeriaFields = function() {
	var fieldsRegistry = {};

	var i = {};

	i.register = function(name, type) {
		if (!fieldsRegistry[name]) {
			fieldsRegistry[name] = type;
		}
	};

	i.getInstance = function(opts) {
		var name = opts.type;
		if (typeof fieldsRegistry[name] !== 'undefined') {
			return new fieldsRegistry[name](opts);
		} else {
			//console.log("Unknown field type '" + name + "'");
			throw("Unknown field type '" + name + "'");
		}
	};

	/**
	 * Populate a container with a list of AeriaField(s).
	 * @param  string      containerId id attribute of the container
	 * @param  DOM-element container   container element
	 * @param  object      fields      fields to render inside the container
	 * @return array.                  array with the rendered AeriaFields
	 */
	i.populateFields = function(containerId, container, fields) {
		var container = jQuery(container);
		var containerFields = [];

		var metaboxIndex = jQuery('<input type="hidden" name="aeria--meta[]"/>');
		metaboxIndex.val(containerId);
		container.append(metaboxIndex);

		for (var index in fields) {
			var field = fields[index];
			// add scope to field definition; needed by AeriaField
			field.scope = containerId;

			/* old *\/
			var newField;
			switch(field.type) {
				case "text":
					newField = new AeriaText(field);
					break;
				case "textarea":
					newField = new AeriaTextArea(field);
					break;
				case "select":
					newField = new AeriaSelect(field);
					break;
				case "checkbox":
					newField = new AeriaCheckbox(field);
					break;
				default:
					console.log("Unknown field type '" + field.type + "'");
					continue;
			}
			/* new */
			var newField = i.getInstance(field);
			/**/

			containerFields.push(newField);
			newField.render(container);
		}

		container.data('fields', containerFields);
		return containerFields;
	};

	return i;
}();